//
//  SubCategoryText.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SubCategoryText.h"
#import "SubCategoryDescriptionTextRepository.h"

@implementation SubCategoryText

+(NSMutableArray*) getSubCategory_Texts : (int)textId
{
    SubCategoryDescriptionTextRepository *subCategoryDescriptionTextRepository = [[SubCategoryDescriptionTextRepository alloc] init];
    
    return [subCategoryDescriptionTextRepository getSubCategory_Texts : (int)textId];
}

@end
