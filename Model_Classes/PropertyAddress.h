//
//  PropertyAddress.h
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface PropertyAddress : NSManagedObject

@property (nonatomic, retain) NSString * directionType;
@property (nonatomic, retain) NSString * floorType;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * propertyType;
@property (nonatomic, retain) NSString * streetNo;
@property (nonatomic, retain) NSString * vacancyType;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSSet *propertyAddressToOrder;
@end

@interface PropertyAddress (CoreDataGeneratedAccessors)

- (void)addPropertyAddressToOrderObject:(OrderTable *)value;
- (void)removePropertyAddressToOrderObject:(OrderTable *)value;
- (void)addPropertyAddressToOrder:(NSSet *)values;
- (void)removePropertyAddressToOrder:(NSSet *)values;

@end
