//
//  OwnerAddress.h
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface OwnerAddress : NSManagedObject

@property (nonatomic, retain) NSString * centralPhoneNo;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * emailID;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * landlinePhoneNo;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * maleOrFemale;
@property (nonatomic, retain) NSString * mobilePhoneNo;
@property (nonatomic, retain) NSString * ownerName;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * streetNo;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSSet *ownerAddressToOrder;
@end

@interface OwnerAddress (CoreDataGeneratedAccessors)

- (void)addOwnerAddressToOrderObject:(OrderTable *)value;
- (void)removeOwnerAddressToOrderObject:(OrderTable *)value;
- (void)addOwnerAddressToOrder:(NSSet *)values;
- (void)removeOwnerAddressToOrder:(NSSet *)values;

@end
