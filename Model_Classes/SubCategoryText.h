//
//  SubCategoryText.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubCategoryText : NSObject

@property (nonatomic) NSString *textto;
@property (nonatomic) NSString *checks1;
@property (nonatomic) NSString *checks2;

+(NSMutableArray*) getSubCategory_Texts : (int)textId;

@end
