//
//  TenantAddress.h
//  Service48
//
//  Created by Redbytes on 28/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface TenantAddress : NSManagedObject

@property (nonatomic, retain) NSString * bussinessPhoneNo;
@property (nonatomic, retain) NSString * directionType;
@property (nonatomic, retain) NSString * emailID;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * floorType;
@property (nonatomic, retain) NSString * homePhoneNo;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * maleOrFemale;
@property (nonatomic, retain) NSString * mobilePhoneNo;
@property (nonatomic, retain) NSString * propertyType;
@property (nonatomic, retain) NSString * vacancyType;
@property (nonatomic, retain) NSString * tenantNo;
@property (nonatomic, retain) NSString * orderNo;
@property (nonatomic, retain) NSSet *tenantAddressToOrder;
@end

@interface TenantAddress (CoreDataGeneratedAccessors)

- (void)addTenantAddressToOrderObject:(OrderTable *)value;
- (void)removeTenantAddressToOrderObject:(OrderTable *)value;
- (void)addTenantAddressToOrder:(NSSet *)values;
- (void)removeTenantAddressToOrder:(NSSet *)values;

@end
