//
//  CareTakerCompanyAddress.m
//  Service48
//
//  Created by Redbytes on 21/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "CareTakerCompanyAddress.h"
#import "OrderTable.h"


@implementation CareTakerCompanyAddress

@dynamic careTakerFirmaName;
@dynamic centralPhoneNo;
@dynamic companyName;
@dynamic emailID;
@dynamic firstName;
@dynamic landlinePhoneNo;
@dynamic lastName;
@dynamic maleOrFemale;
@dynamic mobilePhoneNo;
@dynamic place;
@dynamic streetNo;
@dynamic zipCode;
@dynamic firmaEmailID;
@dynamic careTakerCompanyToOrder;

@end
