//
//  CareTakerCompanyAddress.h
//  Service48
//
//  Created by Redbytes on 21/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface CareTakerCompanyAddress : NSManagedObject

@property (nonatomic, retain) NSString * careTakerFirmaName;
@property (nonatomic, retain) NSString * centralPhoneNo;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * emailID;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * landlinePhoneNo;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * maleOrFemale;
@property (nonatomic, retain) NSString * mobilePhoneNo;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * streetNo;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSString * firmaEmailID;
@property (nonatomic, retain) NSSet *careTakerCompanyToOrder;
@end

@interface CareTakerCompanyAddress (CoreDataGeneratedAccessors)

- (void)addCareTakerCompanyToOrderObject:(OrderTable *)value;
- (void)removeCareTakerCompanyToOrderObject:(OrderTable *)value;
- (void)addCareTakerCompanyToOrder:(NSSet *)values;
- (void)removeCareTakerCompanyToOrder:(NSSet *)values;

@end
