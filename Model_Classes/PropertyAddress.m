//
//  PropertyAddress.m
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PropertyAddress.h"
#import "OrderTable.h"


@implementation PropertyAddress

@dynamic directionType;
@dynamic floorType;
@dynamic place;
@dynamic propertyType;
@dynamic streetNo;
@dynamic vacancyType;
@dynamic zipCode;
@dynamic propertyAddressToOrder;

@end
