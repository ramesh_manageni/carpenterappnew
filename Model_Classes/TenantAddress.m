//
//  TenantAddress.m
//  Service48
//
//  Created by Redbytes on 28/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "TenantAddress.h"
#import "OrderTable.h"


@implementation TenantAddress

@dynamic bussinessPhoneNo;
@dynamic directionType;
@dynamic emailID;
@dynamic firstName;
@dynamic floorType;
@dynamic homePhoneNo;
@dynamic lastName;
@dynamic maleOrFemale;
@dynamic mobilePhoneNo;
@dynamic propertyType;
@dynamic vacancyType;
@dynamic tenantNo;
@dynamic orderNo;
@dynamic tenantAddressToOrder;

@end
