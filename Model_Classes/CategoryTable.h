//
//  CategoryTable.h
//  PackingList
//
//  Created by Chetan Zalake on 23/07/13.
//  Copyright (c) 2013 Chetan Zalake. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryTable : NSObject

@property (nonatomic, assign) NSString *category_id;

/*
@property (nonatomic, strong) NSString *category_name;
@property (nonatomic, strong) NSString *category_description;
@property (nonatomic, strong) NSString *category_type ;
@property (nonatomic, strong) NSString *packed;
@property (nonatomic, assign) BOOL isNewCategory;
@property (nonatomic, assign) int count;


+ (CategoryTable *)retriveCategoryDataFromDBWithCategoryType : (NSString*)category_type;

+(BOOL)addCategoryToDBWithCategoryName : (NSString *)category_name description: (NSString*)category_description category_id : (int)category_id category_type :
(NSString *)category_type packedChar:(NSString *)packed_char count:(int)count;

+(BOOL) setCountOfItemsWithCategory_id : (int)category_id addOrSubstractValueNotation : (NSString*)Notation;

+(int) getLastCategory_Id;

+(BOOL) deleteCategoryWithCategory_id : (int)category_id;

+ (BOOL)getPaidStausOfCategory_id : (int) category_id;

+(BOOL)setPaidStatusofCategory_id : (int) category_id Status : (BOOL) status;

+ (CategoryTable *)getAllCategoryDescriptionDataFromDB;

+ (CategoryTable *)getCategoryDescriptionDataFromDBWithCategory_ID : (int)category_id;

*/

+(NSMutableArray*) getCategory_Name : (int)sectionNumber;

@end
