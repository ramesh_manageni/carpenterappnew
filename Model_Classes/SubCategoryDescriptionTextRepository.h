//
//  SubCategoryDescriptionTextRepository.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AbstractRepository.h"

@interface SubCategoryDescriptionTextRepository : AbstractRepository

-(NSMutableArray*) getSubCategory_Texts : (int)textId  ;

@end
