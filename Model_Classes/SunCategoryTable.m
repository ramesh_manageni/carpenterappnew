//
//  SunCategoryTable.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SunCategoryTable.h"

#import "SubCategoryDescriptionRepository.h"

@implementation SunCategoryTable

+(NSMutableArray*) getSubCategory_Name : (int)categoryId
{
    SubCategoryDescriptionRepository *subCategoryDescriptionRepositoryObj = [[SubCategoryDescriptionRepository alloc] init];
    
    return [subCategoryDescriptionRepositoryObj getSubCategory_Name : (int)categoryId];
}

@end
