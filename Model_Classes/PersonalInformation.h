//
//  PersonalInformation.h
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface PersonalInformation : NSManagedObject

@property (nonatomic, retain) NSString * centralPhoneNo;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * emailCC;
@property (nonatomic, retain) NSString * emailID;
@property (nonatomic, retain) NSString * faxDirectly;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * maleOrFemale;
@property (nonatomic, retain) NSString * mobilePhoneNo;
@property (nonatomic, retain) NSString * phoneDirectly;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * streetNo;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSSet *personalIfoToOrder;
@end

@interface PersonalInformation (CoreDataGeneratedAccessors)

- (void)addPersonalIfoToOrderObject:(OrderTable *)value;
- (void)removePersonalIfoToOrderObject:(OrderTable *)value;
- (void)addPersonalIfoToOrder:(NSSet *)values;
- (void)removePersonalIfoToOrder:(NSSet *)values;

@end
