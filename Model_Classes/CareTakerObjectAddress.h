//
//  CareTakerObjectAddress.h
//  Service48
//
//  Created by Redbytes on 10/12/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface CareTakerObjectAddress : NSManagedObject

@property (nonatomic, retain) NSString * careTakerObjectName;
@property (nonatomic, retain) NSString * centralPhoneNo;
@property (nonatomic, retain) NSString * emailID;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * maleOrFemale;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * streetNo;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSString * mobileNo;
@property (nonatomic, retain) NSSet *careTakerObjectToOrder;
@end

@interface CareTakerObjectAddress (CoreDataGeneratedAccessors)

- (void)addCareTakerObjectToOrderObject:(OrderTable *)value;
- (void)removeCareTakerObjectToOrderObject:(OrderTable *)value;
- (void)addCareTakerObjectToOrder:(NSSet *)values;
- (void)removeCareTakerObjectToOrder:(NSSet *)values;

@end
