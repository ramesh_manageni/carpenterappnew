//
//  OrderTable.h
//  Service48
//
//  Created by Redbytes on 21/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BillingAddress, CareTakerCompanyAddress, CareTakerObjectAddress, OrderDescription, OwnerAddress, PersonalInformation, PhotoDetail, PropertyAddress, TenantAddress;

@interface OrderTable : NSManagedObject

@property (nonatomic, retain) NSString * billingFirma;
@property (nonatomic, retain) NSString * careTakerFirma;
@property (nonatomic, retain) NSString * careTakerObject;
@property (nonatomic, retain) NSString * isSentMail;
@property (nonatomic, retain) NSString * lodgerName;
@property (nonatomic, retain) NSString * orderDate;
@property (nonatomic, retain) NSString * orderName;
@property (nonatomic, retain) NSString * ownerNmae;
@property (nonatomic, retain) NSSet *orderDescription;
@property (nonatomic, retain) BillingAddress *orderTableToBillingAddress;
@property (nonatomic, retain) CareTakerCompanyAddress *orderTableToCTCAddress;
@property (nonatomic, retain) CareTakerObjectAddress *orderTableToCTObjAddress;
@property (nonatomic, retain) OwnerAddress *orderTableToOwnerAddress;
@property (nonatomic, retain) PersonalInformation *orderTableToPersonalIfo;
@property (nonatomic, retain) NSSet *orderTableToPhotoDetail;
@property (nonatomic, retain) PropertyAddress *orderTableToPropertyAddress;
@property (nonatomic, retain) TenantAddress *orderTableToTenantAddress;
@end

@interface OrderTable (CoreDataGeneratedAccessors)

- (void)addOrderDescriptionObject:(OrderDescription *)value;
- (void)removeOrderDescriptionObject:(OrderDescription *)value;
- (void)addOrderDescription:(NSSet *)values;
- (void)removeOrderDescription:(NSSet *)values;

- (void)addOrderTableToPhotoDetailObject:(PhotoDetail *)value;
- (void)removeOrderTableToPhotoDetailObject:(PhotoDetail *)value;
- (void)addOrderTableToPhotoDetail:(NSSet *)values;
- (void)removeOrderTableToPhotoDetail:(NSSet *)values;

@end
