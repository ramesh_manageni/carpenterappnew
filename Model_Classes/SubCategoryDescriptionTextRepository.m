//
//  SubCategoryDescriptionTextRepository.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SubCategoryDescriptionTextRepository.h"
#import "SubCategoryText.h"

@implementation SubCategoryDescriptionTextRepository

-(NSMutableArray*) getSubCategory_Texts : (int)textId
{
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM subcategorias_textos WHERE id = %d",textId];
    //SingletonArray *sharedArray = [SingletonArray sharedArray];
    NSMutableArray *receivedObjects = [[NSMutableArray alloc]init];
    FMResultSet *resultSet = [db executeQuery:query];
    
   
    
    if ([resultSet next])
    {
        
         SubCategoryText *subCategoryText = [[SubCategoryText alloc]init];
        subCategoryText.checks1 = [resultSet stringForColumn:@"checks1"];
        subCategoryText.textto = [resultSet stringForColumn:@"texto"];
        subCategoryText.checks2 = [resultSet stringForColumn:@"checks2"];
        
        if([subCategoryText.checks2 isEqualToString: @""])
        {
            NSLog(@"Null");
        }
        [receivedObjects addObject:subCategoryText];
    }
    [resultSet close];
    return receivedObjects ;
}

@end
