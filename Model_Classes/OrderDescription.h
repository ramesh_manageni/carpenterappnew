//
//  OrderDescription.h
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable, Tasks;

@interface OrderDescription : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * subcategory;
@property (nonatomic, retain) NSString * textDescription;
@property (nonatomic, retain) OrderTable *order;
@property (nonatomic, retain) NSSet *tasks;
@end

@interface OrderDescription (CoreDataGeneratedAccessors)

- (void)addTasksObject:(Tasks *)value;
- (void)removeTasksObject:(Tasks *)value;
- (void)addTasks:(NSSet *)values;
- (void)removeTasks:(NSSet *)values;

@end
