//
//  PhotoDetail.h
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface PhotoDetail : NSManagedObject

@property (nonatomic, retain) NSString * detailText;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSString * titleText;
@property (nonatomic, retain) OrderTable *photoDetailToOrderTable;

@end
