//
//  OrderDescription.m
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OrderDescription.h"
#import "OrderTable.h"
#import "Tasks.h"


@implementation OrderDescription

@dynamic category;
@dynamic subcategory;
@dynamic textDescription;
@dynamic order;
@dynamic tasks;

@end
