//
//  OwnerAddress.m
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OwnerAddress.h"
#import "OrderTable.h"


@implementation OwnerAddress

@dynamic centralPhoneNo;
@dynamic companyName;
@dynamic emailID;
@dynamic firstName;
@dynamic landlinePhoneNo;
@dynamic lastName;
@dynamic maleOrFemale;
@dynamic mobilePhoneNo;
@dynamic ownerName;
@dynamic place;
@dynamic streetNo;
@dynamic zipCode;
@dynamic ownerAddressToOrder;

@end
