//
//  PersonalInformation.m
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PersonalInformation.h"
#import "OrderTable.h"


@implementation PersonalInformation

@dynamic centralPhoneNo;
@dynamic companyName;
@dynamic emailCC;
@dynamic emailID;
@dynamic faxDirectly;
@dynamic firstName;
@dynamic lastName;
@dynamic maleOrFemale;
@dynamic mobilePhoneNo;
@dynamic phoneDirectly;
@dynamic place;
@dynamic streetNo;
@dynamic zipCode;
@dynamic personalIfoToOrder;

@end
