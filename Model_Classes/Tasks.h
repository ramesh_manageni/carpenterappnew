//
//  Tasks.h
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderDescription;

@interface Tasks : NSManagedObject

@property (nonatomic, retain) NSString * assignedTask;
@property (nonatomic, retain) OrderDescription *orderDescription;

@end
