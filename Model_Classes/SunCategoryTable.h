//
//  SunCategoryTable.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SunCategoryTable : NSObject

@property (nonatomic) NSString *subCategoryName;
@property (assign) int textId;


+(NSMutableArray*) getSubCategory_Name : (int)categoryId;

@end
