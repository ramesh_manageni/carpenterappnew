//
//  CareTakerObjectAddress.m
//  Service48
//
//  Created by Redbytes on 10/12/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "CareTakerObjectAddress.h"
#import "OrderTable.h"


@implementation CareTakerObjectAddress

@dynamic careTakerObjectName;
@dynamic centralPhoneNo;
@dynamic emailID;
@dynamic firstName;
@dynamic lastName;
@dynamic maleOrFemale;
@dynamic place;
@dynamic streetNo;
@dynamic zipCode;
@dynamic mobileNo;
@dynamic careTakerObjectToOrder;

@end
