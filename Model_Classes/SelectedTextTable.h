//
//  SelectedTextTable.h
//  Service48
//
//  Created by Chetan Zalake on 20/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface SelectedTextTable : NSObject

@property (nonatomic) NSDictionary *selectedTextsDictionary;
@property (nonatomic) NSString *textFieldDescription;
@property (nonatomic) NSString *category;
@property (nonatomic) NSString *subCategory;
@property (nonatomic) NSManagedObject *descriptionObject;

@end
