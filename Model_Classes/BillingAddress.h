//
//  BillingAddress.h
//  Service48
//
//  Created by Redbytes on 28/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderTable;

@interface BillingAddress : NSManagedObject

@property (nonatomic, retain) NSString * billingFirmaName;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * maleOrFemale;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * streetNo;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSSet *billingAddressToOrder;
@end

@interface BillingAddress (CoreDataGeneratedAccessors)

- (void)addBillingAddressToOrderObject:(OrderTable *)value;
- (void)removeBillingAddressToOrderObject:(OrderTable *)value;
- (void)addBillingAddressToOrder:(NSSet *)values;
- (void)removeBillingAddressToOrder:(NSSet *)values;

@end
