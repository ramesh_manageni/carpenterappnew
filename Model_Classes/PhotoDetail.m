//
//  PhotoDetail.m
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PhotoDetail.h"
#import "OrderTable.h"


@implementation PhotoDetail

@dynamic detailText;
@dynamic imageURL;
@dynamic titleText;
@dynamic photoDetailToOrderTable;

@end
