//
//  CategoryTable.m
//  PackingList
//
//  Created by Chetan Zalake on 23/07/13.
//  Copyright (c) 2013 Chetan Zalake. All rights reserved.
//

#import "CategoryTable.h"
#import "CategoryDescriptionRepository.h"

@implementation CategoryTable

/*

+ (CategoryTable *)retriveCategoryDataFromDBWithCategoryType : (NSString*)category_type
                                                  {
    
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj getCategoryDescriptionDataFromDBWithCategoryType : (NSString*)category_type
                                                                             ];
}

+ (CategoryTable *)getCategoryDescriptionDataFromDBWithCategory_ID : (int)category_id
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj getCategoryDescriptionDataFromDBWithCategory_ID : (int)category_id];
}

+(BOOL)addCategoryToDBWithCategoryName : (NSString *)category_name description: (NSString*)category_description category_id : (int)category_id category_type : (NSString *)category_type packedChar:(NSString *)packed_char count:(int)count
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj addCategoryToDBWithCategoryName : (NSString *)category_name description: (NSString*)category_description category_id : (int)category_id category_type : (NSString *)category_type packedChar:(NSString *)packed_char count:(int)count
            ];
}

+ (BOOL)getPaidStausOfCategory_id : (int) category_id
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj getPaidStausOfCategory_id : (int) category_id];
}

+(BOOL)setPaidStatusofCategory_id : (int) category_id Status : (BOOL) status
{
    
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj setPaidStatusofCategory_id : (int) category_id Status : (BOOL) status];
}

+(BOOL) setCountOfItemsWithCategory_id : (int)category_id addOrSubstractValueNotation : (NSString*)Notation
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj setCountOfItemsWithCategory_id : (int)category_id addOrSubstractValueNotation : (NSString*)Notation
            ];

}

+(int) getLastCategory_Id
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj getLastCategory_Id];
}

+ (CategoryTable *)getAllCategoryDescriptionDataFromDB
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj getAllCategoryDescriptionDataFromDB];
}

+(BOOL) deleteCategoryWithCategory_id : (int)category_id
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj deleteCategoryWithCategory_id : (int)category_id];
}
*/

+(NSMutableArray*) getCategory_Name : (int)sectionNumber
{
    CategoryDescriptionRepository *categoryDescriptionRepositoryObj = [[CategoryDescriptionRepository alloc] init];
    
    return [categoryDescriptionRepositoryObj getCategory_Name : (int)sectionNumber];
}




@end
