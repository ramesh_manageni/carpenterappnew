//
//  OrderTable.m
//  Service48
//
//  Created by Redbytes on 21/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OrderTable.h"
#import "BillingAddress.h"
#import "CareTakerCompanyAddress.h"
#import "CareTakerObjectAddress.h"
#import "OrderDescription.h"
#import "OwnerAddress.h"
#import "PersonalInformation.h"
#import "PhotoDetail.h"
#import "PropertyAddress.h"
#import "TenantAddress.h"


@implementation OrderTable

@dynamic billingFirma;
@dynamic careTakerFirma;
@dynamic careTakerObject;
@dynamic isSentMail;
@dynamic lodgerName;
@dynamic orderDate;
@dynamic orderName;
@dynamic ownerNmae;
@dynamic orderDescription;
@dynamic orderTableToBillingAddress;
@dynamic orderTableToCTCAddress;
@dynamic orderTableToCTObjAddress;
@dynamic orderTableToOwnerAddress;
@dynamic orderTableToPersonalIfo;
@dynamic orderTableToPhotoDetail;
@dynamic orderTableToPropertyAddress;
@dynamic orderTableToTenantAddress;

@end
