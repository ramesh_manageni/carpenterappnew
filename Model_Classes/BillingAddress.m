//
//  BillingAddress.m
//  Service48
//
//  Created by Redbytes on 28/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "BillingAddress.h"
#import "OrderTable.h"


@implementation BillingAddress

@dynamic billingFirmaName;
@dynamic companyName;
@dynamic firstName;
@dynamic lastName;
@dynamic maleOrFemale;
@dynamic place;
@dynamic streetNo;
@dynamic zipCode;
@dynamic email;
@dynamic billingAddressToOrder;

@end
