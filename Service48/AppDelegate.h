//
//  AppDelegate.h
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.


#import <UIKit/UIKit.h>

#import "SplashScreenViewController.h"
#import "ListOrdersViewController.h"
#import "PhotoTabViewController.h"
#import "WikiTabViewController.h"
#import "AddressTabViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIImageView*imageView;
}
@property(strong,nonatomic)NSArray *fetchedRecordArray;

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic)UINavigationController *objNvc;
@property (strong,nonatomic)UITabBarController *objTab;
@property(strong ,nonatomic)UINavigationController *rvc_orders;
@property(strong ,nonatomic)UINavigationController *rvc_photo;
@property(strong ,nonatomic)UINavigationController *rvc_wiki;
@property(strong ,nonatomic)UINavigationController *rvc_address;

@property(strong,nonatomic)ListOrdersViewController *objListOrdersViewController;
@property(strong,nonatomic)PhotoTabViewController *objPhotoTabViewController;
@property (strong,nonatomic)WikiTabViewController *objWikiTabViewController;
@property(strong ,nonatomic)AddressTabViewController *objAddressTabViewController;


@property(strong,nonatomic)SplashScreenViewController *objSplashScreenViewController;

//sqlite
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end
