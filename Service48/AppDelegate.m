//
//  AppDelegate.m
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AppDelegate.h"
#import "Constant.h"
#import "DatabaseMigrator.h"
#import "AbstractRepository.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize fetchedRecordArray;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    else
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [[UITextView appearance] setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    }
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    [self makeTabBar];
    [self addInitialVIew];
    [self setupDb];
    self.window.backgroundColor = [UIColor whiteColor];
    propertyAddressArray=[[NSMutableArray alloc] init];
    tempOrderArray=[[NSMutableArray alloc] init];
    return YES;
}

- (void)setupDb
{
    DatabaseMigrator *migrator = [[DatabaseMigrator alloc] initWithDatabaseFile:[AbstractRepository databaseFilename]];
    [migrator moveDatabaseToUserDirectoryIfNeeded];
}

//For creating custom tabbar
-(void)makeTabBar
{
    _objTab=[[UITabBarController alloc] init];
    [_objTab.tabBar setBackgroundImage:[UIImage imageNamed:@"tabbarBG.png"]];
    
    
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
    _objListOrdersViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListOrdersViewController"];
    
    [_objListOrdersViewController.tabBarItem setTitle:@"Aufträge"];

    if ([[UIImage imageNamed:@"order1.png"] respondsToSelector:@selector(imageWithRenderingMode:)])
    {
        [_objListOrdersViewController.tabBarItem setImage:[[UIImage imageNamed:@"order1.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [_objListOrdersViewController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"orderSelected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else
    {
     [_objListOrdersViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"orderSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"order1.png"]];
    }
    
    _objListOrdersViewController.tabBarItem.tag=0;
    _rvc_orders=[[UINavigationController alloc]initWithRootViewController:_objListOrdersViewController];
    

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    _objPhotoTabViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PhotoTabViewController"];
    
    if ([[UIImage imageNamed:@"photo1.png"] respondsToSelector:@selector(imageWithRenderingMode:)])
    {
        [_objPhotoTabViewController.tabBarItem setImage:[[UIImage imageNamed:@"photo1.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [_objPhotoTabViewController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"photoSelected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else
    {
        [_objPhotoTabViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"photoSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"photo1.png"]];
    }


    [_objPhotoTabViewController.tabBarItem setTitle:@"Fotos"];
    _objPhotoTabViewController.tabBarItem.tag=1;
    _rvc_photo=[[UINavigationController alloc] initWithRootViewController:_objPhotoTabViewController];
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    _objWikiTabViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"WikiTabViewController"];
    
    [_objWikiTabViewController.tabBarItem setTitle:@"Wiki"];

    if ([[UIImage imageNamed:@"wiki1.png"] respondsToSelector:@selector(imageWithRenderingMode:)])
    {
        [_objWikiTabViewController.tabBarItem setImage:[[UIImage imageNamed:@"wiki1.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [_objWikiTabViewController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"wikiSelected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
    }
    else
    {
        [_objWikiTabViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"wikiSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"wiki1.png"]];
    }
    
    
    _objWikiTabViewController.tabBarItem.tag=2;
    _rvc_wiki=[[UINavigationController alloc]initWithRootViewController:_objWikiTabViewController];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    _objAddressTabViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressTabViewController"];
    
    [_objAddressTabViewController.tabBarItem setTitle:@"Adressen"];

    if ([[UIImage imageNamed:@"address1.png"] respondsToSelector:@selector(imageWithRenderingMode:)])
    {
        [_objAddressTabViewController.tabBarItem setImage:[[UIImage imageNamed:@"address1.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [_objAddressTabViewController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"addressSelected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
        
    }
    else
    {
        [_objAddressTabViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"addressSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"address1.png"]];
    }


    
    _objAddressTabViewController.tabBarItem.tag=3;
    _rvc_address=[[UINavigationController alloc] initWithRootViewController:_objAddressTabViewController];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    NSArray* controllers = [NSArray arrayWithObjects: _rvc_orders,_rvc_photo,_rvc_wiki,_rvc_address,nil];
    [_objTab setViewControllers:controllers animated:NO];
    
    for(UIViewController *tab in  _objTab.viewControllers)
        
    {
        [tab.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIFont fontWithName:@"Helvetica-Bold" size:11.0], UITextAttributeFont,    nil]
                                      forState:UIControlStateNormal];
        
        [tab.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor whiteColor], UITextAttributeTextColor,     nil]
                                      forState:UIControlStateSelected];
    }
}

//To Add Splash Screen
-(void)addInitialVIew
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _objSplashScreenViewController=[storyBoard instantiateViewControllerWithIdentifier:@"SplashScreenViewController"];
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [self saveContext];
    
}
//To save data to database
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LocalDatabaseModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LocalDatabaseModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



@end
