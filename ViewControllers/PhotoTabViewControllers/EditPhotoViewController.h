//
//  EditPhotoViewController.h
//  Service48
//
//  Created by Redbytes on 20/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"PhotoDetail.h"
#import"TGRImageViewController.h"
@interface EditPhotoViewController : UIViewController<UIGestureRecognizerDelegate,UIViewControllerTransitioningDelegate,UITextViewDelegate>


@property(strong,nonatomic) NSManagedObject *record;

@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) UIImage *orderImage;

@property (strong, nonatomic) IBOutlet UILabel *bottomTitleLabel;

@property (strong, nonatomic) IBOutlet UITextView *freetextTextView;
@property (strong, nonatomic) IBOutlet UIImageView *orderImageView;

@property(strong,nonatomic) NSString *freeText;

@end
