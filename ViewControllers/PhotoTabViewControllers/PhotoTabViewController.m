//
//  PhotoTabViewController.m
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PhotoTabViewController.h"
#import "PhotoCustomCell.h"
#import "PhotoDetail.h"

@interface PhotoTabViewController ()

@property (nonatomic,strong)UIImage *orderImage;

@property (nonatomic,strong)NSMutableArray* fetchedRecordsArray;
@property (nonatomic, strong)NSMutableDictionary *recordDict;
@property (nonatomic, strong)NSMutableDictionary *masterDict;

@property (strong, nonatomic) IBOutlet UIButton *takePhotoButtonClicked;
@property (strong, nonatomic) IBOutlet UIButton *importPhotoButtonClicked;
@property (strong, nonatomic) IBOutlet UITableView *objTableView;

@end

@implementation PhotoTabViewController
@synthesize recordDict,orderImage,masterDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
        
    }
    //Set Tabbar delegate
    self.tabBarController.delegate=self;
    
    //Dictionary used to store(temporary) address information
    recordDict=[[NSMutableDictionary alloc] init];
    masterDict=[[NSMutableDictionary alloc] init];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:@"textfieldBG.png"] forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [button setTitle:@"Keine Foto vorhanden" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    button.frame = CGRectMake(0, 0.0, 280.0, 45.0);

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Fotos" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgImage setImage:[UIImage imageNamed:@"bluebg.jpg"]];
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];

    //
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    self.fetchedRecordsArray=[self getEntityCount];
    if(self.fetchedRecordsArray.count==0)
    {
        [self.objTableView addSubview:button];
        
    }
    else
    {
        [button removeFromSuperview];
    }
   
    if(self.fetchedRecordsArray.count>0)
    {
        self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
        
        [self.objTableView setEditing:NO];

        
    }
    if(!PHOTO_FLAG)
    {
        self.navigationItem.leftBarButtonItem = nil;
        
    }
    
    [self.objTableView reloadData];
    
    if(isOrderSentPhoto)
    {
        self.navigationItem.rightBarButtonItem = nil;
       // self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
        
        NSArray *subviews=self.view.subviews;
        for(UIView *view in subviews)
        {
            if([view isKindOfClass:[UIButton class]])
            {
                [(UIButton *)view setEnabled:NO];
            }

            for(UIView *textField in view.subviews)
                {
                    if([textField isKindOfClass:[UIButton class]])
                    {
                        [(UIButton *)textField setEnabled:NO];
                    }
                    
                }
            
        }
        
        
    }
    else
    {
        NSArray *subviews=self.view.subviews;
        for(UIView *view in subviews)
        {
            if([view isKindOfClass:[UIButton class]])
            {
                [(UIButton *)view setEnabled:YES];
            }
            
            for(UIView *textField in view.subviews)
            {
                if([textField isKindOfClass:[UIButton class]])
                {
                    [(UIButton *)textField setEnabled:YES];
                }
                
            }
            
        }
        
   
    }
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  sichern" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"bearbeiten" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(toggleEdit) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    PHOTO_FLAG=0;
    isOrderSentPhoto=NO;
    
    [self.tabBarController setSelectedIndex:0];
}

//Method to change title of Edit/Done button
-(void)toggleEdit
{
    [self.objTableView reloadData];

    [self.objTableView setEditing:!self.objTableView.editing animated:NO];
    
    if (self.objTableView.editing)
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"fertig" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(30, 3, 70, 30)];

    }
    
    else
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"bearbeiten" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(10, 3, 95, 30)];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.fetchedRecordsArray.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *) indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}



//// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
   // self.fetchedRecordsArray=[self getEntityCount];

    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        deleteIndexPath=indexPath;
        [self showAlert];
     }

}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    PhotoCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
    if (cell == nil) {
        cell = [[PhotoCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    BOOL fileExists=[[NSFileManager defaultManager] fileExistsAtPath:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] imageURL]];
    
    if (fileExists)
    {
        cell.orderImageView.image=[UIImage imageWithContentsOfFile:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] imageURL]];
    }
    if(!isSecureButtonClicked)
    {
        
        cell.titleTextLabel.text=[[self.fetchedRecordsArray objectAtIndex:indexPath.row] titleText];
       
    }
    else
    {
        cell.titleTextLabel.text=[[self.fetchedRecordsArray objectAtIndex:indexPath.row] titleText];

    }
    
   
    cell.freeTextView.text=[[self.fetchedRecordsArray objectAtIndex:indexPath.row] detailText];

    [cell.freeTextView setEditable:NO];
        [cell setBackgroundColor:[UIColor clearColor]];
    //Disable selection
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    [cell.imageBackgroundImageView setFrame:CGRectMake(0, 0, 85, 70)];
    [cell.orderImageView setFrame:CGRectMake(0, 0, 82, 69)];
    [cell.titleTextLabel setFrame:CGRectMake(99, 8, 168, 21)];
    [cell.freeTextView setFrame:CGRectMake(93, 32, 168, 36)];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"textbox.png"]]];
}

#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditPhotoViewController *objEditPhotoViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditPhotoViewController"];
    objEditPhotoViewController.orderImage=[UIImage imageWithContentsOfFile:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] imageURL]];
    objEditPhotoViewController.freeText=[[self.fetchedRecordsArray objectAtIndex:indexPath.row] detailText];
    objEditPhotoViewController.record=[self.fetchedRecordsArray objectAtIndex:indexPath.row];
 
    if(isOrderSentPhoto)
    {
        [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Sie nicht bearbeiten können Zugestellte Aufträge." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
        
    }
    else
    {

    [self.navigationController pushViewController:objEditPhotoViewController animated:NO];
    }
    
}

#pragma mark takePhoto method
- (IBAction)takePhotoButtonClicked:(id)sender
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Fehler"
                                                              message:@"Gerät hat keine Kamera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"akzeptieren"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

// use photo from PhotoLibrary
- (IBAction)importPhotoButtonClicked:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark UIImagePickerControllerDelegate
- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    orderImage=image;
    //Go To OrderList Tab
    
    
    EditPhotoViewController *objEditPhotoViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditPhotoViewController"];
    objEditPhotoViewController.orderImage=image;
    [self.navigationController pushViewController:objEditPhotoViewController animated:NO];
    
    if(!PHOTO_FLAG)
    {
        isFromPhotoTab=1;
        [self.tabBarController setSelectedIndex:0];
        
        [[self.tabBarController.viewControllers objectAtIndex:0] popToRootViewControllerAnimated:YES];
    }
    
 
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)saveOrderImageToDocDirectory
{
    NSFileManager *fm = [[NSFileManager alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    int num;
    num=0;
    
    do
    {
        NSString  *savedImagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%d.png",MASTER_RECORD.orderTableToOwnerAddress.firstName,num]];
        num += 1; // for next time
        
        if ( ![fm fileExistsAtPath: savedImagePath] )
        {
            // save your image here using savedImagePath
            [recordDict setValue:savedImagePath forKey:@"imageURL"];
            
            NSData* data = UIImagePNGRepresentation(orderImage);
            [data writeToFile:savedImagePath atomically:YES];
            
            break;
        }
        
    } while (num);
    
    
}



#pragma mark Database Method
//Add address information to the database
-(void)insertRecord
{
      AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    PhotoDetail * record=[NSEntityDescription insertNewObjectForEntityForName:@"PhotoDetail" inManagedObjectContext:context];
    
    if(![[recordDict valueForKey:@"imageURL"] isEqual:[NSNull null]])
    {
        record.imageURL=[recordDict valueForKey:@"imageURL"];
    }
    
    [MASTER_RECORD addOrderTableToPhotoDetailObject:record];
    
    NSError *err;
    
    if( ! [context save:&err] )
    {
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    
}

-(NSMutableArray *)getEntityCount
{

    if(isSecureButtonClicked)
    {
        return [DatabaseHandler getEntityCountWithName:@"PhotoDetail"];
    }
    return [[MASTER_RECORD.orderTableToPhotoDetail allObjects] mutableCopy];
}

#pragma mark Tabbar Delegate
- (void)tabBarController:(UITabBarController *)tabBarController
 didSelectViewController:(UIViewController *)viewController
{
    if(!isOrderSentPhoto)
    {
    isPhotoTabSelected=1;
    isSecureButtonClicked=1;
    popToRoot=YES;
    
    self.fetchedRecordsArray=[self getEntityCount];
    
    if(self.fetchedRecordsArray.count>0)
    {
        self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
        
    }

    [self.navigationController popToRootViewControllerAnimated:NO];
    [self.objTableView reloadData];
    
    self.navigationItem.leftBarButtonItem = nil;

    isPhotoTabSelected=0;
    PHOTO_FLAG=0;
    
    int tabitem = (int)tabBarController.selectedIndex;
    [[tabBarController.viewControllers objectAtIndex:tabitem] popToRootViewControllerAnimated:NO];
    }
    else
    {
       // self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];

    }
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSLog(@"controller class: ");

}

#pragma mark UIAlertView delegate
- (void)showAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Foto löschen" message:@"Wollen Sie das Foto wirklich löschen?" delegate:self cancelButtonTitle:@"Löschen" otherButtonTitles:@"Abbrechen", nil];
    
    alert.delegate=self;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)//OK button pressed
    {
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        
        
        //    1
        [self.objTableView beginUpdates];
        // Delete the row from the data source
        
        //    2
        
        [context deleteObject:[self.fetchedRecordsArray objectAtIndex:deleteIndexPath.row]];
        
        [self.objTableView deleteRowsAtIndexPaths:@[deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        BOOL fileExists=[[NSFileManager defaultManager] fileExistsAtPath:[[self.fetchedRecordsArray objectAtIndex:deleteIndexPath.row] imageURL]];
        
        if (fileExists)
        {
            [[NSFileManager defaultManager] removeItemAtPath:[[self.fetchedRecordsArray objectAtIndex:deleteIndexPath.row] imageURL] error:NULL];
            
            NSLog(@"Image Removed");
        }
        
        
        //    3
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        else
        {
            
            self.fetchedRecordsArray=[self getEntityCount];
            if(self.fetchedRecordsArray.count==0)
            {
                [button removeFromSuperview];
                
                [self.objTableView addSubview:button];
                
            }
            else
            {
                [button removeFromSuperview];
            }
            
            if(self.fetchedRecordsArray.count==0)
            {
                self.navigationItem.rightBarButtonItem = nil;
                [self.objTableView setEditing:NO];
            }
            
        }
        //    4
        
        //    5
        [self.objTableView endUpdates];
        [self.objTableView reloadData];
        

    }
}
@end
