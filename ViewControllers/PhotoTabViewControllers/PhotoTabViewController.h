//
//  PhotoTabViewController.h
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoCustomCell.h"
#import "PhotoDetail.h"
#import"EditPhotoViewController.h"

@interface PhotoTabViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITabBarControllerDelegate>
{
    UIButton *button;
    NSIndexPath *deleteIndexPath;

}


- (IBAction)takePhotoButtonClicked:(id)sender;//To take photo from camera

- (IBAction)importPhotoButtonClicked:(id)sender;//To import photo from device

@end
