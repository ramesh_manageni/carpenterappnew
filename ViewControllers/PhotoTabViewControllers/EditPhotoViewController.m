//
//  EditPhotoViewController.m
//  Service48
//
//  Created by Redbytes on 20/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "EditPhotoViewController.h"
#import "TGRImageZoomAnimationController.h"
#import "Constant.h"
#import "NavigationUtility.h"
#import "DatabaseHandler.h"
@interface EditPhotoViewController ()

@property (nonatomic, strong)NSMutableDictionary *recordDict;

@end

@implementation EditPhotoViewController

@synthesize recordDict,record,freeText;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.freetextTextView.text=freeText;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    singleTap.delegate = self;
    [self.orderImageView addGestureRecognizer:singleTap];
    self.objScrollView.contentSize = CGSizeMake(320,1500);
    [self.objScrollView setNeedsDisplay];

    [self.objScrollView setCanCancelContentTouches:YES];
    self.orderImageView.userInteractionEnabled = YES; //disabled by default
    
    
    //Dictionary used to store(temporary) address information
    recordDict=[[NSMutableDictionary alloc] init];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    [self.view setBackgroundColor:[UIColor blueColor]];
}

-(void)oneTap
{
    TGRImageViewController *objTGRImageViewController = [[TGRImageViewController alloc] initWithImage:self.orderImage];
    if (!SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        objTGRImageViewController.transitioningDelegate = self;
    }
    
    [self.navigationController pushViewController:objTGRImageViewController animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Fotos" withFontNmae:@"Helvetica-Bold" andSize:18.0];

    if(isSecureButtonClicked)
    {
        if(MASTER_RECORD_PHOTO.orderName!=nil)
        {
            self.titleLabel.text=[NSString stringWithFormat:@"%@",MASTER_RECORD_PHOTO.orderName];
            
        }
        else
        {
           self.titleLabel.text=@"(Ohne Adresse)";
            
        }
        
        
        
    }
    else
    {
        if(MASTER_RECORD.orderName!=nil)
        {
            self.titleLabel.text=[NSString stringWithFormat:@"%@",MASTER_RECORD.orderName];
            
        }
        else
        {
            self.titleLabel.text=@"(Ohne Adresse)";
            
        }
        
        
    }

    
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    self.orderImageView.image=self.orderImage;
    if(isPhotoTabSelected)
    {
        self.navigationItem.leftBarButtonItem=nil;
        [self.navigationItem setHidesBackButton: YES animated: NO];

    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

#pragma mark back baar Button
-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    self.objScrollView.contentOffset = CGPointMake(0,0);

    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark save baar Button
-(void)saveButtonClicked
{
    self.objScrollView.contentOffset = CGPointMake(0,0);
 
        [self saveOrderImageToDocDirectory];
        [self insertRecord];
        [self.navigationController popViewControllerAnimated:NO];

}



#pragma mark Database Method

-(void)saveOrderImageToDocDirectory
{
    NSFileManager *fm = [[NSFileManager alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    int num;
    num=0;
    
    do
    {
        NSString  *savedImagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Foto-%d.png",num]];
        num += 1; // for next time
        
        if ( ![fm fileExistsAtPath: savedImagePath] )
        {
            // save your image here using savedImagePath
            [recordDict setValue:savedImagePath forKey:@"imageURL"];
            
            NSData* data = UIImagePNGRepresentation(self.orderImage);
            [data writeToFile:savedImagePath atomically:YES];
            
            break;
        }
        
    } while (num);
    
    
}


//Add address information to the database
-(void)insertRecord
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    PhotoDetail * recordNew;
  
    if(record!=nil)
    {
        recordNew=(PhotoDetail *)record;
        record=nil;
    }
    else
    {
        
        recordNew=[NSEntityDescription insertNewObjectForEntityForName:@"PhotoDetail" inManagedObjectContext:context];

    }
    if(![[recordDict valueForKey:@"imageURL"] isEqual:[NSNull null]])
    {
        recordNew.imageURL=[recordDict valueForKey:@"imageURL"];
    }
    if(![self.freetextTextView.text isEqual:[NSNull null]])
    {
        recordNew.detailText=self.freetextTextView.text;
    }
    
    if(isSecureButtonClicked)
    {
        if(MASTER_RECORD_PHOTO.orderName!=nil)
        {
            recordNew.titleText=[NSString stringWithFormat:@"%@",MASTER_RECORD_PHOTO.orderName];

        }
        else
        {
            recordNew.titleText=@"(Ohne Adresse)";

        }
        
       
        [MASTER_RECORD_PHOTO addOrderTableToPhotoDetailObject:recordNew];

    }
    else
    {
        if(MASTER_RECORD.orderName!=nil)
        {
            recordNew.titleText=[NSString stringWithFormat:@"%@",MASTER_RECORD.orderName];
            
        }
        else
        {
            recordNew.titleText=@"(Ohne Adresse)";
            
        }
        
        [MASTER_RECORD addOrderTableToPhotoDetailObject:recordNew];
        
    }
    
    NSError *err;
    
    if( ! [context save:&err] )
    {
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    
}

#pragma mark UITextViewDelegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    //Check screen height
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                self.objScrollView.contentOffset = CGPointMake(0,300);

            }
            
            else if (pixelHeight == 1136.0f)
            {
                self.objScrollView.contentOffset = CGPointMake(0,200);

            }
        }
    }

    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
      //  self.objScrollView.contentOffset = CGPointMake(0,0);

        return NO;
    }
    
    return YES;
}

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}
@end
