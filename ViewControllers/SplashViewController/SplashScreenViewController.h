//
//  SplashScreenViewController.h
//  Service48
//
//  Created by Redbytes on 23/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashScreenViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageViewSplash;

- (IBAction)onClickContact:(id)sender;//link to address tab

- (IBAction)onClickOrderBtn:(id)sender;//link to Order tab

@end
