//
//  SplashScreenViewController.m
//  Service48
//
//  Created by Redbytes on 23/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "AppDelegate.h"
#import "PersonalInfoViewController.h"
@interface SplashScreenViewController ()

@property(strong,nonatomic)NSArray *fetchedRecordArray;

@end

@implementation SplashScreenViewController

@synthesize imageViewSplash;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Button For Order #1
    UIButton *buttonOrder1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonOrder1 addTarget:self
                    action:@selector(onClickOrderBtn:)
          forControlEvents:UIControlEventTouchUpInside];
    
    //Button For Order #2
    UIButton *buttonOrder2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonOrder2 addTarget:self
                    action:@selector(onClickOrderBtn:)
          forControlEvents:UIControlEventTouchUpInside];

    //Button For Contact
    UIButton *buttonContact = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonContact addTarget:self
                    action:@selector(onClickContact:)
          forControlEvents:UIControlEventTouchUpInside];

    //Check screen height
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                self.imageViewSplash.image=[UIImage imageNamed:@"splash3.5.png"];
                
                //Change button frame
                buttonOrder1.frame = CGRectMake(77, 22, 166, 113);
                buttonOrder2.frame = CGRectMake(77, 200, 166, 113);
                buttonContact.frame = CGRectMake(77, 341, 166, 113);
                
               

            }
            
            else if (pixelHeight == 1136.0f)
            {
                self.imageViewSplash.image=[UIImage imageNamed:@"splash4.0.png"];
                
                //Change button frame
                buttonOrder1.frame = CGRectMake(77, 62, 166, 113);
                buttonOrder2.frame = CGRectMake(77, 245, 166, 113);
                buttonContact.frame = CGRectMake(77, 385, 166, 113);
                
            }
        }
    }
    [self.view addSubview:buttonOrder1];
    [self.view addSubview:buttonOrder2];
    [self.view addSubview:buttonContact];

    
   }
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
    
    self.fetchedRecordArray= [DatabaseHandler getEntityCountWithName:@"PersonalInformation"];
    NSLog(@" Personal informatin araay %lu",(unsigned long)self.fetchedRecordArray.count);
    if(self.fetchedRecordArray.count==0)
    {
        
        PersonalInfoViewController *objPersonalInfoViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                                                   instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
        [self.navigationController pushViewController:objPersonalInfoViewController animated:NO];
        //[self.view addSubview:objPersonalInfoViewController.view];
        [self showAlert];
        
    }


    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickContact:(id)sender
{
    
    UIDevice *device = [UIDevice currentDevice];
    
    if ([[device model] isEqualToString:@"iPhone"] ) {
        
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"+41800554848"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        
    } else {
        
        [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Ihr Gerät diese Funktion nicht unterstützt." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
    }
}


- (IBAction)onClickOrderBtn:(id)sender
{
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [appdelegte.objNvc.view removeFromSuperview];
    [appdelegte.window addSubview:appdelegte.objTab.view];
    [appdelegte.objTab setSelectedIndex:0];
    appdelegte.objNvc=nil;


}
- (void)showAlert
{
    [[[UIAlertView alloc] initWithTitle:@"Willkommen bei Service 48" message:@"Für ein korrektes Funktionieren der Applikation ist es notwendig, dass Sie Ihre persönlichen Daten angeben." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}
@end
