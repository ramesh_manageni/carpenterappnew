// Nihal
//  OrderSubCategoriesTextViewController.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@interface OrderSubCategoriesTextViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,BSKeyboardControlsDelegate >


@property(nonatomic, assign) int categoryId;
@property(nonatomic, strong) NSString *categoryName;
@property(nonatomic, strong) NSString *subCategoryName;
@property(nonatomic, assign) int subCategoryId;
@property(nonatomic, assign) int sectionNumber;
@property(nonatomic, assign) int textId;


@property BOOL showRightBarButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
