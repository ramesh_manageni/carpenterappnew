//
//  OrderSubCategoriesTextViewController.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OrderSubCategoriesTextViewController.h"
#import "SubCategoryText.h"
#import "OrderDescriptionViewController.h"
#import "SelectedTextTable.h"
#import "OrderDescription.h"
#import "OrderTable.h"
#import "Tasks.h"


#define MAX_HEIGHT 50

@interface OrderSubCategoriesTextViewController ()


@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@property (strong, nonatomic) NSMutableArray *fetchedData;
@property (strong, nonatomic) NSArray *subCategoriesTextArray;
@property (strong, nonatomic) NSArray *subCategoriesTextArraySecondChecks;
@property (strong, nonatomic) NSString *descriptionText;
@property (nonatomic)  SubCategoryText *subCategoryText;
@property (nonatomic) NSMutableArray *selectedText;
@property (nonatomic) AppContext* appContext;
@property (nonatomic) UITextField *textField;
@property (nonatomic) UILabel *freiTextLabel;

@end

@implementation OrderSubCategoriesTextViewController

@synthesize showRightBarButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    _selectedText = [[NSMutableArray alloc]init];
    
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    _appContext = [AppContext sharedAppContext];
    _textField.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self fetchSubCategoriesText];
    // Do any additional setup after loading the view.
    //Set keyboard controls to all textfields
    //Avoid cell colapse
    CGFloat dummyViewHeight = 40;
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, dummyViewHeight)];
    self.tableView.tableHeaderView = dummyView;
    self.tableView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0);
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle: _categoryName withFontNmae:@"Helvetica-Bold" andSize:18.0];
    
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    
    if(selectedSectionNumber==_sectionNumber)
    {
        [bgImage setImage:[UIImage imageNamed:@"HG_Schriener.jpg"]];
    }
    else if (selectedSectionNumber+1==_sectionNumber)
    {
        [bgImage setImage:[UIImage imageNamed:@"HG_Parkett.jpg"]];
    }
    
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
    
    if(showRightBarButton)
    {
        self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
        
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
}

- (void)keyboardDidShow: (NSNotification *) notif{
    //Keyboard becomes visible
    NSLog(@"%@",NSStringFromCGRect(self.tableView.frame));
    
    //Check screen height
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
               if(self.tableView.frame.size.height>200)
               {
                self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                                  self.tableView.frame.origin.y,
                                                  self.tableView.frame.size.width,
                                                  self.tableView.frame.size.height - 170);   //move up
               }

            }
            
            else if (pixelHeight == 1136.0f)
            {
                if(self.tableView.frame.size.height>300)
                {
                self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                                  self.tableView.frame.origin.y,
                                                  self.tableView.frame.size.width,
                                                  self.tableView.frame.size.height - 170);   //move up
                }

            }
        }
    }

    
    
    NSLog(@"%@",NSStringFromCGRect(self.tableView.frame));

}

- (void)keyboardDidHide: (NSNotification *) notif{
    //keyboard will hide
    //Check screen height
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                if(self.tableView.frame.size.height<200)
                {
                    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                                      self.tableView.frame.origin.y,
                                                      self.tableView.frame.size.width,
                                                      self.tableView.frame.size.height + 170);   //move up
                }
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                if(self.tableView.frame.size.height<300)
                {
                    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                                      self.tableView.frame.origin.y,
                                                      self.tableView.frame.size.width,
                                                      self.tableView.frame.size.height + 170);   //move up
                }
                
            }
        }
    }
    
    

    
}

- (void)fetchSubCategoriesText
{
    _fetchedData = [SubCategoryText getSubCategory_Texts:_textId];
    
    _subCategoryText = [_fetchedData objectAtIndex:0];
    
    _descriptionText = _subCategoryText.textto;
    _subCategoriesTextArray = [_subCategoryText.checks1 componentsSeparatedByString:@"|"];
    
    if(![_subCategoryText.checks2 isEqualToString:@""])
        _subCategoriesTextArraySecondChecks = [_subCategoryText.checks2 componentsSeparatedByString:@"|"];
    
    NSLog(@"SUBCATEGORY: %lu",(unsigned long)_subCategoriesTextArraySecondChecks.count);
    
    [self.tableView reloadData];
    
}

#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_textId != 48)
    {
        return 4;
    }
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return _subCategoriesTextArray.count;
    }
    else if (section == 1)
    {
        return _subCategoriesTextArraySecondChecks.count;
    }
    else
        return 1;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellIdentifier = [NSString stringWithFormat:@"cell%ld",(long)indexPath.section];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    
    if(_textId != 48)
    {
        
        if(indexPath.section == 0)
        {
            NSLog(@"indexPath : %ld",(long)indexPath.section);
            
            NSRange r;
            NSString *subcategotyText = [_subCategoriesTextArray objectAtIndex:indexPath.row];
            while ((r = [subcategotyText rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
                subcategotyText = [subcategotyText stringByReplacingCharactersInRange:r withString:@""];
            
            cell.textLabel.text = subcategotyText;
            
            NSLog(@"SUB TEXT %@",[_subCategoriesTextArray objectAtIndex:indexPath.row]);
            
            //[cell.textLabel.text setValue:[_subCategoriesTextArray objectAtIndex:indexPath.row] forKey:@"contentToHTMLString"];
            cell.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
            
            if([_selectedText containsObject:cell.textLabel.text])
            {
                cell.imageView.image = [UIImage imageNamed:@"check1.png"];
            }
            else
            {
                cell.imageView.image = [UIImage imageNamed:@"uncheck1.png"];
            }
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
            line.backgroundColor = [UIColor darkGrayColor];
            [cell addSubview:line];
            //[cell bringSubviewToFront:line];
            // cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, cell.bounds.size.width);
            
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                UIView *myView = [[UIView alloc] init];
                myView.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
                cell.backgroundView = myView;
            }
        }
        else if (indexPath.section == 1)
        {
            NSLog(@"indexPath : %ld",(long)indexPath.section);
            
            NSRange r;
            NSString *subcategotyText = [_subCategoriesTextArraySecondChecks objectAtIndex:indexPath.row];
            while ((r = [subcategotyText rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
                subcategotyText = [subcategotyText stringByReplacingCharactersInRange:r withString:@""];
            
            
            
            NSLog(@"SUB TEXT %@",[_subCategoriesTextArraySecondChecks objectAtIndex:indexPath.row]);
            
            
            
            if(_subCategoriesTextArraySecondChecks.count > 0)
            {
                
                cell.textLabel.text = subcategotyText;
                cell.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
                
                if([_selectedText containsObject:cell.textLabel.text])
                {
                    cell.imageView.image = [UIImage imageNamed:@"check1.png"];
                }
                else
                {
                    cell.imageView.image = [UIImage imageNamed:@"uncheck1.png"];
                }
            }
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
            line.backgroundColor = [UIColor darkGrayColor];
            [cell addSubview:line];
            
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                UIView *myView = [[UIView alloc] init];
                myView.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
                cell.backgroundView = myView;
            }
            
        }
        else if (indexPath.section == 2)
        {
            cell.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
            _freiTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 60, 36)];
            _freiTextLabel.text = @"Freitext";
            _freiTextLabel.font = [UIFont systemFontOfSize:16];
            [cell addSubview:_freiTextLabel];
            
            _textField = [[UITextField alloc]initWithFrame:CGRectMake(70, 5, 225, 36)];
            [_textField setBorderStyle:UITextBorderStyleBezel];
            _textField.backgroundColor = [UIColor whiteColor];
            //_textField.autocorrectionType = UITextAutocorrectionTypeNo;
            
            [[_textField valueForKey:@"textInputTraits"] setValue:[UIColor blueColor] forKey:@"insertionPointColor"];
            
            [cell addSubview:_textField];
            
            NSArray *fields = @[self.textField];
            
            [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
            [self.keyboardControls setDelegate:self];
            
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                [_freiTextLabel setBackgroundColor:[UIColor clearColor]];
                UIView *myView = [[UIView alloc] init];
                myView.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
                cell.backgroundView = myView;
            }
            else
            {
                [_textField setTintColor:[UIColor blueColor]];
            }
        }
        else//(indexPath.section == 2)
        {
            CGSize cellSize = [_descriptionText sizeWithFont:[UIFont systemFontOfSize:18]
                                           constrainedToSize:CGSizeMake(300, 2500)
                                               lineBreakMode:NSLineBreakByWordWrapping];
            
            CGFloat cellHeight = cellSize.height;
            
            UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 300, cellHeight)];
            
            [webView loadHTMLString:[NSString stringWithFormat:@"<font face='Helvetica' size='3'>%@", _descriptionText] baseURL:nil];
            webView.scrollView.scrollEnabled = NO;
            webView.opaque = NO;
            webView.scrollView.bounces = NO;
            [webView setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:webView];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.backgroundColor = [UIColor clearColor];
            
        }
    }
    else
    {
        CGSize cellSize = [_descriptionText sizeWithFont:[UIFont systemFontOfSize:18]
                                       constrainedToSize:CGSizeMake(300, 2500)
                                           lineBreakMode:NSLineBreakByWordWrapping];
        
        CGFloat cellHeight = cellSize.height;
        
        UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 300, cellHeight)];
        
        [webView loadHTMLString:[NSString stringWithFormat:@"<font face='Helvetica' size='3'>%@", _descriptionText] baseURL:nil];
        webView.scrollView.scrollEnabled = NO;
        webView.opaque = NO;
        webView.scrollView.bounces = NO;
        [webView setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:webView];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(indexPath.section == 3 || _textId == 48)
    {
        CGSize cellSize = [_descriptionText sizeWithFont:[UIFont systemFontOfSize:18]
                                       constrainedToSize:CGSizeMake(300, 2000)
                                           lineBreakMode:NSLineBreakByWordWrapping];
        
        CGFloat cellHeight = cellSize.height;
        return cellHeight;
    }
    else if(indexPath.section == 0)
    {
        CGSize cellSize = [[_subCategoriesTextArray objectAtIndex:indexPath.row] sizeWithFont:[UIFont systemFontOfSize:18]
                                                                            constrainedToSize:CGSizeMake(300, 2000)
                                                                                lineBreakMode:NSLineBreakByWordWrapping];
        
        CGFloat cellHeight = cellSize.height;
        
        if(cellHeight<35)
        {
            return 45;
        }
        return cellHeight;
    }
    else if(indexPath.section == 1)
    {
        CGSize cellSize = [[_subCategoriesTextArraySecondChecks objectAtIndex:indexPath.row] sizeWithFont:[UIFont systemFontOfSize:18]
                                                                                        constrainedToSize:CGSizeMake(300, 2000)
                                                                                            lineBreakMode:NSLineBreakByWordWrapping];
        
        CGFloat cellHeight = cellSize.height;
        
        if(cellHeight<35)
        {
            return 45;
        }
        return cellHeight;
    }
    
    else
    {
        return 45;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return @"Allgemeiner Fensterservice";
    else
        return @"";
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, tableView.frame.size.width, 16)];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    label.textColor = [UIColor whiteColor];
    NSString *string;
    if(section == 0)
    {
        string = self.subCategoryName;//@"Allgemeiner Fensterservice";
        
    }
    else
    {
        string = @"";
    }
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 30;
    }
    else
    {
        return 10;
    }
    
}
#pragma mark - table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isOrderSentGlobal)
    {
        [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Sie nicht bearbeiten können Zugestellte Aufträge." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
        
    }
    else
    {
        UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if(indexPath.section == 0)
        {
            //if(cell.imageView.image == [UIImage imageNamed:@"uncheck1.png"])
            
            if([cell.imageView.image isEqual:[UIImage imageNamed:@"uncheck1.png"]] )
            {
                cell.imageView.image = [UIImage imageNamed:@"check1.png"];
                [_selectedText addObject:cell.textLabel.text];
            }
            else
            {
                cell.imageView.image = [UIImage imageNamed:@"uncheck1.png"];
                if([_selectedText containsObject:cell.textLabel.text])
                {
                    [_selectedText removeObject:cell.textLabel.text];
                }
            }
        }
        else if(indexPath.section == 1)
        {
            if([cell.imageView.image isEqual:[UIImage imageNamed:@"uncheck1.png"]] )
            {
                cell.imageView.image = [UIImage imageNamed:@"check1.png"];
                [_selectedText addObject:cell.textLabel.text];
            }
            else
            {
                cell.imageView.image = [UIImage imageNamed:@"uncheck1.png"];
                if([_selectedText containsObject:cell.textLabel.text])
                {
                    [_selectedText removeObject:cell.textLabel.text];
                }
            }
        }
    }
    
}


#pragma mark Navigation bar button items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

#pragma mark back baar Button
-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    self.showRightBarButton=YES;
    
    [self.navigationController popViewControllerAnimated:NO];
    NSLog(@"Pressed Back Baar Button");
}

#pragma mark save baar Button
- (void)saveButtonClicked
{
    
    if(isOrderSentGlobal)
    {
        [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Sie nicht bearbeiten können Zugestellte Aufträge." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
    }
    else
    {
        if(_selectedText.count!=0 || _textField.text.length!=0)
        {
            OrderDescription *orderDescriptionObject = [NSEntityDescription insertNewObjectForEntityForName:@"OrderDescription" inManagedObjectContext:_appContext.sharedAppDelegate.managedObjectContext];
            [orderDescriptionObject setValue:_categoryName forKey:@"category"];
            [orderDescriptionObject setValue:_subCategoryName forKey:@"subcategory"];
            //textDescription
            [orderDescriptionObject setValue:_textField.text forKey:@"textDescription"];
            
            for (int i=0; i<_selectedText.count; i++) {
                Tasks *tasks = [NSEntityDescription insertNewObjectForEntityForName:@"Tasks" inManagedObjectContext:_appContext.sharedAppDelegate.managedObjectContext];
                tasks.assignedTask = [_selectedText objectAtIndex:i];
                tasks.orderDescription = orderDescriptionObject;
                [orderDescriptionObject addTasksObject:tasks];
            }
            
            [MASTER_RECORD addOrderDescriptionObject:orderDescriptionObject];
            orderDescriptionObject.order = MASTER_RECORD;
            
            NSError *err;
            
            if( ! [_appContext.sharedAppDelegate.managedObjectContext save:&err] )
            {
                NSLog(@"Cannot save data: %@", [err localizedDescription]);
                
            }
            
            
            [self.view endEditing:YES];
            [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
        }
        
        else
        {
            [self showAlert];
        }
    }
}
- (void)popViewController
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[OrderDescriptionViewController class]]){
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
}

- (void)showAlert
{
    [[[UIAlertView alloc] initWithTitle:@"Auftrag" message:@"Um einen Auftrag zu beginnen müssen Sie erst eine Reparatur wählen und/oder einen Freitext eingeben." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}
#pragma mark -
#pragma mark Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-113);
    [self.tableView setContentOffset:scrollPoint animated:YES];
   
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.tableView  setContentOffset:CGPointZero animated:YES];
    [textField resignFirstResponder];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
