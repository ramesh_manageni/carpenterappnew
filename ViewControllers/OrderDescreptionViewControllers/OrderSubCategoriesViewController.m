//
//  OrderSubCategoriesViewController.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OrderSubCategoriesViewController.h"
#import "OrderSubCategoriesTextViewController.h"
#import "SunCategoryTable.h"

@interface OrderSubCategoriesViewController ()

@property (strong, nonatomic) NSMutableArray *subCategories;
@property (nonatomic) SunCategoryTable *sunCategoryObject;
@property (nonatomic) AppContext *appContext;

@end

@implementation OrderSubCategoriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    _appContext = [AppContext sharedAppContext];
        [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setShowsHorizontalScrollIndicator:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    
    [self fetchSubCategories];
    _sunCategoryObject = [[SunCategoryTable alloc]init];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationItem.titleView = [NavigationUtility setNavigationTitle: _categoryName withFontNmae:@"Helvetica-Bold" andSize:18.0];
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    if(selectedSectionNumber==_sectionNumber)
    {
        [bgImage setImage:[UIImage imageNamed:@"HG_Schriener.jpg"]];
    }
    else if (selectedSectionNumber+1==_sectionNumber)
    {
        [bgImage setImage:[UIImage imageNamed:@"HG_Parkett.jpg"]];
    }
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
}
- (void)fetchSubCategories
{
    NSLog(@"Section number %d",_sectionNumber);
    if((_sectionNumber-_appContext.storedTextsArray.count) == 1)
    {
        _subCategories = [SunCategoryTable getSubCategory_Name:_categoryId];
        
    }
    else
    {
        _subCategories = [SunCategoryTable getSubCategory_Name:_categoryId+6];
    }
    [self.tableView reloadData];
}

#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _subCategories.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    _sunCategoryObject = nil;
    _sunCategoryObject = [_subCategories objectAtIndex:indexPath.row];

    cell.textLabel.text = _sunCategoryObject.subCategoryName;
    
    if([_sunCategoryObject.subCategoryName isEqualToString:@"Reparatur Kehricht-System/-Möbel"])
    {
        cell.textLabel.text =@"Rep. Kehricht-System/-Möbel";
    }

    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:16];

    
    UIImageView *cellBackImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 38)];
    cellBackImage.image = [UIImage imageNamed:@"cellbggray.png"];
    cell.backgroundView = cellBackImage;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}


- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if((_sectionNumber - _appContext.storedTextsArray.count-1) == 0)
    {
        return @"Schreiner Service";
    }
    else
    {
        return @"Parkett Service";
    }
    
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, tableView.frame.size.width, 16)];
    [label setBackgroundColor:[UIColor clearColor]];

    [label setFont:[UIFont boldSystemFontOfSize:16]];
    label.textColor = [UIColor whiteColor];
    NSString *string;
    if((_sectionNumber - _appContext.storedTextsArray.count-1) == 0)
    {
        string = @"Schreiner Service 48";
    }
    else
    {
        string = @"Parkett Service 48";
    }
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    _sunCategoryObject = nil;
    _sunCategoryObject = [_subCategories objectAtIndex:indexPath.row];
    
    OrderSubCategoriesTextViewController *orderSubCategoriesTextViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderSubCategoriesTextViewController"];
    
    orderSubCategoriesTextViewController.sectionNumber = _sectionNumber;
    orderSubCategoriesTextViewController.categoryName = _categoryName;
    orderSubCategoriesTextViewController.subCategoryName = _sunCategoryObject.subCategoryName;
    
    NSLog(@"%d",_sunCategoryObject.textId);
    orderSubCategoriesTextViewController.textId = _sunCategoryObject.textId;
    
    if([_sunCategoryObject.subCategoryName isEqualToString:@"SEHRVICE!"])
    {
        orderSubCategoriesTextViewController.showRightBarButton=NO;
    }
    else
    {
        orderSubCategoriesTextViewController.showRightBarButton=YES;
        
    }
    [self.navigationController pushViewController:orderSubCategoriesTextViewController animated:NO];
}

#pragma mark NavigationbarButton Items
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  sichern" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
}

@end
