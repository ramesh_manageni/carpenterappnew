//
//  OrderDescCell.m
//  Service48
//
//  Created by RedBytes on 23/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OrderDescCell.h"

@implementation OrderDescCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       
        
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        {
            [self.subCategoryLabel setBackgroundColor:[UIColor clearColor]];
            [self.subCategoryTextLabel setBackgroundColor:[UIColor clearColor]];
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];

    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        CGRect newFrame1 = self.subCategoryTextLabel.frame;
        newFrame1.origin.x = 5;
        self.subCategoryTextLabel.frame = newFrame1;
        
        CGRect newFrame2 = self.subCategoryLabel.frame;
        newFrame2.origin.x = 5;
        self.subCategoryLabel.frame = newFrame2;

        CGRect newFrame3 = self.textView.frame;
    newFrame3.size.width = 280;
    newFrame3.origin.x = 0;
    self.textView.frame = newFrame3;
    }
    else
    {
       // [self.subCategoryTextLabel setFrame:CGRectMake(self.subCategoryTextLabel.frame.origin.x+5, self.subCategoryTextLabel.frame.origin.y, 300, self.subCategoryTextLabel.frame.size.height)];
        [self.subCategoryTextLabel setFrame:CGRectMake(5, self.subCategoryTextLabel.frame.origin.y, 300, self.subCategoryTextLabel.frame.size.height)];

        
     //   [self.subCategoryLabel setFrame:CGRectMake(self.subCategoryLabel.frame.origin.x+5, self.subCategoryLabel.frame.origin.y, 300, self.subCategoryLabel.frame.size.height)];
        [self.subCategoryLabel setFrame:CGRectMake(5, self.subCategoryLabel.frame.origin.y, 300, self.subCategoryLabel.frame.size.height)];
        

        CGRect newFrame3 = self.textView.frame;
        newFrame3.size.width = 300;
        newFrame3.origin.x = 0;
        self.textView.frame = newFrame3;
    }
    if(isToggleEdit && SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        [UIView setAnimationsEnabled:NO];

        [self.textView setFrame:CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y, 248, self.textView.frame.size.height)];
        [self.subCategoryTextLabel setFrame:CGRectMake(self.subCategoryTextLabel.frame.origin.x, self.subCategoryTextLabel.frame.origin.y, 238, self.subCategoryTextLabel.frame.size.height)];
        
        [self.subCategoryLabel setFrame:CGRectMake(self.subCategoryLabel.frame.origin.x, self.subCategoryLabel.frame.origin.y, 238, self.subCategoryLabel.frame.size.height)];
    }
    else if (isToggleEdit && !SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        [UIView setAnimationsEnabled:NO];

        [self.subCategoryLabel setBackgroundColor:[UIColor clearColor]];
        [self.textView setFrame:CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y, 262, self.textView.frame.size.height)];
        [self.subCategoryTextLabel setFrame:CGRectMake(self.subCategoryTextLabel.frame.origin.x, self.subCategoryTextLabel.frame.origin.y, 245, self.subCategoryTextLabel.frame.size.height)];
        
        [self.subCategoryLabel setFrame:CGRectMake(self.subCategoryLabel.frame.origin.x, self.subCategoryLabel.frame.origin.y, 245, self.subCategoryLabel.frame.size.height)];
    }

    [UIView setAnimationsEnabled:NO];
    
    for (UIView *subview in self.subviews)
    {
        for (UIView *subview2 in subview.subviews)
        {
            NSLog(@"S : %@ \n subview %@",[subview2 class],subview2);
            
            if ([NSStringFromClass([subview2 class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"])
            { // move delete confirmation view
                
                [UIView setAnimationsEnabled:NO];
                
                [self.textView setFrame:CGRectMake(self.textView.frame.origin.x+45, self.textView.frame.origin.y, 216, self.textView.frame.size.height)];
                
                
                [self.subCategoryTextLabel setFrame:CGRectMake(self.subCategoryTextLabel.frame.origin.x+40, self.subCategoryTextLabel.frame.origin.y, 200, self.subCategoryTextLabel.frame.size.height)];
                

                
                [self.subCategoryLabel setFrame:CGRectMake(self.subCategoryLabel.frame.origin.x+40, self.subCategoryLabel.frame.origin.y, 200, self.subCategoryLabel.frame.size.height)];
            }
        }
    }
}
-(void)didTransitionToState:(UITableViewCellStateMask)state
{
    if (state & UITableViewCellStateShowingDeleteConfirmationMask )
    {
        [UIView setAnimationsEnabled:NO];
    }
    [super didTransitionToState:state];
}

@end
