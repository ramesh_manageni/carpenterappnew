// Nihal
//  OrderDescriptionViewController.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedTextTable.h"

@interface OrderDescriptionViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    NSIndexPath *deleteIndexPath;
    SelectedTextTable *tempSelectedText;
    UIToolbar *myToolbar1;
    UITextView *tempTextView;

}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;


@end
