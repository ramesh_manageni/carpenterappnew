// Nihal
//  OrderDescCell.h
//  Service48
//
//  Created by RedBytes on 23/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDescCell : UITableViewCell

@property (nonatomic) UILabel *subCategoryLabel;
@property (nonatomic) UILabel *subCategoryTextLabel;
@property (nonatomic) UITextView *textView;

@end
