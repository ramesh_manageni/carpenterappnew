// Nihal
//  OrderCategoriesViewController.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderSubCategoriesViewController.h"

@interface OrderCategoriesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    int tempCategoryId;
    int tempSectionNumber;
    NSString* tempCategoryName;

    OrderSubCategoriesViewController *orderSubCategoriesViewController;
}

@property(nonatomic, assign) int sectionNumber;
@property (weak, nonatomic) IBOutlet UITableView *tabelView;

@end
