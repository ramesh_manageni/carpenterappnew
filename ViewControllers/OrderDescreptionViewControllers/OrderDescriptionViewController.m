//
//  OrderDescriptionViewController.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OrderDescriptionViewController.h"
#import "OrderCategoriesViewController.h"
#import "SelectedTextTable.h"
#import "OrderDescription.h"
#import "Tasks.h"
#import "OrderDescCell.h"

@interface OrderDescriptionViewController ()
{
    UIBarButtonItem *deleteButton;
    UITextView *textView;
    int tempCount;
}

@property (nonatomic) AppContext *appContext;
@property (nonatomic) NSString *selectedTextString;
@property (assign) int editingFlag;

@end

@implementation OrderDescriptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle: @"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    if([MASTER_RECORD.orderDescription allObjects].count>0)
    {
        self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    [self loadData];
    
    // You can not edit Delivered orders
    if(isOrderSentGlobal)
    {
        self.navigationItem.rightBarButtonItem = nil;
        [[self.navigationItem.leftBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"  zurück" forState:UIControlStateNormal];
        
    }
}
- (void) loadData
{
    
    
    [_appContext.storedTextsArray removeAllObjects];
    for (OrderDescription *orderDescObject in MASTER_RECORD.orderDescription)
    {
        NSMutableArray *selectedArray = [[NSMutableArray alloc]init];
        NSMutableDictionary *selectedTextsDictionary = [[NSMutableDictionary alloc]init];
        for (Tasks *tasks in orderDescObject.tasks)
        {
            [selectedArray addObject:tasks.assignedTask];
        }
        [selectedTextsDictionary setValue:selectedArray forKey:orderDescObject.category];
        
        SelectedTextTable *selectedText = [[SelectedTextTable alloc]init];
        selectedText.selectedTextsDictionary = selectedTextsDictionary;
        selectedText.textFieldDescription = orderDescObject.textDescription;
        selectedText.category = orderDescObject.category;
        selectedText.subCategory = orderDescObject.subcategory;
        selectedText.descriptionObject = orderDescObject;
        _appContext = [AppContext sharedAppContext];
        [_appContext.storedTextsArray addObject:selectedText];
    }
    if(_appContext.storedTextsArray.count == 0)
    {
        [self.tableView setEditing:NO animated:YES];
        _editingFlag = 0;
        [deleteButton setTitle:@""];
    }
    [self reloadTableView];
}
- (void) reloadTableView
{
    [self.tableView reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(inputAccessoryViewDidFinish)];
    
    UIBarButtonItem *cancelButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                  target:self action:@selector(cancelButtonClicked:)];
    
    myToolbar1 = [[UIToolbar alloc] initWithFrame:
                  CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [myToolbar1 setItems:[NSArray arrayWithObjects:cancelButton,flexibleSpace,doneButton, nil] animated:NO];
    
    [self.bgImage setImage:[UIImage imageNamed:@"bluebg.jpg"]];
    _appContext = [AppContext sharedAppContext];
    deleteButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(editTableView)];
    self.navigationItem.rightBarButtonItem = deleteButton;
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
}
-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.tableView setEditing:NO animated:YES];
}
- (void) editTableView
{
    if(_editingFlag == 0)
    {
        if(_appContext.storedTextsArray.count>0)
        {
            [self.tableView setEditing:YES animated:YES];
            [deleteButton setTitle:@"fertig"];
            _editingFlag = 1;
        }
    }
    else
    {
        [self.tableView setEditing:NO animated:YES];
        [deleteButton setTitle:@"bearbeiten"];
        _editingFlag = 0;
    }
}

#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    selectedSectionNumber = (int)_appContext.storedTextsArray.count+1;
    return 3 + _appContext.storedTextsArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return 0;
    else
        return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int i=0;
    NSString * cellIdentifier = [NSString stringWithFormat:@"cell%d",i++];
    OrderDescCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[OrderDescCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    else
    {
        cell.textLabel.text = nil;
        cell.imageView.image = nil;
        for (UIView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
    }
    
    if(indexPath.section == 0){
        
    }
    else if(indexPath.section == _appContext.storedTextsArray.count+1)
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.BackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"button 1.png"]];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = nil;
    }
    else if (indexPath.section == _appContext.storedTextsArray.count+2)
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.BackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"button 2.png"]];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = nil;
    }
    else
    {
        cell.BackgroundView = nil;
        cell.backgroundColor =[UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
        
        SelectedTextTable *selectedText = [_appContext.storedTextsArray objectAtIndex:indexPath.section-1];
        
        tempSelectedText=selectedText;
        
        cell.subCategoryLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 300, 20)];
        cell.subCategoryLabel.font = [UIFont boldSystemFontOfSize:14];
        [cell.contentView addSubview:cell.subCategoryLabel];
        
        cell.subCategoryTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 30, 300, 100)];
        
        cell.subCategoryTextLabel.font = [UIFont systemFontOfSize:14];
        [cell.subCategoryTextLabel setNumberOfLines:0];
        [cell.contentView addSubview:cell.subCategoryTextLabel];
        cell.subCategoryLabel.text = selectedText.subCategory;
        
        NSArray *selectedTextArray = [selectedText.selectedTextsDictionary objectForKey:selectedText.category];
        NSLog(@"Array : %@",selectedTextArray);
        
        _selectedTextString = [selectedTextArray componentsJoinedByString:@"\n• "];
        if(selectedTextArray.count>0)
        {
            cell.subCategoryTextLabel.text = [NSString stringWithFormat:@"• %@", _selectedTextString];
        }
        CGSize labelSize = [cell.subCategoryTextLabel.text sizeWithFont:[UIFont systemFontOfSize:14]
                                                      constrainedToSize:CGSizeMake(300, 2000)
                                                          lineBreakMode:NSLineBreakByWordWrapping];
        
        CGRect newFrame = cell.subCategoryTextLabel.frame;
        newFrame.size.height = labelSize.height;
        cell.subCategoryTextLabel.frame = newFrame;
        
        cell.textView= [[UITextView alloc]initWithFrame:CGRectMake(00,labelSize.height+35, 280, 30)];
        
        cell.textView.tag=indexPath.section;
        cell.textView.layer.borderColor = [[UIColor grayColor] CGColor];
        cell.textView.layer.borderWidth = 1.0f;
        
        if(isOrderSentGlobal)
        {
            [cell.textView setEditable:NO];
        }
        else
        {
            [cell.textView setEditable:YES];
            cell.textView.delegate=(id)self;
            cell.textView.inputAccessoryView=myToolbar1;
        }
        cell. textView.font = [UIFont systemFontOfSize:14];
        [cell.contentView addSubview:cell.textView];
        
        cell.textView.text = selectedText.textFieldDescription;
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        {
            [cell.subCategoryLabel setBackgroundColor:[UIColor clearColor]];
            [cell.subCategoryTextLabel setBackgroundColor:[UIColor clearColor]];
            
            UIView *myView = [[UIView alloc] init];
            myView.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
            cell.backgroundView = myView;
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 0)
    {
        
    }
    else if(indexPath.section == _appContext.storedTextsArray.count+1)
    {
        return 40;
    }
    else if (indexPath.section == _appContext.storedTextsArray.count+2)
    {
        return 40;
    }
    
    else
    {
        SelectedTextTable *selectedText = [_appContext.storedTextsArray objectAtIndex:indexPath.section-1];
        NSArray *selectedTextArray = [selectedText.selectedTextsDictionary objectForKey:selectedText.category];
        CGSize cellSize;
        CGFloat cellHeight;
        _selectedTextString = [selectedTextArray componentsJoinedByString:@"\n☻"];
        if(selectedTextArray.count>0)
        {
            cellSize = [_selectedTextString sizeWithFont:[UIFont systemFontOfSize:14]
                                       constrainedToSize:CGSizeMake(300, 2000)
                                           lineBreakMode:NSLineBreakByWordWrapping];
            cellHeight = cellSize.height;
            
            return cellHeight+30+36;
        }
    }
    return 65;
}
- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"Auftragsübersicht";
    }
    else if (_appContext.storedTextsArray.count>0 && section!=_appContext.storedTextsArray.count+1 && section!=_appContext.storedTextsArray.count+2)
    {
        SelectedTextTable *selectedTextTable = [_appContext.storedTextsArray objectAtIndex:section-1];
        return selectedTextTable.category;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 30;
    }
    else if (_appContext.storedTextsArray.count>0 && section!=_appContext.storedTextsArray.count+1 && section!=_appContext.storedTextsArray.count+2)
    {
        return 25;
    }
    else
    {
        return 01;
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    //[label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    NSString *string; //=@"Auftragsübersicht";
    
    if (section == 0)
    {
        //return @"Auftragsübersicht";
        string =@"Auftragsübersicht";
        [label setTextColor:[UIColor whiteColor]];
    }
    else if (_appContext.storedTextsArray.count>0 && section!=_appContext.storedTextsArray.count+1 && section!=_appContext.storedTextsArray.count+2)
    {
        [label setTextColor:[UIColor blackColor]];
        SelectedTextTable *selectedTextTable = [_appContext.storedTextsArray objectAtIndex:section-1];
        string = selectedTextTable.category;
        //return selectedTextTable.category;
    }
    else
    {
        [label setTextColor:[UIColor blackColor]];
        string = nil;
        //return nil;
    }
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    //[view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //[tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        deleteIndexPath=indexPath;
        [self showAlert];
    }
    //[tableView endUpdates];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL edit = NO;
    if(indexPath.section == 0)
    {
        edit = NO;
    }
    else if(indexPath.section == _appContext.storedTextsArray.count+1)
    {
        edit = NO;
    }
    else if (indexPath.section == _appContext.storedTextsArray.count+2)
    {
        edit = NO;
    }
    else
    {
        edit = YES;
        
    }
    return edit;
}

#pragma mark - table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == _appContext.storedTextsArray.count+1 || indexPath.section == _appContext.storedTextsArray.count+2)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        OrderCategoriesViewController *orderCategoriesViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderCategoriesViewController"];
        orderCategoriesViewController.sectionNumber = (int)indexPath.section;
        
        NSLog(@"indexPath : %ld",(long)indexPath.section);
        [self.navigationController pushViewController:orderCategoriesViewController animated:NO];
    }
    
}

#pragma mark Navigation Bar Button Items
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  sichern" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Navigation Bar Button Items
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"bearbeiten" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(toggleEdit) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
//Method to change title of Edit/Done button
-(void)toggleEdit
{
    if([[[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] titleForState:UIControlStateNormal] isEqualToString:@"fertig"])
    {
        isToggleEdit=NO;
    }
    else if ([[[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] titleForState:UIControlStateNormal] isEqualToString:@"bearbeiten"])
    {
        isToggleEdit=YES;
        
    }
    [self.tableView reloadData];
    
    [self.tableView setEditing:!self.tableView.editing animated:NO];
    
    if (self.tableView.editing)
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"fertig" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(30, 3, 70, 30)];
        
    }
    
    else
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"bearbeiten" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(10, 3, 95, 30)];
        
    }
}

#pragma mark UIAlertView delegate
- (void)showAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Reparatur löschen" message:@"Wollen Sie die Reparatur wirklich löschen?" delegate:self cancelButtonTitle:@"Löschen" otherButtonTitles:@"Abbrechen", nil];
    
    alert.delegate=self;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)//OK button pressed
    {
        // Do whatever data deletion you need to do...
        // Delete the row from the data source
        SelectedTextTable *selectText = [_appContext.storedTextsArray objectAtIndex:deleteIndexPath.section-1];
        
        [_appContext.sharedAppDelegate.managedObjectContext deleteObject:selectText.descriptionObject];
        
        NSError *error;
        [_appContext.sharedAppDelegate.managedObjectContext save:&error];
        [self loadData];
        //
        if([MASTER_RECORD.orderDescription allObjects].count==0)
        {
            self.navigationItem.rightBarButtonItem = nil;
            isToggleEdit=NO;
            
        }
        
    }
}

#pragma mark UITextView delegate

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                self.tableView.frame= CGRectMake(10, 0, 300, 375);
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                self.tableView.frame= CGRectMake(10, 0, 300, 455);
                
            }
        }
    }
    [self loadData];
    [self.tableView reloadData];
    [txtView resignFirstResponder];
    return NO;
}


- (void)textViewDidBeginEditing:(UITextView *)txtView
{
    tempTextView=txtView;
    
    NSIndexPath *indexPath;
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                
                self.tableView.frame= CGRectMake(10, 0, 300, 375 - 190);
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                self.tableView.frame= CGRectMake(10, 0, 300, 455 - 190);
                
            }
        }
    }
    
    indexPath =[NSIndexPath indexPathForRow:0 inSection:txtView.tag];
    
    
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
-(void)inputAccessoryViewDidFinish
{
    tempCount=1;
    for (OrderDescription *orderDescObject in MASTER_RECORD.orderDescription)
    {
        
        if(tempCount==tempTextView.tag)
        {
            orderDescObject.textDescription=tempTextView.text;
            NSError *err;
            
            if( ! [_appContext.sharedAppDelegate.managedObjectContext save:&err] )
            {
                NSLog(@"Cannot save data: %@", [err localizedDescription]);
                
            }
            
        }
        tempCount++;
    }
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                self.tableView.frame= CGRectMake(10, 0, 300, 375);
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                self.tableView.frame= CGRectMake(10, 0, 300, 455);
                
            }
        }
    }
    
    [self.view endEditing:YES];
    [self loadData];
    
    [self.tableView reloadData];
    
}

-(void)cancelButtonClicked:(UIButton *)sender
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                self.tableView.frame= CGRectMake(10, 0, 300, 375);
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                self.tableView.frame= CGRectMake(10, 0, 300, 455);
                
            }
        }
    }
    
    [self.view endEditing:YES];
    [self loadData];
    
    [self.tableView reloadData];
}

@end
