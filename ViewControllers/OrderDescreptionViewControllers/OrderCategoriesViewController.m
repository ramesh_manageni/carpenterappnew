//
//  OrderCategoriesViewController.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OrderCategoriesViewController.h"
#import "OrderSubCategoriesViewController.h"
#import "CategoryTable.h"
#import "AppContext.h"
#import "SunCategoryTable.h"
#import "OrderSubCategoriesTextViewController.h"

@interface OrderCategoriesViewController ()

@property (strong, nonatomic) NSMutableArray *categories;

@property (nonatomic) AppContext *appContext;

//ramesh
@property (strong, nonatomic) NSMutableArray *subCategories;
@property (nonatomic) SunCategoryTable *sunCategoryObject;


@end

@implementation OrderCategoriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tabelView setBackgroundColor:[UIColor clearColor]];
    [self.tabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tabelView setShowsHorizontalScrollIndicator:NO];
    [self.tabelView setShowsVerticalScrollIndicator:NO];
    //[self.tabelView scrollsToTop:NO];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    [self fetchCategories];
	// Do any additional setup after loading the view.
    
    

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if(selectedSectionNumber==_sectionNumber)
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Schreiner 48" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    }
    else if (selectedSectionNumber+1==_sectionNumber)
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Parkett 48" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    }
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    
    if(selectedSectionNumber==_sectionNumber)
    {
        [bgImage setImage:[UIImage imageNamed:@"HG_Schriener.jpg"]];
    }
    else if (selectedSectionNumber+1==_sectionNumber)
    {
        [bgImage setImage:[UIImage imageNamed:@"HG_Parkett.jpg"]];
    }
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
}

- (void)fetchCategories
{
    _appContext = [AppContext sharedAppContext];
    int tempCount=(int)(_sectionNumber - _appContext.storedTextsArray.count);
    int section = tempCount-1;
    _categories = [CategoryTable getCategory_Name:section];
}

#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _categories.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

    }
    cell.textLabel.text = [_categories objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
    
    
    UIImageView *cellBackImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 38)];
    cellBackImage.image = [UIImage imageNamed:@"bg2.png"];
    cell.backgroundView = cellBackImage;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}



- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if((_sectionNumber - _appContext.storedTextsArray.count-1) == 0)
    {
        return @"Schreiner Service 48";
    }
    else
    {
        return @"Parkett Service 48";
    }
    
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, tableView.frame.size.width, 16)];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    label.textColor = [UIColor whiteColor];
    NSString *string;
    if((_sectionNumber - _appContext.storedTextsArray.count-1) == 0)
    {
        string = @"Schreiner Service 48";
    }
    else
    {
        string = @"Parkett Service 48";
    }
    
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    orderSubCategoriesViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderSubCategoriesViewController"];
    
    orderSubCategoriesViewController.sectionNumber = _sectionNumber;
    
    orderSubCategoriesViewController.categoryId =(int) indexPath.row;
    orderSubCategoriesViewController.categoryName = [_categories objectAtIndex:indexPath.row];
  
    tempSectionNumber=_sectionNumber;
    tempCategoryId=(int)indexPath.row;
    tempCategoryName=[_categories objectAtIndex:indexPath.row];
    //ramesh
    [self fetchSubCategories];
    
    //[self.navigationController pushViewController:orderSubCategoriesViewController animated:NO];
}

- (void)fetchSubCategories
{
    NSLog(@"Section number %d",_sectionNumber);
    if((_sectionNumber-_appContext.storedTextsArray.count) == 1)
    {
        _subCategories = [SunCategoryTable getSubCategory_Name:tempCategoryId];
        
    }
    else
    {
        _subCategories = [SunCategoryTable getSubCategory_Name:tempCategoryId+6];
    }
    
    if(_subCategories.count==1)
    {
        _sunCategoryObject = nil;
        _sunCategoryObject = [_subCategories objectAtIndex:0];
        
        OrderSubCategoriesTextViewController *orderSubCategoriesTextViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderSubCategoriesTextViewController"];
        
        orderSubCategoriesTextViewController.sectionNumber = _sectionNumber;
        orderSubCategoriesTextViewController.categoryName = tempCategoryName;
        orderSubCategoriesTextViewController.subCategoryName = _sunCategoryObject.subCategoryName;
        
        NSLog(@"%d",_sunCategoryObject.textId);
        orderSubCategoriesTextViewController.textId = _sunCategoryObject.textId;
        
        if([_sunCategoryObject.subCategoryName isEqualToString:@"SEHRVICE!"])
        {
            orderSubCategoriesTextViewController.showRightBarButton=NO;
        }
        else
        {
            orderSubCategoriesTextViewController.showRightBarButton=YES;
            
        }

        [self.navigationController pushViewController:orderSubCategoriesTextViewController animated:NO];


    }
    else
    {
        [self.navigationController pushViewController:orderSubCategoriesViewController animated:NO];

    }
}
#pragma mark NavigationbarButton Items
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  sichern" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
}
@end
