// Nihal
//  OrderSubCategoriesViewController.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderSubCategoriesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, assign) int categoryId;
@property(nonatomic, strong) NSString* categoryName;
@property(nonatomic, assign) int sectionNumber;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
