//
//  ListBillingAddressViewController.m
//  Service48
//
//  Created by Redbytes on 21/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ListBillingAddressViewController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "BillingAddress.h"
#import "DatabaseHandler.h"
#import "NavigationUtility.h"
#import "BillingAddressViewController.h"
#import "OrderTable.h"

@interface ListBillingAddressViewController ()

@property (nonatomic,strong)NSArray* fetchedRecordsArray;


@end

@implementation ListBillingAddressViewController

@synthesize tableviewObject;
@synthesize senderTag;
@synthesize showRadioButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    addressIndex=-1;
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        
    }
    list=[[NSMutableArray alloc]initWithObjects:@"Rechnungs-Adresse",@"Bereits erfasste Adresse auswählen", nil];
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    [self.view setBackgroundColor:[UIColor blueColor]];
    
    //Check screen height
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(10, 0, 300, 365) style:UITableViewStylePlain];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(10, 0, 300, 452) style:UITableViewStylePlain];
            }
        }
    }
    [tableviewObject setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [tableviewObject setShowsHorizontalScrollIndicator:NO];
    [tableviewObject setShowsVerticalScrollIndicator:NO];
    [self.view addSubview:tableviewObject];
    
    self.fetchedRecordsArray=[DatabaseHandler getEntityCountWithName:@"BillingAddress"];
    [self initialSetup];
    [self.tableviewObject reloadData];
    
    
    CGFloat dummyViewHeight = 40;
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableviewObject.bounds.size.width, dummyViewHeight)];
    self.tableviewObject.tableHeaderView = dummyView;
    self.tableviewObject.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0);
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:@"empty_row.png"] forState:UIControlStateNormal];
    [button.titleLabel setTextColor:[UIColor whiteColor]];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitle:@"  Keine Adressen" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    button.frame = CGRectMake(0, 145.0, 300.0, 45.0);
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //[self initialSetup];
    NSInteger tag=self.tabBarController.tabBar.selectedItem.tag;
    if(tag==3)
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    }
    else
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
        
    }
    
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    
    [bgImage setImage:[UIImage imageNamed:@"bluebg.jpg"]];
    
    
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
    
    self.fetchedRecordsArray=[DatabaseHandler getEntityCountWithName:@"BillingAddress"];
    
    if(self.fetchedRecordsArray.count==0)
    {
        [self.tableviewObject addSubview:button];
        
    }
    else
    {
        [button removeFromSuperview];
    }
    
    if(self.fetchedRecordsArray.count>0)
    {
        self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
        
    }
    
    [self.tableviewObject reloadData];
    
}

//
-(void)initialSetup
{
    NSArray *allAttributeKeys = [[[MASTER_RECORD.orderTableToBillingAddress entity] attributesByName] allKeys];
    
    for (int i=0; i<self.fetchedRecordsArray.count; i++)
    {
        if([[MASTER_RECORD.orderTableToBillingAddress entity] isEqual:[[self.fetchedRecordsArray objectAtIndex:i] entity]]
           && [[MASTER_RECORD.orderTableToBillingAddress committedValuesForKeys:allAttributeKeys] isEqual:[[self.fetchedRecordsArray objectAtIndex:i] committedValuesForKeys:allAttributeKeys]])
        {
            addressIndex=i;
            
            NSLog(@"EQUAL OBJECT FOUND %@",MASTER_RECORD.orderTableToBillingAddress);
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table View data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [label setTextColor:[UIColor whiteColor]];
    
    NSString *string =[list objectAtIndex:section];
    
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    //[view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section==0)
    {
        return 1;
        
    }
    else if (section==1)
    {
        return [self.fetchedRecordsArray count];
    }
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell6";
    
    ListObjectAddressCustomCell *cell;
    if (cell == nil) {
        cell = [[ListObjectAddressCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    if(!isOrderSentGlobal)
    {
        [cell.button addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.cellOverlaybutton addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [cell.button setTag:indexPath.row];
    [cell.cellOverlaybutton setTag:indexPath.row];
    
    if(indexPath.section==0)
    {
        cell.enterOrderLabel.text=@" Neue Adresse erfassen";
        if(indexPath.row==0)
        {
            [cell.button removeFromSuperview];
            [cell.cellOverlaybutton removeFromSuperview];
            
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        
        //return cell;
        
    }
    if(indexPath.section==1)
    {
        [cell.enterOrderLabel removeFromSuperview];
        
        if([[self.fetchedRecordsArray objectAtIndex:indexPath.row] companyName].length==0)
        {
            [[self.fetchedRecordsArray objectAtIndex:indexPath.row]setBillingFirmaName:@"(Ohne Firma)"];
            if([[self.fetchedRecordsArray objectAtIndex:indexPath.row] firstName].length!=0)
            {
                [[self.fetchedRecordsArray objectAtIndex:indexPath.row]setBillingFirmaName:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] firstName]];
                if([[self.fetchedRecordsArray objectAtIndex:indexPath.row] lastName].length!=0)
                {
                    [[self.fetchedRecordsArray objectAtIndex:indexPath.row]setBillingFirmaName:[NSString stringWithFormat:@"%@ %@",[[self.fetchedRecordsArray objectAtIndex:indexPath.row] firstName],[[self.fetchedRecordsArray objectAtIndex:indexPath.row] lastName]]];
                    
                }
                
            }
        }
        else
        {
            
            [[self.fetchedRecordsArray objectAtIndex:indexPath.row]setBillingFirmaName:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] companyName]];
            
        }
        
        cell.addressLabel.text=[[self.fetchedRecordsArray objectAtIndex:indexPath.row]billingFirmaName];
        
        if(MASTER_RECORD.orderTableToBillingAddress==[self.fetchedRecordsArray objectAtIndex:indexPath.row])
        {
            [cell.button setBackgroundImage:[UIImage imageNamed:@"radiobuttonselect.png"] forState:UIControlStateNormal];
            // MASTER_RECORD.orderTableToOwnerAddress=[self.fetchedRecordsArray objectAtIndex:indexPath.row];
            
            MASTER_RECORD.billingFirma=[[self.fetchedRecordsArray objectAtIndex:indexPath.row] billingFirmaName];
        }
        else
        {
            [cell.button setBackgroundImage:[UIImage imageNamed:@"radiobuttondeselect.png"] forState:UIControlStateNormal];
            
        }
        
        
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    //
    if(!self.showRadioButton)
    {
        [cell.button removeFromSuperview];
        [cell.cellOverlaybutton removeFromSuperview];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        
        UIImageView* img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellbg2.png"]];
        [cell setBackgroundView:img];
        [cell.backgroundView sendSubviewToBack:img];
    }
    else if (indexPath.section!=0)
    {
        UIImageView* img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellbgSplited.png"]];
        [cell setBackgroundView:img];
        [cell.backgroundView sendSubviewToBack:img];
        
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *) indexPath
{
    if(indexPath.section==0)
    {
        return NO;
    }
    return YES;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(MASTER_RECORD.orderTableToBillingAddress ==[self.fetchedRecordsArray objectAtIndex:indexPath.row])
    {
        
        [self showAlert:@"Das Objekt konnte nicht gelöscht werden. Das Objekt wird in einem Auftrag benutzt." withTitle:@"Objekt löschen"];
    }
    else
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            deleteIndexPath=indexPath;
            [self showAlert];
        }
    }
}


-(void)autoRadioButtonClicked:(UIButton *)sender
{
    MASTER_RECORD.orderTableToBillingAddress=[self.fetchedRecordsArray objectAtIndex:sender.tag];
    MASTER_RECORD.billingFirma=[[self.fetchedRecordsArray objectAtIndex:sender.tag] billingFirmaName];
    
}
-(void)radioButtonClicked:(UIButton *)sender
{
    addressIndex=sender.tag;
    [tableviewObject reloadData];
    
    //Add perticular address object to master record
    MASTER_RECORD.orderTableToBillingAddress=[self.fetchedRecordsArray objectAtIndex:sender.tag];
    MASTER_RECORD.billingFirma=[[self.fetchedRecordsArray objectAtIndex:sender.tag] billingFirmaName];
    
    
    NSLog(@"SENDER TAG: %ld",(long)addressIndex);
    
}
#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BillingAddressViewController *objBillingAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BillingAddressViewController"];
    
    if(indexPath.section==0)
    {
        objBillingAddressViewController.record=nil;
    }
    else if (indexPath.section==1)
    {
        objBillingAddressViewController.record=(BillingAddress *)[self.fetchedRecordsArray objectAtIndex:indexPath.row];
        
    }
    objBillingAddressViewController.buttonTag=self.senderTag;
    
    if(isOrderSentGlobal && indexPath.section==0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Sie nicht bearbeiten können Zugestellte Aufträge." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
        
    }
    else
    {
        [self.navigationController pushViewController:objBillingAddressViewController animated:NO];
    }
    
    
    
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    NSString *btnTitle;
    if(isOrderSentGlobal)
    {
        btnTitle=@"  zurück";
    }
    else
    {
        btnTitle=@"  sichern";
        
    }
    
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:btnTitle withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"bearbeiten" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(toggleEdit) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
}

//Method to change title of Edit/Done button
-(void)toggleEdit
{
    [self.tableviewObject reloadData];
    
    [self.tableviewObject setEditing:!self.tableviewObject.editing animated:YES];
    
    if (self.tableviewObject.editing)
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"fertig" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(30, 3, 70, 30)];
        
    }
    
    else
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"bearbeiten" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(10, 3, 95, 30)];
        
    }
}
//Alert
- (void)showAlert:(NSString *)message withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}

#pragma mark UIAlertView delegate
- (void)showAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Element löschen" message:@"Sind Sie sicher dass Sie das Element löschen wollen?" delegate:self cancelButtonTitle:@"Löschen" otherButtonTitles:@"Abbrechen", nil];
    
    alert.delegate=self;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)//OK button pressed
    {
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        
        //    1
        [self.tableviewObject beginUpdates];
        // Delete the row from the data source
        
        //    2
        [self.tableviewObject deleteRowsAtIndexPaths:@[deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        //    3
        [context deleteObject:[self.fetchedRecordsArray objectAtIndex:deleteIndexPath.row]];
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        //    4
        
        self.fetchedRecordsArray=[DatabaseHandler getEntityCountWithName:@"BillingAddress"];
        if(self.fetchedRecordsArray.count==0)
        {
            [button removeFromSuperview];
            
            [self.tableviewObject addSubview:button];
            
        }
        else
        {
            [button removeFromSuperview];
        }
        
        if(self.fetchedRecordsArray.count==0)
        {
            self.navigationItem.rightBarButtonItem = nil;
            [self.tableviewObject setEditing:NO];
        }
        
        [self.tableviewObject endUpdates];
        [self.tableviewObject reloadData];
        
        
    }
}


@end
