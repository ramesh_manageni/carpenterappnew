//
//  BillingAddressViewController.h
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "BillingAddress.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface BillingAddressViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate>
{
    
    
}
@property int buttonTag;

@property(strong,nonatomic) BillingAddress *record;

- (IBAction)maleFemaleButtonClicked:(UIButton *)sender;//To choose gender

@end
