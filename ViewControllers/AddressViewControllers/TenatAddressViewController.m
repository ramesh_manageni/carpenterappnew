//
//  TenatAddressViewController.m
//  Service48
//
//  Created by Redbytes on 02/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "TenatAddressViewController.h"
#import "BSKeyboardControls.h"
#import "AppDelegate.h"
#import "NavigationUtility.h"
@interface TenatAddressViewController ()

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (nonatomic,strong)NSArray* fetchedRecordsArray;

@property (nonatomic, strong)NSMutableDictionary *addressInfoDict;
@property (nonatomic, strong)NSMutableDictionary *recordDict;
@property (nonatomic, strong)NSMutableDictionary *masterDict;

@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelSubtitle;

@property (strong, nonatomic) IBOutlet UILabel *labelMale;
@property (strong, nonatomic) IBOutlet UILabel *labelFemale;
@property (strong, nonatomic) IBOutlet UILabel *labelFirstName;
@property (strong, nonatomic) IBOutlet UILabel *labelLastName;
@property (strong, nonatomic) IBOutlet UILabel *labelPrivateTel;
@property (strong, nonatomic) IBOutlet UILabel *labelBussinessTel;
@property (strong, nonatomic) IBOutlet UILabel *labelMobile;
@property (strong, nonatomic) IBOutlet UILabel *labelEmail;
@property (strong, nonatomic) IBOutlet UILabel *labelFreeText;
@property (strong, nonatomic) IBOutlet UITextField *textfieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *textfieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *textfieldPrivateTel;
@property (strong, nonatomic) IBOutlet UITextField *textfieldBussinessTel;
@property (strong, nonatomic) IBOutlet UITextField *textfieldMobile;
@property (strong, nonatomic) IBOutlet UITextField *textfieldEmail;
@property (strong, nonatomic) IBOutlet UIButton *buttonMale;
@property (strong, nonatomic) IBOutlet UIButton *buttonFemale;


//Object Address Box
@property (strong, nonatomic) IBOutlet UITextField *textlabel_propertyType;
@property (strong, nonatomic) IBOutlet UITextField *textlabel_floorNo;

@property (strong, nonatomic) IBOutlet UITextField *textfieldTenantNo;

@property (strong, nonatomic) IBOutlet UITextField *textfieldOrderNo;
@end

@implementation TenatAddressViewController

@synthesize record,recordDict,addressInfoDict,masterDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
        
    }
    //Dictionary used to store(temporary) address information
    addressInfoDict=[[NSMutableDictionary alloc] init];
    recordDict=[[NSMutableDictionary alloc] init];
    masterDict=[[NSMutableDictionary alloc] init];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    //Set keyboard controls to all textfields
    NSArray *fields = @[self.textfieldFirstName,
                        self.textfieldLastName,
                        self.textfieldPrivateTel,
                        self.textfieldBussinessTel,
                        self.textfieldMobile,
                        self.textfieldEmail,
                        self.textlabel_propertyType,
                        self.textlabel_floorNo,
                        self.textfieldTenantNo,
                        self.textfieldOrderNo,
                        ];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    //Method for display data from databse to view
    self.fetchedRecordsArray=[DatabaseHandler getEntityCountWithName:@"TenantAddress"];
    
    if([self tenantAddressObjectExist])
    {
        self.record=MASTER_RECORD.orderTableToTenantAddress;
    }
    else
    {
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        
        self.record=[NSEntityDescription insertNewObjectForEntityForName:@"TenantAddress" inManagedObjectContext:context];
    }
    
    [self initialSetup];
    [self.objScrollView setCanCancelContentTouches:YES];
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(isOrderSentGlobal)
    {
        self.navigationItem.rightBarButtonItem = nil;
        
        NSArray *subviews=self.view.subviews;
        for(UIView *view in subviews)
        {
            if([view isKindOfClass:[UIScrollView class]])
            {
                for(UIView *textField in view.subviews)
                {
                    if([textField isKindOfClass:[UITextField class]])
                    {
                        [(UITextField *)textField setEnabled:NO];
                    }
                    if([textField isKindOfClass:[UIButton class]])
                    {
                        [(UIButton *)textField setEnabled:NO];
                    }
                    
                }
            }
        }
        
        
    }
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 200, 0.0);
    self.objScrollView.contentInset = contentInsets;
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    
}
-(BOOL)tenantAddressObjectExist
{
    
    NSArray *allAttributeKeys = [[[MASTER_RECORD.orderTableToTenantAddress entity] attributesByName] allKeys];
    
    for (int i=0; i<self.fetchedRecordsArray.count; i++)
    {
        if([[MASTER_RECORD.orderTableToTenantAddress entity] isEqual:[[self.fetchedRecordsArray objectAtIndex:i] entity]]
           && [[MASTER_RECORD.orderTableToTenantAddress committedValuesForKeys:allAttributeKeys] isEqual:[[self.fetchedRecordsArray objectAtIndex:i] committedValuesForKeys:allAttributeKeys]])
        {
            return YES;
            NSLog(@"EQUAL OBJECT FOUND %@",MASTER_RECORD.orderTableToTenantAddress);
        }
    }
    return NO;
}
-(void)initialSetup
{
    
    self.textlabel_propertyType.userInteractionEnabled=NO;
    self.textlabel_floorNo.userInteractionEnabled=NO;
    
    self.textfieldFirstName.text=self.record.firstName;
    self.textfieldLastName.text=self.record.lastName;
    self.textfieldPrivateTel.text=self.record.homePhoneNo;
    self.textfieldBussinessTel.text=self.record.bussinessPhoneNo;
    self.textfieldMobile.text=self.record.mobilePhoneNo;
    self.textfieldEmail.text=self.record.emailID;
    self.textfieldTenantNo.text=self.record.tenantNo;
    self.textfieldOrderNo.text=self.record.orderNo;
    
    
    [self initialSetupRadioButton];
}
-(void)initialSetupRadioButton
{
    //Set radio button selected for male or female
    if([self.record.maleOrFemale isEqualToString:@"Herr"])
    {
        [self maleFemaleButtonClicked:(UIButton *)[self.view viewWithTag:2]];
    }
    else if ([self.record.maleOrFemale isEqualToString:@"Frau"])
    {
        [self maleFemaleButtonClicked:(UIButton *)[self.view viewWithTag:1]];
    }
    
    //VacanyType
    if([[self.record valueForKey:@"vacancyType"] isEqualToString:@"Leerstand"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:11]];
    }
    else if ([[self.record valueForKey:@"vacancyType"] isEqualToString:@"Bewohnt"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:12]];
    }
    //PropertyType
    if ([[self.record valueForKey:@"propertyType"] isEqualToString:@"Wohnung"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:21]];
    }
    else if ([[self.record valueForKey:@"propertyType"] isEqualToString:@"Büro"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:22]];
    }
    else if(((NSString *)[self.record valueForKey:@"propertyType"]).length>0)
    {
        self.textlabel_propertyType.userInteractionEnabled=YES;
        self.textlabel_propertyType.text=[self.record valueForKey:@"propertyType"];
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:23]];
    }
    //FloorType
    if ([[self.record valueForKey:@"floorType"] isEqualToString:@"UG"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:31]];
    }
    else if ([[self.record valueForKey:@"floorType"] isEqualToString:@"EG"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:32]];
    }
    else if (![[self.record valueForKey:@"floorType"] isEqualToString:@"EG"] && ![[self.record valueForKey:@"floorType"] isEqualToString:@"UG"]  && ((NSString *)[self.record valueForKey:@"floorType"]).length>0)
    {
        self.textlabel_floorNo.userInteractionEnabled=YES;
        self.textlabel_floorNo.text=[self.record valueForKey:@"floorType"];
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:33]];
    }
    //DirectionType
    if ([[self.record valueForKey:@"directionType"] isEqualToString:@"Links"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:41]];
    }
    else if ([[self.record valueForKey:@"directionType"] isEqualToString:@"Mitte"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:42]];
    }
    else if ([[self.record valueForKey:@"directionType"] isEqualToString:@"Rechts"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:43]];
    }
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setTextlabel_propertyType:nil];
    [self setTextlabel_floorNo:nil];
    
    [self setKeyboardControls:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self setTextlabel_propertyType:nil];
    [self setTextlabel_floorNo:nil];
    
    [self setKeyboardControls:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Method to choose between male and female
- (IBAction)maleFemaleButtonClicked:(UIButton *)sender
{
    
    switch (sender.tag)
    {
        case 1:
            [addressInfoDict setValue:@"Frau" forKey:@"gender"];
            break;
        case 2:
            [addressInfoDict setValue:@"Herr" forKey:@"gender"];
            break;
            
        default:
            break;
    }
    
    UIImage *image = [UIImage imageNamed:@"radiobuttonselect.png"]; //the radioButton image when selected
    UIImage *deselectImage = [UIImage imageNamed:@"radiobuttondeselect.png"]; //the radioButton image when deselected
    
    if(sender == (UIButton *)[self.view viewWithTag:1])
    {
        [(UIButton *)[self.view viewWithTag:1] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:2] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
    }
    else if(sender == (UIButton *)[self.view viewWithTag:2])
    {
        [(UIButton *)[self.view viewWithTag:2] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:1] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    
}

- (IBAction)radiobuttonClicked:(UIButton *)sender
{
    int buttonTagCount;//buttonTagCount is used to identify the row
    
    switch (sender.tag)
    {
        case 11:
            buttonTagCount=0;
            [addressInfoDict setValue:@"Leerstand" forKey:@"vacancyType"];//Added value to dictionary which will be used to store in database
            break;
        case 12:
            buttonTagCount=0;
            [addressInfoDict setValue:@"Bewohnt" forKey:@"vacancyType"];
            break;
            
        case 21:
            buttonTagCount=10;
            self.textlabel_propertyType.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"Wohnung" forKey:@"propertyType"];
            break;
        case 22:
            buttonTagCount=10;
            self.textlabel_propertyType.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"Büro" forKey:@"propertyType"];
            break;
        case 23:
            buttonTagCount=10;
            self.textlabel_propertyType.userInteractionEnabled=YES;
            break;
            
        case 31:
            buttonTagCount=20;
            self.textlabel_floorNo.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"UG" forKey:@"floorType"];
            break;
        case 32:
            buttonTagCount=20;
            self.textlabel_floorNo.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"EG" forKey:@"floorType"];
            break;
        case 33:
            buttonTagCount=20;
            self.textlabel_floorNo.userInteractionEnabled=YES;
            
            break;
            
        case 41:
            buttonTagCount=30;
            [addressInfoDict setValue:@"Links" forKey:@"directionType"];
            break;
        case 42:
            buttonTagCount=30;
            [addressInfoDict setValue:@"Mitte" forKey:@"directionType"];
            break;
        case 43:
            buttonTagCount=30;
            [addressInfoDict setValue:@"Rechts" forKey:@"directionType"];
            break;
            
        default:
            buttonTagCount=0;
            break;
    }
    
    UIImage *image = [UIImage imageNamed:@"radiobuttonselect.png"]; //the radioButton image when selected
    UIImage *deselectImage = [UIImage imageNamed:@"radiobuttondeselect.png"]; //the radioButton image when deselected
    
    if(sender == (UIButton *)[self.view viewWithTag:11+buttonTagCount])
    {
        [(UIButton *)[self.view viewWithTag:11+buttonTagCount] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:12+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:13+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    else if(sender == (UIButton *)[self.view viewWithTag:12+buttonTagCount])
    {
        [(UIButton *)[self.view viewWithTag:12+buttonTagCount] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:11+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:13+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    else
    {
        [(UIButton *)[self.view viewWithTag:13+buttonTagCount] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:12+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:11+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
}



#pragma mark -
#pragma mark Keyboard Controls Delegate


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self.keyboardControls setActiveField:textField];
    
    
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-57);
    [self.objScrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.objScrollView  setContentOffset:CGPointZero animated:YES];
//    if (textField==self.textfieldEmail&&textField.text.length>0)
//    {
//        if (![self validateEmailWithString:textField.text])
//        {
//            [self showAlert];
//            [self.textfieldEmail becomeFirstResponder];
//        }
//    }
    
    
    if(textField==self.textlabel_propertyType)
    {
        if(!self.textlabel_propertyType.text.length==0)
        {
            [addressInfoDict setValue:[NSString stringWithFormat:@"%@",self.textlabel_propertyType.text] forKey:@"propertyType"];
        }
        else
        {
            //Added value to dictionary which will be used to store in database.
            
            [addressInfoDict setValue:[NSString stringWithFormat:@" "] forKey:@"propertyType"];
            
        }
        
    }
    if(textField==self.textlabel_floorNo)
    {
        //[addressInfoDict setValue:@"OG" forKey:@"floorType"];
        if(!self.textlabel_floorNo.text.length==0)
        {
            [addressInfoDict setValue:[NSString stringWithFormat:@"%@",self.textlabel_floorNo.text] forKey:@"floorType"];
        }
        else
        {
            [addressInfoDict setValue:[NSString stringWithFormat:@" "] forKey:@"floorType"];
            
        }
        
    }
    if(textField==self.textfieldPrivateTel||textField==self.textfieldBussinessTel||textField==self.textfieldMobile)
    {
        int length = [self getLengtWithFirstZero:textField.text];
        NSString *num = [self formatNumber:textField.text];
        
        NSString *str=[[NSString alloc] init];
        
        if(textField.text.length > 0)
        {
            str=[NSString stringWithFormat:@"%c",[textField.text characterAtIndex:0]];
        }
        
        if(length >= 9)
        {
            if([str isEqualToString:@"+"])
            {
                textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ ",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 2)],[num substringWithRange:NSMakeRange(5, 3)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
            }
            
            if([[textField.text substringToIndex:2] isEqualToString:@"00"])
            {
                if(length==12)
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ ",[num  substringToIndex:2],[num substringWithRange:NSMakeRange(2, 2)],[num substringWithRange:NSMakeRange(4, 2)],[num substringWithRange:NSMakeRange(6, 3)],[num substringWithRange:NSMakeRange(9, 2)],[num substringFromIndex:11]];
                }
                if(length>12)
                {
                    
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ ",[num  substringToIndex:2],[num substringWithRange:NSMakeRange(2, 2)],[num substringFromIndex:4]];
                    
                }
                
                
            }
            
            else
            {
                if(length==9)
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 3)],[num substringWithRange:NSMakeRange(6, 2)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
                    
                }
                else
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ ",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 2)],[num substringWithRange:NSMakeRange(5, 3)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
                    
                }
            }
        }
    }
    
    
    [textField resignFirstResponder];
    
}
- (void)showAlert
{
    [[[UIAlertView alloc] initWithTitle:@"Fehler im Formular" message:@"Bitte geben Sie eine gültige Emailadresse an." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
}

#pragma mark Database methods
-(void)saveButtonClicked
{
    if (![self validateEmailWithString:self.textfieldEmail.text] && self.textfieldEmail.text.length>0)
    {
        [self showAlert];
        //[self.textfieldEmail becomeFirstResponder];
    }
    else
    {
        
        [self insertRecord];
        [self.view endEditing:YES];
        [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
    }
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)insertRecord
{
    [recordDict setValue:self.textfieldFirstName.text forKey:@"firstName"];
    [recordDict setValue:self.textfieldLastName.text forKey:@"lastName"];
    [recordDict setValue:self.textfieldPrivateTel.text forKey:@"homePhoneNo"];
    [recordDict setValue:self.textfieldBussinessTel.text forKey:@"bussinessPhoneNo"];
    [recordDict setValue:self.textfieldMobile.text forKey:@"mobilePhoneNo"];
    [recordDict setValue:self.textfieldEmail.text forKey:@"emailID"];
    [recordDict setValue:[addressInfoDict valueForKey:@"gender"] forKey:@"maleOrFemale"];
    
    
    [recordDict setValue:[addressInfoDict valueForKey:@"vacancyType"] forKey:@"vacancyType"];
    [recordDict setValue:[addressInfoDict valueForKey:@"propertyType"] forKey:@"propertyType"];
    [recordDict setValue:[addressInfoDict valueForKey:@"floorType"] forKey:@"floorType"];
    [recordDict setValue:[addressInfoDict valueForKey:@"directionType"] forKey:@"directionType"];
    
    [recordDict setValue:self.textfieldTenantNo.text forKey:@"tenantNo"];
    [recordDict setValue:self.textfieldOrderNo.text forKey:@"orderNo"];
    
    
    //ADD RECORD TO MASTER_RECORD
    [masterDict setValue:@"" forKey:@"orderTableToTenantAddress"];
    
    [DatabaseHandler insertRecord:record withData:recordDict withMasterData:masterDict withEntityNmae:@"TenantAddress"];
    [self addLodgerNameToOrder];
}
-(void)addLodgerNameToOrder
{
    if(self.textfieldFirstName.text.length==0 && self.textfieldLastName.text.length==0 && self.textfieldPrivateTel.text.length==0 && self.textfieldBussinessTel.text.length==0 && self.textfieldMobile.text.length==0 && self.textfieldEmail.text.length==0 && [addressInfoDict valueForKey:@"gender"]==nil && [addressInfoDict valueForKey:@"vacancyType"]==nil && [addressInfoDict valueForKey:@"propertyType"]==nil && [addressInfoDict valueForKey:@"floorType"]==nil && [addressInfoDict valueForKey:@"directionType"]==nil && self.textfieldTenantNo.text.length==0 && self.textfieldOrderNo.text.length==0)
    {
        MASTER_RECORD.lodgerName=nil;
    }
    else if (self.textfieldLastName.text.length==0)
    {
        if(self.textfieldFirstName.text.length==0)
        {
            MASTER_RECORD.lodgerName=@"(Ohne Nachname)";
        }
        else
        {
            MASTER_RECORD.lodgerName=self.textfieldFirstName.text;
            
        }
    }
    else
    {
        if(self.textfieldFirstName.text.length!=0)
        {
            MASTER_RECORD.lodgerName=[NSString stringWithFormat:@"%@ %@",self.textfieldLastName.text,self.textfieldFirstName.text];
            
        }
        else
        {
            MASTER_RECORD.lodgerName=self.textfieldLastName.text;
        }
        
    }
}

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}
-(int)getLength:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int length = (int)[mobileNumber length];
    return length;
}
-(int)getLengtWithFirstZero:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    int length = (int)[mobileNumber length]-1;
    return length;
}
-(NSString*)formatNumber:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        NSLog(@"%@", mobileNumber);
        
    }
    
    
    return mobileNumber;
}

@end
