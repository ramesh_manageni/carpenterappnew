//
//  PersonalInfoViewController.m
//  Service48
//
//  Created by Redbytes on 02/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PersonalInfoViewController.h"
#import "BSKeyboardControls.h"
#import "AppDelegate.h"

@interface PersonalInfoViewController ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@property (nonatomic,strong)NSArray* fetchedRecordsArray;
@property (nonatomic, strong)NSMutableDictionary *addressInfoDict;
@property (nonatomic, strong)NSMutableDictionary *recordDict;
@property (nonatomic, strong)NSMutableDictionary *masterDict;

@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;

@property (strong, nonatomic) IBOutlet UILabel *labelTitleTop;
@property (strong, nonatomic) IBOutlet UILabel *labelFirstName;
@property (strong, nonatomic) IBOutlet UILabel *labelLastName;
@property (strong, nonatomic) IBOutlet UILabel *labelPhoneDirectly;
@property (strong, nonatomic) IBOutlet UILabel *labelMobile;
@property (strong, nonatomic) IBOutlet UILabel *labelFax;

@property (strong, nonatomic) IBOutlet UILabel *labelTitleBottom;
@property (strong, nonatomic) IBOutlet UILabel *labelEmail;
@property (strong, nonatomic) IBOutlet UILabel *labelCC;

@property (strong, nonatomic) IBOutlet UILabel *labelTitleEnd;
@property (strong, nonatomic) IBOutlet UILabel *labelCompany;
@property (strong, nonatomic) IBOutlet UILabel *labelStreetNo;
@property (strong, nonatomic) IBOutlet UILabel *labelZipcode;
@property (strong, nonatomic) IBOutlet UILabel *labelPlace;
@property (strong, nonatomic) IBOutlet UILabel *labelCentralNo;


@property (strong, nonatomic) IBOutlet UIButton *buttonMale;
@property (strong, nonatomic) IBOutlet UIButton *buttonFemale;
@property (strong, nonatomic) IBOutlet UILabel *labelMale;
@property (strong, nonatomic) IBOutlet UILabel *labelFemale;


@property (strong, nonatomic) IBOutlet UITextField *textfieldFirstNmae;
@property (strong, nonatomic) IBOutlet UITextField *textfieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *textfieldPhoneDirectly;
@property (strong, nonatomic) IBOutlet UITextField *textfieldMobile;
@property (strong, nonatomic) IBOutlet UITextField *textfieldFax;

@property (strong, nonatomic) IBOutlet UITextField *textfieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *textfieldCC;

@property (strong, nonatomic) IBOutlet UITextField *textfieldCompany;
@property (strong, nonatomic) IBOutlet UITextField *textfieldStreetNo;
@property (strong, nonatomic) IBOutlet UITextField *textfieldZipcode;
@property (strong, nonatomic) IBOutlet UITextField *textfieldPlace;
@property (strong, nonatomic) IBOutlet UITextField *textfieldCentralNo;


@end

@implementation PersonalInfoViewController

@synthesize record,recordDict,addressInfoDict,masterDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        
        //  [self setEdgesForExtendedLayout:UIRectEdgeNone];
//        self.edgesForExtendedLayout=UIRectEdgeNone;
//        self.extendedLayoutIncludesOpaqueBars=NO;
//        self.automaticallyAdjustsScrollViewInsets=NO;
        
    }
    
    //Dictionary used to store(temporary) address information
    addressInfoDict=[[NSMutableDictionary alloc] init];
    recordDict=[[NSMutableDictionary alloc] init];
    masterDict=[[NSMutableDictionary alloc] init];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    //Set keyboard controls to all textfields
    NSArray *fields = @[self.textfieldFirstNmae,
                        self.textfieldLastName,
                        self.textfieldPhoneDirectly,
                        self.textfieldMobile,
                        self.textfieldFax,
                        
                        self.textfieldEmail,
                        self.textfieldCC,
                        
                        self.textfieldCompany,
                        self.textfieldStreetNo,
                        self.textfieldZipcode,
                        self.textfieldPlace,
                        self.textfieldCentralNo];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    //Method for display data from databse to view
    self.fetchedRecordsArray=[DatabaseHandler getEntityCountWithName:@"PersonalInformation"];
    
    
    if([self personalInfoObjectExist])
    {
        self.record=[self.fetchedRecordsArray objectAtIndex:0];
    }
    else
    {
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        
        self.record=[NSEntityDescription insertNewObjectForEntityForName:@"PersonalInformation" inManagedObjectContext:context];
    }
    
    [self initialSetup];
    [self.objScrollView setCanCancelContentTouches:YES];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 200, 0.0);
    self.objScrollView.contentInset = contentInsets;
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    self.navigationController.navigationBar.hidden=NO;
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    
}
-(BOOL)personalInfoObjectExist
{
    if(self.fetchedRecordsArray.count>0)
    {
        return YES;
        
    }
    return NO;
}
#pragma mark Initial Setup
-(void)initialSetup
{
    self.textfieldFirstNmae.text=self.record.firstName;
    self.textfieldLastName.text=self.record.lastName;
    self.textfieldPhoneDirectly.text=self.record.phoneDirectly;
    self.textfieldMobile.text=self.record.mobilePhoneNo;
    self.textfieldFax.text=self.record.faxDirectly;
    
    self.textfieldEmail.text=self.record.emailID;
    self.textfieldCC.text=self.record.emailCC;
    
    self.textfieldCompany.text=self.record.companyName;
    self.textfieldStreetNo.text=self.record.streetNo;
    self.textfieldZipcode.text=self.record.zipCode;
    self.textfieldPlace.text=self.record.place;
    self.textfieldCentralNo.text=self.record.centralPhoneNo;
    
    [self initialSetupRadioButton];
}
-(void)initialSetupRadioButton
{
    //Set radio button selected for male or female
    if([self.record.maleOrFemale isEqualToString:@"Herr"])
    {
        [self maleFemaleButtonClicked:(UIButton *)[self.view viewWithTag:12]];
    }
    else if ([self.record.maleOrFemale isEqualToString:@"Frau"])
    {
        [self maleFemaleButtonClicked:(UIButton *)[self.view viewWithTag:11]];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Method to choose between male and female
- (IBAction)maleFemaleButtonClicked:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 11:
            [addressInfoDict setValue:@"Frau" forKey:@"gender"];
            break;
        case 12:
            [addressInfoDict setValue:@"Herr" forKey:@"gender"];
            break;
            
        default:
            break;
    }
    
    UIImage *image = [UIImage imageNamed:@"radiobuttonselect.png"]; //the radioButton image when selected
    UIImage *deselectImage = [UIImage imageNamed:@"radiobuttondeselect.png"]; //the radioButton image when deselected
    
    if(sender == (UIButton *)[self.view viewWithTag:11])
    {
        [(UIButton *)[self.view viewWithTag:11] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:12] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
    }
    else if(sender == (UIButton *)[self.view viewWithTag:12])
    {
        [(UIButton *)[self.view viewWithTag:12] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:11] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    
}
#pragma mark -
#pragma mark Keyboard Controls Delegate



- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
    
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-113);
    [self.objScrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.objScrollView  setContentOffset:CGPointZero animated:YES];
    
//    if ((textField==self.textfieldEmail || textField == self.textfieldCC) && textField.text.length>0)
//    {
//        if (![self validateEmailWithString:textField.text])
//        {
//            [self showAlertEmail];
//            [textField becomeFirstResponder];
//        }
//    }
    
    if(textField==self.textfieldPhoneDirectly||textField==self.textfieldMobile||textField==self.textfieldFax||textField==self.textfieldCentralNo)
    {
        
        int length = [self getLengtWithFirstZero:textField.text];
        NSString *num = [self formatNumber:textField.text];
        
        NSString *str=[[NSString alloc] init];
        
        if(textField.text.length > 0)
        {
            str=[NSString stringWithFormat:@"%c",[textField.text characterAtIndex:0]];
        }
        
        if(length >= 9)
        {
            if([str isEqualToString:@"+"])
            {
                textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ ",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 2)],[num substringWithRange:NSMakeRange(5, 3)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
            }
            
            if([[textField.text substringToIndex:2] isEqualToString:@"00"])
            {
                if(length==12)
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ ",[num  substringToIndex:2],[num substringWithRange:NSMakeRange(2, 2)],[num substringWithRange:NSMakeRange(4, 2)],[num substringWithRange:NSMakeRange(6, 3)],[num substringWithRange:NSMakeRange(9, 2)],[num substringFromIndex:11]];
                }
                if(length>12)
                {
                    
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ ",[num  substringToIndex:2],[num substringWithRange:NSMakeRange(2, 2)],[num substringFromIndex:4]];
                    
                }
                
                
            }
            
            else
            {
                if(length==9)
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 3)],[num substringWithRange:NSMakeRange(6, 2)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
                    
                }
                else
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ ",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 2)],[num substringWithRange:NSMakeRange(5, 3)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
                    
                }
            }
        }
    }
    
    
    
    [textField resignFirstResponder];
    
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
- (void)showAlertEmail
{
    [[[UIAlertView alloc] initWithTitle:@"Fehler im Formular" message:@"Bitte geben Sie eine gültige Emailadresse an." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
}
-(void)saveButtonClicked
{
    if([self.textfieldFirstNmae.text isEqualToString:@""])
    {
        [self showAlert];
    }
    else if([self.textfieldLastName.text isEqualToString:@""])
    {
        [self showAlert];
    }
    else if([self.textfieldPhoneDirectly.text isEqualToString:@""])
    {
        [self showAlert];
    }
    else if([self.textfieldEmail.text isEqualToString:@""])
    {
        [self showAlert];
    }
    else if (![self validateEmailWithString:self.textfieldEmail.text])
    {
        [self showAlertEmail];
       // [self.textfieldEmail becomeFirstResponder];
        
    }
    else if([self.textfieldZipcode.text isEqualToString:@""])
    {
        [self showAlert];
    }
    else if (![self validateEmailWithString:self.textfieldCC.text] && self.textfieldCC.text.length>0)
    {
        [self showAlertEmail];
        //[self.textfieldCC becomeFirstResponder];
    }
    
    else
    {
        [self insertRecord];
        [self.view endEditing:YES];
        [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
        
    }
    
}

- (void)showAlert
{
    [[[UIAlertView alloc] initWithTitle:@"Fehler im Formular" message:@"Füllen Sie die Pflichtfelder aus und geben Sie gültige Emailadressen an." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}
- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:NO];
}
#pragma mark Database methods
-(void)insertRecord
{
    [recordDict setValue:self.textfieldFirstNmae.text forKey:@"firstName"];
    [recordDict setValue:self.textfieldLastName.text forKey:@"lastName"];
    [recordDict setValue:self.textfieldPhoneDirectly.text forKey:@"phoneDirectly"];
    [recordDict setValue:self.textfieldMobile.text forKey:@"mobilePhoneNo"];
    [recordDict setValue:self.textfieldFax.text forKey:@"faxDirectly"];
    
    [recordDict setValue:self.textfieldEmail.text forKey:@"emailID"];
    [recordDict setValue:self.textfieldCC.text forKey:@"emailCC"];
    
    [recordDict setValue:self.textfieldCompany.text forKey:@"companyName"];
    [recordDict setValue:self.textfieldStreetNo.text forKey:@"streetNo"];
    [recordDict setValue:self.textfieldZipcode.text forKey:@"zipCode"];
    [recordDict setValue:self.textfieldPlace.text forKey:@"place"];
    [recordDict setValue:self.textfieldCentralNo.text forKey:@"centralPhoneNo"];
    
    [recordDict setValue:[addressInfoDict valueForKey:@"gender"] forKey:@"maleOrFemale"];
    //ADD RECORD TO MASTER_RECORD
    [masterDict setValue:@"" forKey:@"orderTableToPersonalIfo"];
    
    [DatabaseHandler insertRecord:record withData:recordDict withMasterData:masterDict withEntityNmae:@"PersonalInformation"];
}
#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}

-(int)getLength:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int length = (int)[mobileNumber length];
    return length;
}
-(int)getLengtWithFirstZero:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    int length = (int)[mobileNumber length]-1;
    return length;
}
-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        NSLog(@"%@", mobileNumber);
        
    }
    
    
    return mobileNumber;
}


@end
