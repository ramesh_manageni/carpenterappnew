//
//  ObjectAddressViewController.h
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"BSKeyboardControls.h"
#import "PropertyAddress.h"

@interface ObjectAddressViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate>
{
    
}

@property(strong,nonatomic) NSManagedObject *record;

- (IBAction)radiobuttonClicked:(UIButton *)sender;//To choose single value from multiple values from address details

@end
