//
//  ListObjectAddressViewController.h
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListObjectAddressCustomCell.h"
#import "ObjectAddressViewController.h"

@interface ListObjectAddressViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *list;
    NSString *tempStr;
    NSInteger addressIndex;
    UIButton *button;
    NSIndexPath *deleteIndexPath;


}

@property(strong,nonatomic)UITableView *tableviewObject;

@property BOOL showRadioButton;
@end
