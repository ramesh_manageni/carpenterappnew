//
//  ObjectAddressViewController.m
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ObjectAddressViewController.h"
#import "PropertyAddress.h"
#import "AppDelegate.h"

#import "BSKeyboardControls.h"
#import "DatabaseHandler.h"
#import "NavigationUtility.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface ObjectAddressViewController ()

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@property (nonatomic, strong)NSMutableDictionary *addressInfoDict;
@property (nonatomic, strong)NSMutableDictionary *recordDict;
@property (nonatomic, strong)NSMutableDictionary *masterDict;

@property (strong, nonatomic) IBOutlet UITextField *textlabel_zipcode;

@property (strong, nonatomic) IBOutlet UITextField *textlabel_street_no;


@property (strong, nonatomic) IBOutlet UITextField *textlabel_place;
@property (strong, nonatomic) IBOutlet UITextField *textlabel_propertyType;
@property (strong, nonatomic) IBOutlet UITextField *textlabel_floorNo;
@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;


@end

@implementation ObjectAddressViewController

@synthesize record,addressInfoDict,recordDict,masterDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.objScrollView setContentSize:CGSizeMake(320, 1500)];
    // Do any additional setup after loading the view from its nib.
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        
        // [self setEdgesForExtendedLayout:UIRectEdgeNone];
        
    }
    //Dictionary used to store(temporary) address information
    addressInfoDict=[[NSMutableDictionary alloc] init];
    recordDict=[[NSMutableDictionary alloc] init];
    masterDict=[[NSMutableDictionary alloc] init];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
    NSArray *fields = @[self.textlabel_street_no,
                        self.textlabel_zipcode,
                        self.textlabel_place,
                        ];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    //Method for display data from databse to view
    [self initialSetup];
    [self.objScrollView setCanCancelContentTouches:YES];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if(isOrderSentGlobal)
    {
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
        
        NSArray *subviews=self.view.subviews;
        for(UIView *view in subviews)
        {
            if([view isKindOfClass:[UIScrollView class]])
            {
                for(UIView *textField in view.subviews)
                {
                    if([textField isKindOfClass:[UITextField class]])
                    {
                        [(UITextField *)textField setEnabled:NO];
                    }
                    
                }
            }
        }
        
        
    }
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    
    int tag=(int)self.tabBarController.tabBar.selectedItem.tag;
    if(tag==3)
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    }
    else
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
        
    }
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    
}
-(void)initialSetup
{
    
    self.textlabel_propertyType.userInteractionEnabled=NO;
    self.textlabel_floorNo.userInteractionEnabled=NO;
    
    self.textlabel_street_no.text=[self.record valueForKey:@"streetNo"];
    self.textlabel_zipcode.text=[self.record valueForKey:@"zipCode"];
    self.textlabel_place.text=[self.record valueForKey:@"place"];
    
    [self initialSetupRadioButton];
}
-(void)initialSetupRadioButton
{
    //VacanyType
    if([[self.record valueForKey:@"vacancyType"] isEqualToString:@"Leerstand"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:11]];
    }
    else if ([[self.record valueForKey:@"vacancyType"] isEqualToString:@"Bewohnt"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:12]];
    }
    //PropertyType
    if ([[self.record valueForKey:@"propertyType"] isEqualToString:@"Wohnung"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:21]];
    }
    else if ([[self.record valueForKey:@"propertyType"] isEqualToString:@"Büro"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:22]];
    }
    else if(((NSString *)[self.record valueForKey:@"propertyType"]).length>0)
    {
        self.textlabel_propertyType.userInteractionEnabled=YES;
        self.textlabel_propertyType.text=[self.record valueForKey:@"propertyType"];
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:23]];
    }
    //FloorType
    if ([[self.record valueForKey:@"floorType"] isEqualToString:@"UG"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:31]];
    }
    else if ([[self.record valueForKey:@"floorType"] isEqualToString:@"EG"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:32]];
    }
    else if (![[self.record valueForKey:@"floorType"] isEqualToString:@"EG"] && ![[self.record valueForKey:@"floorType"] isEqualToString:@"UG"]  && ((NSString *)[self.record valueForKey:@"floorType"]).length>0)
    {
        self.textlabel_floorNo.userInteractionEnabled=YES;
        self.textlabel_floorNo.text=[self.record valueForKey:@"floorType"];
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:33]];
    }
    //DirectionType
    if ([[self.record valueForKey:@"directionType"] isEqualToString:@"Links"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:41]];
    }
    else if ([[self.record valueForKey:@"directionType"] isEqualToString:@"Mitte"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:42]];
    }
    else if ([[self.record valueForKey:@"directionType"] isEqualToString:@"Rechts"])
    {
        [self radiobuttonClicked:(UIButton *)[self.view viewWithTag:43]];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setTextlabel_street_no:nil];
    [self setTextlabel_zipcode:nil];
    [self setTextlabel_place:nil];
    [self setTextlabel_propertyType:nil];
    [self setTextlabel_floorNo:nil];
    
    [self setKeyboardControls:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self setTextlabel_street_no:nil];
    [self setTextlabel_zipcode:nil];
    [self setTextlabel_place:nil];
    [self setTextlabel_propertyType:nil];
    [self setTextlabel_floorNo:nil];
    
    [self setKeyboardControls:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Keyboard Controls Delegate


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
}
-(void)saveButtonClicked
{
    
    [self insertRecord];
    [self.view endEditing:YES];
    [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
}

- (void)popViewController
{
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[NewOrderViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
    [self.navigationController popViewControllerAnimated:NO];
    
}
- (IBAction)radiobuttonClicked:(UIButton *)sender
{
    int buttonTagCount;//buttonTagCount is used to identify the row
    
    switch (sender.tag)
    {
        case 11:
            buttonTagCount=0;
            [addressInfoDict setValue:@"Leerstand" forKey:@"vacancyType"];//Added value to dictionary which will be used to store in database
            break;
        case 12:
            buttonTagCount=0;
            [addressInfoDict setValue:@"Bewohnt" forKey:@"vacancyType"];
            break;
            
        case 21:
            buttonTagCount=10;
            self.textlabel_propertyType.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"Wohnung" forKey:@"propertyType"];
            break;
        case 22:
            buttonTagCount=10;
            self.textlabel_propertyType.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"Büro" forKey:@"propertyType"];
            break;
        case 23:
            buttonTagCount=10;
            self.textlabel_propertyType.userInteractionEnabled=YES;
            
            break;
            
        case 31:
            buttonTagCount=20;
            self.textlabel_floorNo.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"UG" forKey:@"floorType"];
            break;
        case 32:
            buttonTagCount=20;
            self.textlabel_floorNo.userInteractionEnabled=NO;
            [addressInfoDict setValue:@"EG" forKey:@"floorType"];
            break;
        case 33:
            buttonTagCount=20;
            self.textlabel_floorNo.userInteractionEnabled=YES;
            
            
            break;
            
        case 41:
            buttonTagCount=30;
            [addressInfoDict setValue:@"Links" forKey:@"directionType"];
            break;
        case 42:
            buttonTagCount=30;
            [addressInfoDict setValue:@"Mitte" forKey:@"directionType"];
            break;
        case 43:
            buttonTagCount=30;
            [addressInfoDict setValue:@"Rechts" forKey:@"directionType"];
            break;
            
        default:
            buttonTagCount=0;
            break;
    }
    
    UIImage *image = [UIImage imageNamed:@"radiobuttonselect.png"]; //the radioButton image when selected
    UIImage *deselectImage = [UIImage imageNamed:@"radiobuttondeselect.png"]; //the radioButton image when deselected
    
    if(sender == (UIButton *)[self.view viewWithTag:11+buttonTagCount])
    {
        [(UIButton *)[self.view viewWithTag:11+buttonTagCount] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:12+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:13+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    else if(sender == (UIButton *)[self.view viewWithTag:12+buttonTagCount])
    {
        [(UIButton *)[self.view viewWithTag:12+buttonTagCount] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:11+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:13+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    else
    {
        [(UIButton *)[self.view viewWithTag:13+buttonTagCount] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:12+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:11+buttonTagCount] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
}

//Add address information to the database
-(void)insertRecord
{
    
    [recordDict setValue:_textlabel_street_no.text forKey:@"streetNo"];
    [recordDict setValue:_textlabel_zipcode.text forKey:@"zipCode"];
    [recordDict setValue:_textlabel_place.text forKey:@"place"];
    [recordDict setValue:[addressInfoDict valueForKey:@"vacancyType"] forKey:@"vacancyType"];
    [recordDict setValue:[addressInfoDict valueForKey:@"propertyType"] forKey:@"propertyType"];
    [recordDict setValue:[addressInfoDict valueForKey:@"floorType"] forKey:@"floorType"];
    [recordDict setValue:[addressInfoDict valueForKey:@"directionType"] forKey:@"directionType"];
    //Add data to Maser Record
    if(!isFromAddressTabPropertyAddress)
    {
        [masterDict setValue:@"" forKey:@"orderTableToPropertyAddress"];
    }
    
    [DatabaseHandler insertRecord:record withData:recordDict withMasterData:masterDict withEntityNmae:@"PropertyAddress"];
    
}

#pragma mark -
#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
    
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-54);
    [self.objScrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.objScrollView  setContentOffset:CGPointZero animated:YES];
    
    if(textField==self.textlabel_propertyType)
    {
        if(!self.textlabel_propertyType.text.length==0)
        {
            [addressInfoDict setValue:[NSString stringWithFormat:@"%@",self.textlabel_propertyType.text] forKey:@"propertyType"];
        }
        else
        {
            //Added value to dictionary which will be used to store in database.
            
            [addressInfoDict setValue:[NSString stringWithFormat:@" "] forKey:@"propertyType"];
            
        }
        
    }
    if(textField==self.textlabel_floorNo)
    {
        //[addressInfoDict setValue:@"OG" forKey:@"floorType"];
        if(!self.textlabel_floorNo.text.length==0)
        {
            [addressInfoDict setValue:[NSString stringWithFormat:@"%@",self.textlabel_floorNo.text] forKey:@"floorType"];
        }
        else
        {
            [addressInfoDict setValue:[NSString stringWithFormat:@" "] forKey:@"floorType"];
            
        }
        
    }
    
    [textField resignFirstResponder];
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}

@end
