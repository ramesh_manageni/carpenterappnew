//
//  TenatAddressViewController.h
//  Service48
//
//  Created by Redbytes on 02/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "TenantAddress.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface TenatAddressViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate>
{

}
@property(strong,nonatomic) TenantAddress *record;

- (IBAction)maleFemaleButtonClicked:(UIButton *)sender;//To choose gender

- (IBAction)radiobuttonClicked:(UIButton *)sender;//To choose single value from multiple values from address details


@end
