//
//  CTObjectViewController.h
//  Service48
//
//  Created by Redbytes on 08/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "OwnerAddress.h"
#import "CareTakerCompanyAddress.h"
#import "CareTakerObjectAddress.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface CTObjectViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate>
{
    
}

@property int buttonTag;
@property(strong,nonatomic) CareTakerObjectAddress *record;

- (IBAction)maleFemaleuttonClicked:(UIButton *)sender;//To choose gender

@end
