//
//  OwnerAddressViewController.m
//  Service48
//
//  Created by Redbytes on 02/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "OwnerAddressViewController.h"
#import "BSKeyboardControls.h"
#import "AppDelegate.h"
#import "OwnerAddress.h"


@interface OwnerAddressViewController ()

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@property (nonatomic, strong)NSMutableDictionary *addressInfoDict;
@property (nonatomic, strong)NSMutableDictionary *recordDict;
@property (nonatomic, strong)NSMutableDictionary *masterDict;

@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleTop;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleBottom;
@property (strong, nonatomic) IBOutlet UILabel *labelSubtitleBottom;

@property (strong, nonatomic) IBOutlet UILabel *labelCompany;
@property (strong, nonatomic) IBOutlet UILabel *labelStreetNo;
@property (strong, nonatomic) IBOutlet UILabel *labelZipcode;
@property (strong, nonatomic) IBOutlet UILabel *labelPlace;
@property (strong, nonatomic) IBOutlet UILabel *labelCentralTel;

@property (strong, nonatomic) IBOutlet UILabel *labelFirstName;
@property (strong, nonatomic) IBOutlet UILabel *labelLastName;
@property (strong, nonatomic) IBOutlet UILabel *labelLandline;
@property (strong, nonatomic) IBOutlet UILabel *labelMobile;
@property (strong, nonatomic) IBOutlet UILabel *labelEmail;

@property (strong, nonatomic) IBOutlet UILabel *labelMale;
@property (strong, nonatomic) IBOutlet UILabel *labelFemale;

@property (strong, nonatomic) IBOutlet UIButton *buttonMale;
@property (strong, nonatomic) IBOutlet UIButton *buttonFemale;

@property (strong, nonatomic) IBOutlet UITextField *textfieldCompany;
@property (strong, nonatomic) IBOutlet UITextField *textfieldStereetNo;
@property (strong, nonatomic) IBOutlet UITextField *textfieldZipcode;
@property (strong, nonatomic) IBOutlet UITextField *textfieldPlace;
@property (strong, nonatomic) IBOutlet UITextField *textfieldCentralTel;

@property (strong, nonatomic) IBOutlet UITextField *textfieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *textfieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *textfieldLandline;
@property (strong, nonatomic) IBOutlet UITextField *textfieldMobile;
@property (strong, nonatomic) IBOutlet UITextField *textfieldEmail;



@end

@implementation OwnerAddressViewController

@synthesize buttonTag;
@synthesize record,recordDict,addressInfoDict,masterDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
        
    }
    //Dictionary used to store(temporary) address information
    addressInfoDict=[[NSMutableDictionary alloc] init];
    recordDict=[[NSMutableDictionary alloc] init];
    masterDict=[[NSMutableDictionary alloc] init];
    
    switch (buttonTag)
    {
        case 4:
            self.labelTitleTop.text=@"Eigentümer";
            break;
            
        case 5:
            self.labelTitleTop.text=@"Hauswart-Firma";
            break;
            
        case 6:
            self.labelTitleTop.text=@"Objekt-Hauswart";
            break;
            
        default:
            break;
    }
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    //self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    NSArray *fields = @[self.textfieldCompany,
                        self.textfieldStereetNo,
                        self.textfieldZipcode,
                        self.textfieldPlace,
                        self.textfieldCentralTel,
                        self.textfieldFirstName,
                        self.textfieldLastName,
                        self.textfieldLandline,
                        self.textfieldMobile,
                        self.textfieldEmail];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    //Method for display data from databse to view
    [self initialSetup];
    [self.objScrollView setCanCancelContentTouches:YES];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(isOrderSentGlobal)
    {
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
        
        NSArray *subviews=self.view.subviews;
        for(UIView *view in subviews)
        {
            if([view isKindOfClass:[UIScrollView class]])
            {
                for(UIView *textField in view.subviews)
                {
                    if([textField isKindOfClass:[UITextField class]])
                    {
                        [(UITextField *)textField setEnabled:NO];
                    }
                    if([textField isKindOfClass:[UIButton class]])
                    {
                        [(UIButton *)textField setEnabled:NO];
                    }
                    
                }
            }
        }
        
        
    }
    
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    
    int tag=(int)self.tabBarController.tabBar.selectedItem.tag;
    if(tag==3)
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    }
    else
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
        
    }
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    
}
-(void)initialSetup
{
    self.textfieldCompany.text=self.record.companyName;
    self.textfieldStereetNo.text=self.record.streetNo;
    self.textfieldZipcode.text=self.record.zipCode;
    self.textfieldPlace.text=self.record.place;
    self.textfieldCentralTel.text=self.record.centralPhoneNo;
    
    self.textfieldFirstName.text=self.record.firstName;
    self.textfieldLastName.text=self.record.lastName;
    self.textfieldLandline.text=self.record.landlinePhoneNo;
    self.textfieldMobile.text=self.record.mobilePhoneNo;
    self.textfieldEmail.text=self.record.emailID;
    
    [self initialSetupRadioButton];
}
-(void)initialSetupRadioButton
{
    //Set radio button selected for male or female
    if([self.record.maleOrFemale isEqualToString:@"Herr"])
    {
        [self maleFemaleuttonClicked:(UIButton *)[self.view viewWithTag:12]];
    }
    else if ([self.record.maleOrFemale isEqualToString:@"Frau"])
    {
        [self maleFemaleuttonClicked:(UIButton *)[self.view viewWithTag:11]];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Method to choose between male and female
- (IBAction)maleFemaleuttonClicked:(UIButton *)sender
{
    
    switch (sender.tag)
    {
        case 11:
            [addressInfoDict setValue:@"Frau" forKey:@"gender"];
            break;
        case 12:
            [addressInfoDict setValue:@"Herr" forKey:@"gender"];
            break;
            
        default:
            break;
    }
    
    UIImage *image = [UIImage imageNamed:@"radiobuttonselect.png"]; //the radioButton image when selected
    UIImage *deselectImage = [UIImage imageNamed:@"radiobuttondeselect.png"]; //the radioButton image when deselected
    
    if(sender == (UIButton *)[self.view viewWithTag:11])
    {
        [(UIButton *)[self.view viewWithTag:11] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:12] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
    }
    else if(sender == (UIButton *)[self.view viewWithTag:12])
    {
        [(UIButton *)[self.view viewWithTag:12] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:11] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    
}
#pragma mark -
#pragma mark Keyboard Controls Delegate


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
    
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-115);
    [self.objScrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.objScrollView  setContentOffset:CGPointZero animated:YES];
    
//    if (textField==self.textfieldEmail&&textField.text.length>0)
//    {
//        if (![self validateEmailWithString:textField.text])
//        {
//            [self showAlert];
//            [self.textfieldEmail becomeFirstResponder];
//        }
//    }
    if(textField==self.textfieldCentralTel||textField==self.textfieldLandline||textField==self.textfieldMobile)
    {
        int length = [self getLengtWithFirstZero:textField.text];
        NSString *num = [self formatNumber:textField.text];
        
        NSString *str=[[NSString alloc] init];
        
        if(textField.text.length > 0)
        {
            str=[NSString stringWithFormat:@"%c",[textField.text characterAtIndex:0]];
        }
        
        if(length >= 9)
        {
            if([str isEqualToString:@"+"])
            {
                textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ ",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 2)],[num substringWithRange:NSMakeRange(5, 3)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
            }
            
            if([[textField.text substringToIndex:2] isEqualToString:@"00"])
            {
                if(length==12)
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ ",[num  substringToIndex:2],[num substringWithRange:NSMakeRange(2, 2)],[num substringWithRange:NSMakeRange(4, 2)],[num substringWithRange:NSMakeRange(6, 3)],[num substringWithRange:NSMakeRange(9, 2)],[num substringFromIndex:11]];
                }
                if(length>12)
                {
                    
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ ",[num  substringToIndex:2],[num substringWithRange:NSMakeRange(2, 2)],[num substringFromIndex:4]];
                    
                }
                
                
            }
            
            else
            {
                if(length==9)
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 3)],[num substringWithRange:NSMakeRange(6, 2)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
                    
                }
                else
                {
                    textField.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ ",[num  substringToIndex:3],[num substringWithRange:NSMakeRange(3, 2)],[num substringWithRange:NSMakeRange(5, 3)],[num substringWithRange:NSMakeRange(8, 2)],[num substringFromIndex:10]];
                    
                }
            }
        }
    }
    
    [textField resignFirstResponder];
    
}

- (void)showAlert
{
    [[[UIAlertView alloc] initWithTitle:@"Fehler im Formular" message:@"Bitte geben Sie eine gültige Emailadresse an." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
}
#pragma mark Database methods
-(void)saveButtonClicked
{
    if (![self validateEmailWithString:self.textfieldEmail.text] && self.textfieldEmail.text.length>0)
    {
        [self showAlert];
       // [self.textfieldEmail becomeFirstResponder];
    }
    else
    {
        
        [self insertRecord];
        [self.view endEditing:YES];
        [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
    }
}

- (void)popViewController
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[NewOrderViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)insertRecord
{
    
    [recordDict setValue:self.textfieldCompany.text forKey:@"companyName"];
    [recordDict setValue:self.textfieldStereetNo.text forKey:@"streetNo"];
    [recordDict setValue:self.textfieldZipcode.text forKey:@"zipCode"];
    [recordDict setValue:self.textfieldPlace.text forKey:@"place"];
    [recordDict setValue:self.textfieldCentralTel.text forKey:@"centralPhoneNo"];
    [recordDict setValue:[addressInfoDict valueForKey:@"gender"] forKey:@"maleOrFemale"];
    [recordDict setValue:self.textfieldFirstName.text forKey:@"firstName"];
    [recordDict setValue:self.textfieldLastName.text forKey:@"lastName"];
    [recordDict setValue:self.textfieldLandline.text forKey:@"landlinePhoneNo"];
    [recordDict setValue:self.textfieldMobile.text forKey:@"mobilePhoneNo"];
    [recordDict setValue:self.textfieldEmail.text forKey:@"emailID"];
    
    //Add data to Maser Record
    if(!isFromAddressTabOwner)
    {
        [masterDict setValue:@"" forKey:@"orderTableToOwnerAddress"];
    }
    
    
    
    [DatabaseHandler insertRecord:record withData:recordDict withMasterData:masterDict withEntityNmae:@"OwnerAddress"];
    [self addOwnerName];
    
}
-(void)addOwnerName
{
    if(self.textfieldCompany.text.length==0)
    {
        record.ownerName=@"(Ohne Firma)";
        if(self.textfieldFirstName.text.length!=0)
        {
            record.ownerName=self.textfieldFirstName.text;
            if(self.textfieldLastName.text.length!=0)
            {
                record.ownerName=[NSString stringWithFormat:@"%@ %@",MASTER_RECORD.ownerNmae,self.textfieldLastName.text];
            }
            
        }
    }
    else
    {
        record.ownerName=self.textfieldCompany.text;
    }
}


#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}
-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int length = (int)[mobileNumber length];
    return length;
}
-(int)getLengtWithFirstZero:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    int length = (int)[mobileNumber length]-1;
    return length;
}
-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        NSLog(@"%@", mobileNumber);
        
    }
    
    
    return mobileNumber;
}

@end
