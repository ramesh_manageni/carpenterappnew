//
//  ListBillingAddressViewController.h
//  Service48
//
//  Created by Redbytes on 21/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListObjectAddressCustomCell.h"

@interface ListBillingAddressViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *list;
    NSString *tempStr;
    NSInteger addressIndex;
    UIButton *button;
    NSIndexPath *deleteIndexPath;
    
}

@property(strong,nonatomic)UITableView *tableviewObject;

@property BOOL showRadioButton;

@property int senderTag;


@end
