//
//  BillingAddressViewController.m
//  Service48
//
//  Created by Redbytes on 20/11/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "BillingAddressViewController.h"
#import "BSKeyboardControls.h"
#import "AppDelegate.h"
#import "NavigationUtility.h"
#import "BillingAddress.h"


@interface BillingAddressViewController ()

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (nonatomic,strong)NSArray* fetchedRecordsArray;

@property (nonatomic, strong)NSMutableDictionary *addressInfoDict;
@property (nonatomic, strong)NSMutableDictionary *recordDict;
@property (nonatomic, strong)NSMutableDictionary *masterDict;

@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;

@property (strong, nonatomic) IBOutlet UITextField *textfieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *textfieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *textfieldCompany;
@property (strong, nonatomic) IBOutlet UITextField *textfieldStereetNo;
@property (strong, nonatomic) IBOutlet UITextField *textfieldZipcode;
@property (strong, nonatomic) IBOutlet UITextField *textfieldPlace;
@property (strong, nonatomic) IBOutlet UITextField *textfieldEmail;

@property (strong, nonatomic) IBOutlet UIButton *buttonMale;
@property (strong, nonatomic) IBOutlet UIButton *buttonFemale;

@end

@implementation BillingAddressViewController

@synthesize buttonTag;
@synthesize record,recordDict,addressInfoDict,masterDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
        
    }
    //Dictionary used to store(temporary) address information
    addressInfoDict=[[NSMutableDictionary alloc] init];
    recordDict=[[NSMutableDictionary alloc] init];
    masterDict=[[NSMutableDictionary alloc] init];
    
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    
    //Set keyboard controls to all textfields
    NSArray *fields = @[self.textfieldCompany,
                        self.textfieldStereetNo,
                        self.textfieldZipcode,
                        self.textfieldPlace,
                        self.textfieldFirstName,
                        self.textfieldLastName];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    
    [self initialSetup];
    [self.objScrollView setCanCancelContentTouches:YES];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(isOrderSentGlobal)
    {
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
        
        NSArray *subviews=self.view.subviews;
        for(UIView *view in subviews)
        {
            if([view isKindOfClass:[UIScrollView class]])
            {
                for(UIView *textField in view.subviews)
                {
                    if([textField isKindOfClass:[UITextField class]])
                    {
                        [(UITextField *)textField setEnabled:NO];
                    }
                    if([textField isKindOfClass:[UIButton class]])
                    {
                        [(UIButton *)textField setEnabled:NO];
                    }
                    
                }
            }
        }
        
        
    }
    [self.navigationItem setHidesBackButton:YES animated:NO];
    
    NSInteger tag=self.tabBarController.tabBar.selectedItem.tag;
    if(tag==3)
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    }
    else
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
        
    }
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    
    [self setKeyboardControls:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    
    [self setKeyboardControls:nil];
}

-(void)initialSetup
{
    
    self.textfieldCompany.text=self.record.companyName;
    self.textfieldStereetNo.text=self.record.streetNo;
    self.textfieldZipcode.text=self.record.zipCode;
    self.textfieldPlace.text=self.record.place;
    self.textfieldFirstName.text=self.record.firstName;
    self.textfieldLastName.text=self.record.lastName;
    //self.textfieldEmail.text=self.record.email;
    
    [self initialSetupRadioButton];
}
-(void)initialSetupRadioButton
{
    //Set radio button selected for male or female
    if([self.record.maleOrFemale isEqualToString:@"Herr"])
    {
        [self maleFemaleButtonClicked:(UIButton *)[self.view viewWithTag:2]];
    }
    else if ([self.record.maleOrFemale isEqualToString:@"Frau"])
    {
        [self maleFemaleButtonClicked:(UIButton *)[self.view viewWithTag:1]];
    }
    
}

// Method to choose between male and female
- (IBAction)maleFemaleButtonClicked:(UIButton *)sender
{
    
    switch (sender.tag)
    {
        case 1:
            [addressInfoDict setValue:@"Frau" forKey:@"gender"];
            break;
        case 2:
            [addressInfoDict setValue:@"Herr" forKey:@"gender"];
            break;
            
        default:
            break;
    }
    
    UIImage *image = [UIImage imageNamed:@"radiobuttonselect.png"]; //the radioButton image when selected
    UIImage *deselectImage = [UIImage imageNamed:@"radiobuttondeselect.png"]; //the radioButton image when deselected
    
    if(sender == (UIButton *)[self.view viewWithTag:1])
    {
        [(UIButton *)[self.view viewWithTag:1] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:2] setBackgroundImage:deselectImage forState:UIControlStateNormal];
        
    }
    else if(sender == (UIButton *)[self.view viewWithTag:2])
    {
        [(UIButton *)[self.view viewWithTag:2] setBackgroundImage:image forState:UIControlStateNormal];
        
        [(UIButton *)[self.view viewWithTag:1] setBackgroundImage:deselectImage forState:UIControlStateNormal];
    }
    
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(saveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
}
- (void)popViewController
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[NewOrderViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate
- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
    
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-113);
    [self.objScrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.objScrollView  setContentOffset:CGPointZero animated:YES];
    
//    if (textField==self.textfieldEmail && textField.text.length>0)
//    {
//        if (![self validateEmailWithString:textField.text])
//        {
//            [self showAlert];
//            [self.textfieldEmail becomeFirstResponder];
//        }
//    }
    
    
    [textField resignFirstResponder];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)showAlert
{
    [[[UIAlertView alloc] initWithTitle:@"Fehler im Formular" message:@"Bitte geben Sie eine gültige Emailadresse an." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark Database methods
-(void)saveButtonClicked
{
    if (![self validateEmailWithString:self.textfieldEmail.text] && self.textfieldEmail.text.length>0)
    {
        [self showAlert];
        //[self.textfieldEmail becomeFirstResponder];
    }
    else
    {
        [self insertRecord];
        [self.view endEditing:YES];
        [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
    }
}

-(void)insertRecord
{
    [recordDict setValue:self.textfieldFirstName.text forKey:@"firstName"];
    [recordDict setValue:self.textfieldLastName.text forKey:@"lastName"];
    
    [recordDict setValue:self.textfieldCompany.text forKey:@"companyName"];
    [recordDict setValue:self.textfieldStereetNo.text forKey:@"streetNo"];
    [recordDict setValue:self.textfieldZipcode.text forKey:@"zipCode"];
    [recordDict setValue:self.textfieldPlace.text forKey:@"place"];
    
    [recordDict setValue:[addressInfoDict valueForKey:@"gender"] forKey:@"maleOrFemale"];
    
    //Add data to Maser Record
    if(!isFromAddressTabBilling)
    {
        [masterDict setValue:@"" forKey:@"orderTableToBillingAddress"];
    }
    
    [DatabaseHandler insertRecord:record withData:recordDict withMasterData:masterDict withEntityNmae:@"BillingAddress"];
    [self addLodgerNameToOrder];
}
-(void)addLodgerNameToOrder
{
    if(self.textfieldCompany.text.length==0)
    {
        record.billingFirmaName=@"(Ohne Firma)";
        if(self.textfieldFirstName.text.length!=0)
        {
            record.billingFirmaName=self.textfieldFirstName.text;
            if(self.textfieldLastName.text.length!=0)
            {
                record.billingFirmaName=[NSString stringWithFormat:@"%@ %@",MASTER_RECORD.billingFirma,self.textfieldLastName.text];
            }
            
        }
    }
    else
    {
        record.billingFirmaName=self.textfieldCompany.text;
    }
}



@end
