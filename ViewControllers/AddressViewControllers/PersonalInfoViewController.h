//
//  PersonalInfoViewController.h
//  Service48
//
//  Created by Redbytes on 02/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "PersonalInformation.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface PersonalInfoViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate>
{
    
}
@property(strong,nonatomic) PersonalInformation *record;

- (IBAction)maleFemaleButtonClicked:(UIButton *)sender;//To choose gender

@end
