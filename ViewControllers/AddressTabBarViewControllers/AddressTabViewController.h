//
//  AddressTabViewController.h
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListObjectAddressViewController.h"
#import "ObjectAddressViewController.h"
#import "TenatAddressViewController.h"
#import "OwnerAddressViewController.h"
#import "PersonalInfoViewController.h"
#import "ListOwnerAddressViewController.h"
#import "ListCTCAddressViewController.h"
#import "ListCTObjectViewController.h"
#import"ListBillingAddressViewController.h"

@interface AddressTabViewController : UIViewController<UITabBarControllerDelegate>

- (IBAction)allButtonClicked:(UIButton *)sender;//To show Address list



@end
