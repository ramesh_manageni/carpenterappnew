//
//  ContactConfirmationViewController.h
//  Service48
//
//  Created by Rameez on 5/30/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ContactConfirmationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ABNewPersonViewControllerDelegate, ABPeoplePickerNavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *numberLabel;
@property (strong, nonatomic) IBOutlet UITableView *phoneNumberTableview;
@property (nonatomic, strong) ABNewPersonViewController *addressBookController;
@property (nonatomic, strong) NSString *numberString;
@property (nonatomic, weak) ABPeoplePickerNavigationController *picker;
@end
