//
//  AddressTabViewController.m
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AddressTabViewController.h"
#import "ServiceAdressesViewController.h"
#import "NavigationUtility.h"

@interface AddressTabViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnAddress48;
@property (strong, nonatomic) IBOutlet UIButton *btnAddressObject;
@property (strong, nonatomic) IBOutlet UIButton *btnAddressOwner;
@property (strong, nonatomic) IBOutlet UIButton *btnAddressCTCompany;
@property (strong, nonatomic) IBOutlet UIButton *btnAddressCTObject;
@property (strong, nonatomic) IBOutlet UIButton *btnPersonalInfo;
@property (strong, nonatomic) IBOutlet UIButton *btnAddressBilling;

@end

@implementation AddressTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    
    self.objScrollView.showsHorizontalScrollIndicator=NO;
    self.objScrollView.showsVerticalScrollIndicator=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    self.tabBarController.delegate=self;
    [self.objScrollView setCanCancelContentTouches:YES];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSInteger tag=self.tabBarController.tabBar.selectedItem.tag;
    if(tag==3)
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    }
    else
    {
        self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
        
    }
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgImage setImage:[UIImage imageNamed:@"bluebg.jpg"]];
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)allButtonClicked:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            
            ServiceAdressesViewController *objServiceAdressesViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ServiceAdressesViewController"];
            [self.navigationController pushViewController:objServiceAdressesViewController animated:NO];
            
            
            break;

        }
        case 1:
        {
            ListObjectAddressViewController *objListObjectAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListObjectAddressViewController"];
            objListObjectAddressViewController.showRadioButton = NO;//To hide radio button
            isFromAddressTabPropertyAddress=YES;//differentiate from Order and address
            
            [self.navigationController pushViewController:objListObjectAddressViewController animated:NO];

            break;
        }
        case 2:
        {
            ListOwnerAddressViewController *objListOwnerAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListOwnerAddressViewController"];
            objListOwnerAddressViewController.showRadioButton=NO;
            
            isFromAddressTabOwner=YES;
            
            [self.navigationController pushViewController:objListOwnerAddressViewController animated:NO];
            break;
        }
        case 3:
        {
            ListCTCAddressViewController *objListCTCAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListCTCAddressViewController"];

            objListCTCAddressViewController.showRadioButton=NO;
            
            isFromAddressTabCTComp=YES;
            [self.navigationController pushViewController:objListCTCAddressViewController animated:NO];
            
            break;
            
        }
        case 4:
        {
            ListCTObjectViewController *objListCTObjectViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListCTObjectViewController"];
            
            isFromAddressTabCTO=YES;
            
            [self.navigationController pushViewController:objListCTObjectViewController animated:NO];
            
            break;
        }
        case 6:
        {
            ListBillingAddressViewController *objListBillingAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListBillingAddressViewController"];
            
            isFromAddressTabBilling=YES;
            
            [self.navigationController pushViewController:objListBillingAddressViewController animated:NO];
            
            break;
        }
        case 5:
        {
            PersonalInfoViewController *objPersonalInfoViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
            [self.navigationController pushViewController:objPersonalInfoViewController animated:NO];
            break;
        }
        default:
            break;
    }
}
#pragma mark Tabbar Delegate
- (void)tabBarController:(UITabBarController *)tabBarController
 didSelectViewController:(UIViewController *)viewController
{
    NSInteger tabitem = tabBarController.selectedIndex;
    [[tabBarController.viewControllers objectAtIndex:tabitem] popToRootViewControllerAnimated:NO];

}

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}


@end
