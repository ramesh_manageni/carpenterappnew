//
//  ContactConfirmationViewController.m
//  Service48
//
//  Created by Rameez on 5/30/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ContactConfirmationViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ContactConfirmationViewController ()<ABPersonViewControllerDelegate>

@end

@implementation ContactConfirmationViewController
@synthesize numberString;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)cancelButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Info"];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonAction:)];
    
    [self.numberLabel setFont:[UIFont boldSystemFontOfSize:16]];
    self.numberLabel.text = numberString;
	// Do any additional setup after loading the view.
}

#pragma mark UITableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    switch (indexPath.row)
    {
        case 0:
            cell.textLabel.text = @"Create New Contact";
            break;
        case 1:
            cell.textLabel.text = @"Add to Existing Contacts";
            break;
        default:
            break;
    }
    return cell;
}

#pragma mark UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            [self addContactwithNumber:numberString];
            break;
        }
        case 1:
        {
            [self existingContactwithNumber:numberString];
            break;
        }
        default:
            break;
    }
}

-(void)existingContactwithNumber:(NSString *)contactNumber
{
    
    ABPeoplePickerNavigationController *c = [[ABPeoplePickerNavigationController alloc] init];
    [c setPeoplePickerDelegate:self];// = self;
    [self.navigationController presentViewController:c animated:YES completion:nil];
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person;
{
    [self updatePhone:person phone:numberString];
    return YES;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    return YES;
}
-(BOOL)personViewController:(ABPersonViewController *)personViewController shouldPerformDefaultActionForPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    return YES;
}
-(void)updatePhone:(ABRecordRef)person phone:(NSString*)phone
{
    ABMutableMultiValueRef phoneNumberMultiValue =  ABMultiValueCreateMutableCopy (ABRecordCopyValue(person, kABPersonPhoneProperty));
    
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge  CFTypeRef)(phone),  kABPersonPhoneMainLabel, NULL);
    ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    CFRelease(phoneNumberMultiValue);
}

-(void)addContactwithNumber:(NSString *)contactNumber
{
    ABRecordRef person = ABPersonCreate();
    
    
    // Adding phone numbers
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(numberString), (CFStringRef)@"+41", NULL);
    ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    CFRelease(phoneNumberMultiValue);
    
    
    // Creating view controller for a new contact
    ABNewPersonViewController *c = [[ABNewPersonViewController alloc] init];
    [c setNewPersonViewDelegate:self];
    [c setDisplayedPerson:person];
    CFRelease(person);
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:c];
    
    [self.navigationController presentViewController:navController animated:YES completion:nil];
    
    
}

-(void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(ABRecordRef)person
{
    if (person) {
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookAddRecord(addressBook, person, nil);
        ABAddressBookSave(addressBook, nil);
        CFRelease(addressBook);
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
