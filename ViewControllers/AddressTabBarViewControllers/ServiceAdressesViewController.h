//
//  ServiceAdressesViewController.h
//  Service48
//
//  Created by RedBytes on 21/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<MessageUI/MessageUI.h>

@interface ServiceAdressesViewController : UIViewController <UIActionSheetDelegate,MFMailComposeViewControllerDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *_scrollView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@property (weak, nonatomic) IBOutlet UILabel *sec1Label1;

@property (weak, nonatomic) IBOutlet UILabel *sec1Label2;

@property (weak, nonatomic) IBOutlet UILabel *sec2Label1;

@property (weak, nonatomic) IBOutlet UILabel *sec2Label2;

@property (weak, nonatomic) IBOutlet UILabel *sec3Label1;

@property (weak, nonatomic) IBOutlet UILabel *sec3Label2;

@property (weak, nonatomic) IBOutlet UILabel *sec4Label1;

@property (weak, nonatomic) IBOutlet UILabel *sec4Label2;

@property (strong, nonatomic) IBOutlet UIButton *address1Button;
@property (strong, nonatomic) IBOutlet UIButton *address2Button;

@property (strong, nonatomic) IBOutlet UIButton *address3Button;


- (IBAction)sectionsButtonAction:(id)sender;//To show map for perticular address

-(IBAction)savePhoneNumberButtonAction:(id)sender;//To add contacts on device

- (IBAction)mailButton1Clicked:(id)sender;//To send mail For headquaters workshop

- (IBAction)mailButton2Clicked:(id)sender;//To send mail For branch of aargau

- (IBAction)mailButton3Clicked:(id)sender;//To send mail For branch vinterthur fruenfeld















    
@end
