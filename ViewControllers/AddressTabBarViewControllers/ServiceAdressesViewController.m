//

//  ServiceAdressesViewController.m
//  Service48
//
//  Created by RedBytes on 21/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#define phoneNumber1 121
#define phoneNumber2 122
#define phoneNumber3 123
#define phoneNumber4 124

#import "ServiceAdressesViewController.h"
#import "ContactConfirmationViewController.h"
#import<MessageUI/MessageUI.h>

@interface ServiceAdressesViewController ()

@end

@implementation ServiceAdressesViewController
@synthesize _scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    // Do any additional setup after loading the view from its nib.
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.frame];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    _scrollView.showsHorizontalScrollIndicator=NO;
    _scrollView.showsVerticalScrollIndicator=NO;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"bluebg.jpg"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.frame];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Adressen" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    
}
-(void)viewDidLayoutSubviews
{
    [_scrollView setFrame:CGRectMake(self.view.frame.origin.x, -10, self.view.frame.size.width, self.view.frame.size.height+10)];
    [_scrollView setContentSize:CGSizeMake(320, 830)];
}

#pragma mark NavigationbarButton Items
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sectionsButtonAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    switch (button.tag)
    {
        case 1:
        {
            NSString *urlString = @"https://maps.google.de/maps?q=Zürcherstrasse+137d+8952+Schlieren+Schweiz&hl=de&ie=UTF8&ll=47.396795,8.465202&spn=0.010632,0.033023&sll=47.397553,8.460747&sspn=0.001336,0.004128&hnear=Zürcherstrasse+137,+8952+Schlieren,+Zürich,+Suiza&t=m&z=16";
            
            NSString* webStringURL = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL* url = [NSURL URLWithString:webStringURL];
            
            if (![[UIApplication sharedApplication] openURL:url])
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            
            NSLog(@"%ld",(long)button.tag);
            break;
        }
        case 2:
        {
            NSString *urlString = @"https://maps.google.de/maps?q=Köllikerstr+32,+5036+Oberentfelden+Schweiz&hl=de&ie=UTF8&ll=47.352861,8.041783&spn=0.002754,0.008256&sll=47.352861,8.041783&sspn=0.098386,0.264187&hnear=Köllikerstr+32,+5036+Oberentfelden,+Schweiz&t=m&z=16";
            
            NSString* webStringURL = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL* url = [NSURL URLWithString:webStringURL];
            
            if (![[UIApplication sharedApplication] openURL:url])
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            NSLog(@"%ld",(long)button.tag);
            break;
        }
        case 3:
        {
            NSString *urlString = @"https://maps.google.de/maps?q=Tösstalstrasse+86,+8400+Winterthur,+Suiza&hl=de&ie=UTF8&ll=47.493385,8.742092&spn=0.010656,0.033023&sll=47.493545,8.741984&sspn=0.010714,0.033023&oq=Tösstalstrasse+86+8400,+Winterthur,+Suiza&hnear=Tösstalstrasse+86,+8400+Winterthur,+Zürich,+Suiza&t=m&z=16";
            
            NSString* webStringURL = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL* url = [NSURL URLWithString:webStringURL];
            
            if (![[UIApplication sharedApplication] openURL:url])
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            NSLog(@"%ld",(long)button.tag);
            break;
        }
        default:
            break;
    }
    
}
-(IBAction)savePhoneNumberButtonAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    switch (button.tag)
    {
        case phoneNumber1:
        {
            NSLog(@"%ld",(long)button.tag);
            
            UIActionSheet *phoneNumberActionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"stornieren" destructiveButtonTitle:nil otherButtonTitles:@"Zifferblatt nummer",@"Nachricht senden",@"Zu Kontakten hinzufügen",@"Kopie", nil];
            phoneNumberActionSheet.tag = 131;
            [phoneNumberActionSheet showInView:[self.view window]];
            break;
        }
        case phoneNumber2:
        {
            NSLog(@"%ld",(long)button.tag);
            UIActionSheet *phoneNumberActionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"stornieren" destructiveButtonTitle:nil otherButtonTitles:@"Zifferblatt nummer",@"Nachricht senden",@"Zu Kontakten hinzufügen",@"Kopie", nil];
            phoneNumberActionSheet.tag = 132;
            [phoneNumberActionSheet showInView:[self.view window]];
            break;
        }
        case phoneNumber3:
        {
            NSLog(@"%ld",(long)button.tag);
            UIActionSheet *phoneNumberActionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"stornieren" destructiveButtonTitle:nil otherButtonTitles:@"Zifferblatt nummer",@"Nachricht senden",@"Zu Kontakten hinzufügen",@"Kopie", nil];
            phoneNumberActionSheet.tag = 133;
            [phoneNumberActionSheet showInView:[self.view window]];
            break;
        }
        case phoneNumber4:
        {
            NSLog(@"%ld",(long)button.tag);
            UIDevice *device = [UIDevice currentDevice];
            
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"+41800554848"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                
            } else {
                
                [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Ihr Gerät diese Funktion nicht unterstützt." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
            }
            
        }
            
        default:
            break;
    }
}

- (IBAction)mailButton1Clicked:(id)sender
{
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    NSArray *toRecipents = [NSArray arrayWithObject:@"info@service48.ch"];
    [objMFMailcomposeVc setToRecipients:toRecipents];
    //[self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Keine Mail-Konten" message:@"Bitte setzen Sie ein Mail-Konto, um E-Mail senden." delegate:self cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }

}

- (IBAction)mailButton2Clicked:(id)sender
{
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    NSArray *toRecipents = [NSArray arrayWithObject:@"aargau@service48.ch"];
    [objMFMailcomposeVc setToRecipients:toRecipents];
    //[self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Keine Mail-Konten" message:@"Bitte setzen Sie ein Mail-Konto, um E-Mail senden." delegate:self cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }

}

- (IBAction)mailButton3Clicked:(id)sender
{
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    NSArray *toRecipents = [NSArray arrayWithObject:@"winterhur@service48.ch"];
    [objMFMailcomposeVc setToRecipients:toRecipents];
   // [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Keine Mail-Konten" message:@"Bitte setzen Sie ein Mail-Konto, um E-Mail senden." delegate:self cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }

}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            
            break;
            
        case MFMailComposeResultSaved:
            
            NSLog(@"Mail saved");
            break;
            
        case MFMailComposeResultSent:
        {
            NSLog(@"Mail sent");
            break;
        }
            
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
            
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark UIActionSheetDelegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (actionSheet.tag)
    {
        case 131:
            if(buttonIndex == 0)
            {
                UIDevice *device = [UIDevice currentDevice];
                
                if ([[device model] isEqualToString:@"iPhone"] ) {
                    
                    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"+41447304828"];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                    
                } else {
                    
                    [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Ihr Gerät diese Funktion nicht unterstützt." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
                }
            }
            if(buttonIndex == 1)
            {
                NSString *stringURL = @"sms:+41447304828";
                NSURL *url = [NSURL URLWithString:stringURL];
                [[UIApplication sharedApplication] openURL:url];
            }
            else if (buttonIndex == 2)
            {
                [self phoneNumber01];
            }
            else if (buttonIndex == 3)
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = @"+41 (0)44 730 48 28";
            }
            break;
            
        case 132:
            if(buttonIndex == 0)
            {
                UIDevice *device = [UIDevice currentDevice];
                
                if ([[device model] isEqualToString:@"iPhone"] ) {
                    
                    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"+41564704848"];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                    
                } else {
                    
                    [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Ihr Gerät diese Funktion nicht unterstützt." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
                }
            }
            
            if(buttonIndex == 1)
            {
                NSString *stringURL = @"sms:+41564704848";
                NSURL *url = [NSURL URLWithString:stringURL];
                [[UIApplication sharedApplication] openURL:url];
            }
            else if (buttonIndex == 2)
            {
                [self phoneNumber02];
            }
            else if (buttonIndex == 3)
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = @"+41 (0)56 470 48 48";
                
            }
            break;
            
        case 133:
            if(buttonIndex == 0)
            {
                UIDevice *device = [UIDevice currentDevice];
                
                if ([[device model] isEqualToString:@"iPhone"] ) {
                    
                    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"+41522384848"];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                    
                } else {
                    
                    [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Ihr Gerät diese Funktion nicht unterstützt." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
                }
            }
            
            if(buttonIndex == 1)
            {
                NSString *stringURL = @"sms:+41522384848";
                NSURL *url = [NSURL URLWithString:stringURL];
                [[UIApplication sharedApplication] openURL:url];
            }
            else if (buttonIndex == 2)
            {
                [self phoneNumber03];
            }
            else if (buttonIndex == 3)
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = @"+41 (0)52 238 48 48";
                
            }
            break;
            
        default:
            break;
    }
}

-(void)phoneNumber01
{
    [self addContactDetailsofthePersonwithName:@"" andNumber:@"+41 (0)44 730 48 28"];
}

-(void)phoneNumber02
{
    [self addContactDetailsofthePersonwithName:@"" andNumber:@"+41 (0)56 470 48 48"];
}

-(void)phoneNumber03
{
    [self addContactDetailsofthePersonwithName:@"" andNumber:@"+41 (0)52 238 48 48"];
}

-(void)phoneNumber04
{
    [self addContactDetailsofthePersonwithName:@"" andNumber:@"+41 (0)80 055 48 48"];
}


-(void)addContactDetailsofthePersonwithName:(NSString *)name andNumber:(NSString *)phoneNumber
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactConfirmationViewController *confirmationContact = [storyboard instantiateViewControllerWithIdentifier:@"ContactConfirmationViewController"];
    
    confirmationContact.numberString = phoneNumber;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:confirmationContact];
    [self presentViewController:navigationController animated:YES completion:^{
    }];
}

@end
