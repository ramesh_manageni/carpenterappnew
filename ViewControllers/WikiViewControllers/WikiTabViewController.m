//
//  WikiTabViewController.m
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "WikiTabViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"

@interface WikiTabViewController ()

@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic) Reachability *wifiReachability;

@end

@implementation WikiTabViewController
@synthesize internetReachability;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	
	// Regiser for HUD callbacks so we can remove it from the window at the right time
	HUD.delegate = (id)self;
    
    //Check screen height
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                webViewForWiki = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-49)];
                
            }
            
            else if (pixelHeight == 1136.0f)
            {
                webViewForWiki = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 568-49)];
                
            }
        }
    }
    
    [webViewForWiki setDelegate:self];
    webViewForWiki.scalesPageToFit = YES;
    [self.view addSubview:webViewForWiki];
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view setBackgroundColor:[UIColor blackColor]];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    webViewForWiki.opaque = NO;
    webViewForWiki.backgroundColor = [UIColor clearColor];
    
    for(UIView *view in webViewForWiki.subviews){
        if ([view isKindOfClass:[UIImageView class]]) {
            // to transparent
            [view removeFromSuperview];
        }
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView *sView = (UIScrollView *)view;
            //to hide Scroller bar
            sView.showsVerticalScrollIndicator = NO;
            sView.showsHorizontalScrollIndicator = NO;
            for (UIView* shadowView in [sView subviews]){
                //to remove shadow
                if ([shadowView isKindOfClass:[UIImageView class]]) {
                    [shadowView setHidden:TRUE];
                }
            }
        }
    }
    
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgImage setImage:[UIImage imageNamed:@"bluebg.jpg"]];
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
    
    
    internetReachability = [Reachability reachabilityForInternetConnection];
    [internetReachability startNotifier];
    [self updateInterfaceWithReachability:internetReachability];
    
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [super viewWillDisappear:animated];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    NSString *urlAddress = @"http://wiki.service48.ch/";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    switch (netStatus)
    {
        case NotReachable:
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Es kann keine Verbindung hergestellt werden" message:@"Sie haben derzeit keine funtionierende Internetverbindung. Versuchen Sie erneut auf diesen Bereich zuzugreifen wenn Sie über eine Internetverbindung verfügen." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
            
            break;
        }
        case ReachableViaWWAN:
        {
            //Load the request in the UIWebView.
            [webViewForWiki loadRequest:requestObj];
            
            break;
        }
        case ReachableViaWiFi:
        {
            ///Load the request in the UIWebView.
            [webViewForWiki loadRequest:requestObj];
            
            break;
        }
        default:
        {
            break;
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Webview Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES labelText:nil];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[[UIAlertView alloc] initWithTitle:@"Es kann keine Verbindung hergestellt werden" message:@"Sie haben derzeit keine funtionierende Internetverbindung. Versuchen Sie erneut auf diesen Bereich zuzugreifen wenn Sie über eine Internetverbindung verfügen." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
    
}

#pragma mark -
#pragma mark - UIScrollView Delegate Methods

@end
