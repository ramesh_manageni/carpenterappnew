//
//  WikiTabViewController.h
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface WikiTabViewController : UIViewController<UIWebViewDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    UIWebView *webViewForWiki;

}
@end
