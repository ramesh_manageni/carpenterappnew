//
//  NewOrderViewController.h
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListObjectAddressViewController.h"
#import "ObjectAddressViewController.h"
#import "TenatAddressViewController.h"
#import "OwnerAddressViewController.h"
#import "PersonalInfoViewController.h"
#import "ListOwnerAddressViewController.h"
#import "ListCTCAddressViewController.h"
#import "ListCTObjectViewController.h"
#import"PhotoTabViewController.h"
#import "OrderTable.h"
#import"Tasks.h"
#import"OrderDescription.h"
#import"NSData+Base64.h"
#import "PhotoDetail.h"
#import"EmailFeddbackViewController.h"

#import<MessageUI/MessageUI.h>

@interface NewOrderViewController : UIViewController<UITabBarControllerDelegate,MFMailComposeViewControllerDelegate>

@property BOOL isOrderSent;
@property(strong,nonatomic) OrderTable *masterRecord;

- (IBAction)allButtonAction:(UIButton *)sender;//To choose perticular detail of order,for all detail single button is used



@end
