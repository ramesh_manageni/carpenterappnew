//
//  ListOrdersViewController.h
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListOrdersCustomCell.h"
#import "NewOrderViewController.h"
#import "OrderTable.h"


@interface ListOrdersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate>

{
    NSMutableArray *list;
    NSInteger addressIndex;
    UIButton *button;
    NSIndexPath *deleteIndexPath;


}
@property(strong,nonatomic)UITableView *tableviewObject;
@end
