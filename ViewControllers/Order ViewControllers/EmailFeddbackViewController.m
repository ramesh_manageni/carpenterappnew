//
//  EmailFeddbackViewController.m
//  Service48
//
//  Created by Redbytes on 02/06/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "EmailFeddbackViewController.h"

@interface EmailFeddbackViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *objImageView;
@property (strong, nonatomic) IBOutlet UILabel *topLabel;
@property (strong, nonatomic) IBOutlet UILabel *bottomLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *objScrollView;

@end

@implementation EmailFeddbackViewController
@synthesize objScrollView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.objScrollView.delegate=self;
   
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    NSString* topString = @"Eine Kopie der Auftragserteilung ist Ihnen per E-Mail zugestellt worden.";
    NSString* bottomString = @"Nachdem Service 48 den Auftrag erfasst hat, erhalten Sie eine Auftragsbestätigung\nper E-Mail.\nIhr Mieter wird per SMS informiert, vorausgesetzt Sie konnten uns seine \nMobile-Nummer angeben. Andernfalls erhält er eine schriftliche Bestätigung per Post.";
    
    
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
 
    [style setLineSpacing:2.8];
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    self.topLabel.attributedText = [[NSAttributedString alloc] initWithString:topString
                                                                   attributes:attributtes];
    [self.topLabel sizeToFit];
  
    self.bottomLabel.attributedText = [[NSAttributedString alloc] initWithString:bottomString
                                                                   attributes:attributtes];
    [self.bottomLabel sizeToFit];
}
-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if ([[UIScreen mainScreen] bounds].size.height == 568) //iphone 5/5c/5s
    {
        self.objScrollView.userInteractionEnabled=NO;
        [self.objScrollView setContentSize:CGSizeMake(320, self.view.bounds.size.height-64)];
        self.objScrollView.frame = CGRectMake(0, 64, 320, self.view.bounds.size.height-64);
    }
    else //iphone 4/4s
    {
        //making ContentSize greater than frame's height as we need scrolling
        //make changes in height if necessary
        [self.objScrollView setContentSize:CGSizeMake(320, self.view.bounds.size.height+64)];
        if([[UIDevice currentDevice].systemVersion hasPrefix:@"7"]) //iOS 7.0 >
        {
            //made changes in y as status bar height is counted
            self.objScrollView.frame = CGRectMake(0, 64, 320, self.view.bounds.size.height-64);
        }
        else //iOS 6.1 <
        {
            //made changes in y as status bar height not counted
            self.objScrollView.frame = CGRectMake(0, 44, 320, self.view.bounds.size.height-64);
        }
    }

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Vielen Dank!" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgImage setImage:[UIImage imageNamed:@"bluebg.jpg"]];
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
    
   }
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}


#pragma mark back baar Button
-(void)backButtonClicked
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}
#pragma mark ScrollView Delegate

@end
