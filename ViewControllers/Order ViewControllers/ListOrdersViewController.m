//
//  ListOrdersViewController.m
//  Service48
//
//  Created by Redbytes on 15/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ListOrdersViewController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "PropertyAddress.h"

@interface ListOrdersViewController ()

@property (nonatomic,strong)NSArray* fetchedRecordsArray;
@property(nonatomic,strong)NSMutableArray *openOrderArray;
@property(nonatomic,strong)NSMutableArray *sentOrderArray;


@end

@implementation ListOrdersViewController
@synthesize tableviewObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    addressIndex=-1;
    
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    self.tabBarController.delegate=self;
    // Do any additional setup after loading the view from its nib.
    list=[[NSMutableArray alloc]initWithObjects:@"Neuer Auftrag",@"Offener Auftrag",@"Zugestellte Aufträge", nil];
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
    
    //Check screen height
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(10, 0, 300, 365) style:UITableViewStylePlain];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                tableviewObject=[[UITableView alloc]initWithFrame:CGRectMake(10, 0, 300, 452) style:UITableViewStylePlain];
            }
        }
    }
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    tableviewObject.backgroundView = nil;
    [tableviewObject setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableviewObject.delegate=self;
    tableviewObject.dataSource=self;
    [tableviewObject setShowsHorizontalScrollIndicator:NO];
    [tableviewObject setShowsVerticalScrollIndicator:NO];
    [self.view addSubview:tableviewObject];
    
    
    CGFloat dummyViewHeight = 40;
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableviewObject.bounds.size.width, dummyViewHeight)];
    self.tableviewObject.tableHeaderView = dummyView;
    self.tableviewObject.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0);
    
    _openOrderArray=[[NSMutableArray alloc] init];
    _sentOrderArray=[[NSMutableArray alloc]init];
    
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:@"empty_row.png"] forState:UIControlStateNormal];
    [button.titleLabel setTextColor:[UIColor whiteColor]];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitle:@"  Keine Auftrag vorhanden" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    button.frame = CGRectMake(0, 145.0, 300.0, 45.0);
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    
    
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    
    
    self.fetchedRecordsArray= [DatabaseHandler getEntityCountWithName:@"OrderTable"];
    
    if(self.fetchedRecordsArray.count==0)
    {
        [self.tableviewObject addSubview:button];
        
    }
    else
    {
        [button removeFromSuperview];
    }
    
    [self.openOrderArray removeAllObjects];
    [self.sentOrderArray removeAllObjects];
    
    
    
    for(int i=0;i<self.fetchedRecordsArray.count;i++)
    {
        if([[[self.fetchedRecordsArray objectAtIndex:i] isSentMail] isEqualToString:@"YES"])
        {
            [self.sentOrderArray addObject:[self.fetchedRecordsArray objectAtIndex:i]];
            NSLog(@"SENT OERDER:%@",[[self.fetchedRecordsArray objectAtIndex:i] orderName]);
        }
        else
        {
            [self.openOrderArray addObject:[self.fetchedRecordsArray objectAtIndex:i]];
            NSLog(@"OPEN OERDER:%@",[[self.fetchedRecordsArray objectAtIndex:i] orderName]);
            
        }
    }
    
    if(self.sentOrderArray.count==0)
    {
        [list replaceObjectAtIndex:2 withObject:@""];
        
    }
    else
    {
        [list replaceObjectAtIndex:2 withObject:@"Zugestellte Aufträge"];
        
    }
    
    
    [tableviewObject setBackgroundColor:[UIColor clearColor]];
    
    
    [self.view setBackgroundColor:[UIColor colorWithRed:161/255.0f green:168/255.0f blue:224/255.0f alpha:1.0f]];
    
    
    addressIndex=-1;
    
    if(!isFromPhotoTab)
    {
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = nil;
        
        self.fetchedRecordsArray= [DatabaseHandler getEntityCountWithName:@"OrderTable"];
        if(self.fetchedRecordsArray.count>0)
        {
            self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil;
            
        }
        [list replaceObjectAtIndex:0 withObject:@"Neuer Auftrag"];
        
    }
    else
    {
        [self.tableviewObject setBackgroundColor:[UIColor clearColor]];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = [self addRightBarButtonItemFromPhoto];
        self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItemFromPhoto];
        self.fetchedRecordsArray= [DatabaseHandler getEntityCountWithName:@"OrderTable"];
        if(self.fetchedRecordsArray.count>0)
        {
            self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItemFromPhoto];
            [self.navigationItem.leftBarButtonItem setEnabled:NO];
        }
        else
        {
            self.navigationItem.leftBarButtonItem = nil;
            
        }
        
        [list replaceObjectAtIndex:0 withObject:@"Foto an erfassten Auftrag anfügen"];
    }
    [self.tableviewObject reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Table View Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setBackgroundColor:[UIColor clearColor]];
    
    //[label setFont:[UIFont boldSystemFontOfSize:15]];
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    NSString *string =[list objectAtIndex:section];
    
    /* Section header is in 0th index... */
    
    [label setText:string];
    
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
	
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	if(isFromPhotoTab)
    {
        
        return 1;
        
    }
    else
    {
        
        return 3;
        
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isFromPhotoTab)
    {
        return [self.openOrderArray count];
        
    }
    // Return the number of rows in the section.
    else
    {
        if(section==0)
        {
            return 1;
            
        }
        else if (section==1)
        {
            return [self.openOrderArray count];
            
            
            NSLog(@"OPEN ARRAY");
        }
        else if (section==2)
        {
            return [self.sentOrderArray count];
            NSLog(@"sent ARRAY");
            
            
        }
    }
    return 0;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *) indexPath
{
    if(isFromPhotoTab)
    {
        return NO;
        
    }
    else
    {
        if(indexPath.section==0)
        {
            return NO;
        }
    }
    return YES;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isFromPhotoTab)
    {
        return UITableViewCellEditingStyleNone;
    }
    
    return UITableViewCellEditingStyleDelete;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        deleteIndexPath = indexPath;
        [self showAlert];
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    ListOrdersCustomCell *cell ;
    if (cell == nil) {
        cell = [[ListOrdersCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    
    if(!isFromPhotoTab)
    {
        if(indexPath.section==0)
            
        {
            
            
            
            cell.enterOrderLabel.text=@" Auftrag erfassen";
            
        }
        if(indexPath.section==1)
        {
            
            [cell.enterOrderLabel removeFromSuperview];
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            format.dateFormat = @"dd-MM-yy";
            
            cell.dateLabel.text=[[self.openOrderArray objectAtIndex:indexPath.row] orderDate];
            cell.orderLabel.text=[[self.openOrderArray objectAtIndex:indexPath.row] orderName];
            
        }
        if(indexPath.section==2)
        {
            [cell.enterOrderLabel removeFromSuperview];
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            format.dateFormat = @"dd-MM-yy";
            
            cell.dateLabel.text=[[self.sentOrderArray objectAtIndex:indexPath.row] orderDate];
            cell.orderLabel.text=[[self.sentOrderArray objectAtIndex:indexPath.row] orderName];
            
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        
        return cell;
        
    }
    else
    {
        static NSString *CellIdentifier = @"Cell1";
        
        ListObjectAddressCustomCell *cell; //= (ListObjectAddressCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ListObjectAddressCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        [cell.button addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.button setTag:indexPath.row];
        
        
        
        if(addressIndex!=cell.button.tag)
        {
            [cell.button setBackgroundImage:[UIImage imageNamed:@"radiobuttondeselect.png"] forState:UIControlStateNormal];
            cell.addressLabel.text=[[self.openOrderArray objectAtIndex:indexPath.row] orderName];
            return cell;
            
        }
        else
        {
            [cell.button setBackgroundImage:[UIImage imageNamed:@"radiobuttonselect.png"] forState:UIControlStateNormal];
            cell.addressLabel.text=[[self.openOrderArray objectAtIndex:indexPath.row] orderName];
            
            
            [cell setBackgroundColor:[UIColor clearColor]];
            
            return cell;
            
        }
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!isFromPhotoTab)
    {
        if(indexPath.section==0)
        {
            UIImageView* img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellbg2.png"]];
            [cell setBackgroundView:img];
            [cell.backgroundView sendSubviewToBack:img];
            
        }
        else if (indexPath.section==1)
        {
            
            UIImageView* img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellbg1.png"]];
            [cell setBackgroundView:img];
            [cell.backgroundView sendSubviewToBack:img];
            
        }
        else if (indexPath.section==2)
        {
            UIImageView* img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellbg1.png"]];
            [cell setBackgroundView:img];
            [cell.backgroundView sendSubviewToBack:img];        }
    }
    else
    {
        UIImageView* img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellbg1.png"]];
        [cell setBackgroundView:img];
        [cell.backgroundView sendSubviewToBack:img];
    }
}
#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewOrderViewController *objNewOrderViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    
    if(!isFromPhotoTab)
    {
        if(indexPath.section==0)
        {
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSManagedObjectContext *context = [delegate managedObjectContext];
            MASTER_RECORD=[NSEntityDescription insertNewObjectForEntityForName:@"OrderTable" inManagedObjectContext:context];
            
            //Set Date of Order Creation
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            format.dateFormat = @"dd-MM-yy";
            
            MASTER_RECORD.orderDate=[format stringFromDate:[NSDate new]];
            
            objNewOrderViewController.isOrderSent=NO;
            isOrderSentGlobal=NO;
            
        }
        else if (indexPath.section==1)
        {
            MASTER_RECORD=[self.openOrderArray objectAtIndex:indexPath.row];
            objNewOrderViewController.isOrderSent=NO;
            isOrderSentGlobal=NO;
            
        }
        else if (indexPath.section==2)
        {
            MASTER_RECORD=[self.sentOrderArray objectAtIndex:indexPath.row];
            objNewOrderViewController.isOrderSent=YES;
            isOrderSentGlobal=YES;
        }
    }
    else
    {
        MASTER_RECORD=[self.openOrderArray objectAtIndex:indexPath.row];
        objNewOrderViewController.isOrderSent=NO;
        isOrderSentGlobal=NO;
        
        
    }
    
    //
    isFromPhotoTab=0;
    
    [self.navigationController pushViewController:objNewOrderViewController animated:NO];
    
    
}


-(void)radioButtonClicked:(UIButton *)sender
{
    addressIndex=(NSInteger)sender.tag;
    [tableviewObject reloadData];
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
    
    //Add perticular address object to master record
    NSLog(@"SENDER TAG: %ld",(long)addressIndex);
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addRightBarButtonItemFromPhoto
{
    float xValue;
    xValue=0;
    if (!SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        xValue=10;
    }
    
    UIButton *btn = [[UIButton alloc] initWithFrame: CGRectMake(0+xValue, 3, 95.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:@"buttonbg.png"] ;
    [btn setBackgroundImage:backImage  forState:UIControlStateNormal];
    [btn setTitle:@"abbrechen" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 95, 35)];
    btnView.bounds = CGRectOffset(btnView.bounds, 0, 0);
    [btnView addSubview:btn];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:btnView];
    //return buttonItem;
    
    [[buttonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(abortButtonButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return buttonItem;
}
-(UIBarButtonItem *)addLeftBarButtonItemFromPhoto
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"sichern" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(secureButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"bearbeiten" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(toggleEdit) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
}

//Method to change title of Edit/Done button
-(void)toggleEdit
{
    [self.tableviewObject reloadData];
    
    [self.tableviewObject setEditing:!self.tableviewObject.editing animated:NO];
    
    if (self.tableviewObject.editing)
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"fertig" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(30, 3, 70, 30)];
        
    }
    
    else
    {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"bearbeiten" forState:UIControlStateNormal];
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(10, 3, 95, 30)];
        
    }
}
#pragma mark FromPhotoTab NavigationbarButton Items
-(void)abortButtonButtonClicked
{
    isSecureButtonClicked=1;
    isFromPhotoTab=0;
    [self.tabBarController setSelectedIndex:1];
    [[[self.tabBarController viewControllers] objectAtIndex:1] popToRootViewControllerAnimated:NO];
    
    
}
-(void)secureButtonClicked
{
    isFromPhotoTab=0;
    isSecureButtonClicked=1;
    MASTER_RECORD_PHOTO=[self.openOrderArray objectAtIndex:addressIndex];
    [self.tabBarController setSelectedIndex:1];
    
}
#pragma mark Tabbar Delegate
- (void)tabBarController:(UITabBarController *)tabBarController
 didSelectViewController:(UIViewController *)viewController
{
    isPhotoTabSelected=1;
    isSecureButtonClicked=1;
    NSInteger tabitem = tabBarController.selectedIndex;
    [[tabBarController.viewControllers objectAtIndex:tabitem] popToRootViewControllerAnimated:NO];
    
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
}


#pragma mark UIAlertView delegate
- (void)showAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Auftrag löschen" message:@"Möchten Sie den Auftrag wirklich löschen?" delegate:self cancelButtonTitle:@"Löschen" otherButtonTitles:@"Abbrechen", nil];
    
    alert.delegate=self;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)//OK button pressed
    {
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        
        //    1
        [self.tableviewObject beginUpdates];
        // Delete the row from the data source
        
        //    2
        [self.tableviewObject deleteRowsAtIndexPaths:@[deleteIndexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        
        //    3
        if(deleteIndexPath.section==1)
        {
            [context deleteObject:[self.openOrderArray objectAtIndex:deleteIndexPath.row]];
        }
        else if (deleteIndexPath.section==2)
        {
            [context deleteObject:[self.sentOrderArray objectAtIndex:deleteIndexPath.row]];
            
        }
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        //    4
        
        self.fetchedRecordsArray= [DatabaseHandler getEntityCountWithName:@"OrderTable"];
        if(self.fetchedRecordsArray.count==0)
        {
            [button removeFromSuperview];
            [self.tableviewObject addSubview:button];
            
        }
        else
        {
            [button removeFromSuperview];
        }
        
        [self.openOrderArray removeAllObjects];
        [self.sentOrderArray removeAllObjects];
        
        
        for(int i=0;i<self.fetchedRecordsArray.count;i++)
        {
            if([[[self.fetchedRecordsArray objectAtIndex:i] isSentMail] isEqualToString:@"YES"])
            {
                [self.sentOrderArray addObject:[self.fetchedRecordsArray objectAtIndex:i]];
                NSLog(@"SENT OERDER:%@",[[self.fetchedRecordsArray objectAtIndex:i] orderName]);
            }
            else
            {
                [self.openOrderArray addObject:[self.fetchedRecordsArray objectAtIndex:i]];
                NSLog(@"OPEN OERDER:%@",[[self.fetchedRecordsArray objectAtIndex:i] orderName]);
                
            }
        }
        
        if(self.sentOrderArray.count==0)
        {
            [list replaceObjectAtIndex:2 withObject:@""];
            
        }
        else
        {
            [list replaceObjectAtIndex:2 withObject:@"Zugestellte Aufträge"];
            
        }
        
        
        if(self.fetchedRecordsArray.count==0)
        {
            self.navigationItem.rightBarButtonItem = nil;
            [self.tableviewObject setEditing:NO];
        }
        
        //    5
        [self.tableviewObject endUpdates];
        [self.tableviewObject reloadData];
        
        
        
    }
}
@end
