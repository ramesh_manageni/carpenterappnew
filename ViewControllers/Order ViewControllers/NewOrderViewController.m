//
//  NewOrderViewController.m
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "NewOrderViewController.h"
#import "Constant.h"
#import "OrderDescriptionViewController.h"
#import "NavigationUtility.h"
#import "PersonalInformation.h"
#import "BillingAddressViewController.h"
#import "ListBillingAddressViewController.h"

@interface NewOrderViewController ()<UIScrollViewDelegate>

@property(strong,nonatomic)NSArray *fetchedRecordArray;

@property (strong, nonatomic) IBOutlet UIScrollView *_scrollView;

@property (weak, nonatomic) IBOutlet UILabel *objectNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *contractDecLabel1;
@property (weak, nonatomic) IBOutlet UILabel *pictureForOrderLabel1;
@property (weak, nonatomic) IBOutlet UILabel *tenantLabel1;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel1;
@property (weak, nonatomic) IBOutlet UILabel *caretakerComLabel1;
@property (weak, nonatomic) IBOutlet UILabel *objectCaretakerLabel1;
@property (weak, nonatomic) IBOutlet UILabel *personalInfoLabel1;

@property (weak, nonatomic) IBOutlet UIButton *objectNameButton;
@property (weak, nonatomic) IBOutlet UIButton *contractDecButton;
@property (weak, nonatomic) IBOutlet UIButton *pictureForOrderButton;
@property (weak, nonatomic) IBOutlet UIButton *tenantButton;
@property (weak, nonatomic) IBOutlet UIButton *ownerButton;
@property (weak, nonatomic) IBOutlet UIButton *caretakerComButton;
@property (weak, nonatomic) IBOutlet UIButton *objectCaretakerButton;
@property (weak, nonatomic) IBOutlet UIButton *personalInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *billingAddressButton;

@property(nonatomic) CGPoint recentContentOffset;


@end

@implementation NewOrderViewController

@synthesize _scrollView;
@synthesize masterRecord;
@synthesize isOrderSent;
@synthesize recentContentOffset;

@synthesize objectNameButton,contractDecButton,pictureForOrderButton,tenantButton,ownerButton,caretakerComButton,objectCaretakerButton,personalInfoButton;
@synthesize objectNameLabel1,contractDecLabel1,pictureForOrderLabel1,tenantLabel1,ownerLabel1,caretakerComLabel1,objectCaretakerLabel1,personalInfoLabel1;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.fetchedRecordArray= [DatabaseHandler getEntityCountWithName:@"PersonalInformation"];
    
    NSLog(@"COUNT OF PERSONAL INFO %lu",(unsigned long)self.fetchedRecordArray.count);
    
    if(MASTER_RECORD.orderTableToPersonalIfo==nil && self.fetchedRecordArray.count!=0)
    {
        MASTER_RECORD.orderTableToPersonalIfo=[self.fetchedRecordArray objectAtIndex:0];
    }
    
    //
    NSLog(@"MASTER RECORD %@",MASTER_RECORD.orderName);
    // Do any additional setup after loading the view from its nib.
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
    {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    self.navigationItem.rightBarButtonItem = [self addRightBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self addLeftBarButtonItem];
    _scrollView.showsHorizontalScrollIndicator=NO;
    _scrollView.showsVerticalScrollIndicator=NO;
    _scrollView.delegate=self;
    
    if(isFromPhotoTab)
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    [_scrollView setCanCancelContentTouches:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [NavigationUtility setNavigationTitle:@"Aufträge" withFontNmae:@"Helvetica-Bold" andSize:18.0];
    
    if(isOrderSent)
    {
        
        NSArray *subviews=self.view.subviews;
        for(UIView *view in subviews)
        {
            if([view isKindOfClass:[UIScrollView class]])
            {
                for(UIView *btn in view.subviews)
                {
                    if([btn isKindOfClass:[UIButton class]])
                    {
                        //[(UIButton *)btn setEnabled:NO];
                    }
                    
                }
            }
        }
        self.navigationItem.rightBarButtonItem=nil;
        isOrderSent=NO;
        
    }
    
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgImage setImage:[UIImage imageNamed:@"bluebg.jpg"]];
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
    
    
    if(isFromPhotoTab)
    {
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    
    //
    PHOTO_FLAG=0;
    
    [self initialSetup];
    
    
    _scrollView.contentOffset = CGPointMake(0, 0);
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    self.recentContentOffset = _scrollView.contentOffset;
    [super viewWillDisappear:animated];
}
//Intial Setup For View
-(void)initialSetup
{
    if(MASTER_RECORD.orderName!=nil && MASTER_RECORD.orderTableToPropertyAddress!=nil)
    {
        [self.objectNameButton setTitle:[NSString stringWithFormat:@"   %@",MASTER_RECORD.orderName] forState:UIControlStateNormal];
        [self.objectNameButton.titleLabel setTextColor:[UIColor whiteColor]];
    }
    else
    {
        [self.objectNameButton setTitle:@"   fehlt" forState:UIControlStateNormal];
        
    }
    if(MASTER_RECORD.orderName==nil && MASTER_RECORD.orderTableToPropertyAddress==nil)
    {
        [self.objectNameButton setTitle:@"   fehlt" forState:UIControlStateNormal];
        MASTER_RECORD.orderName=@"(Ohne Adresse)";
    }
    
    //Add Lodger Name
    if(MASTER_RECORD.lodgerName!=nil)
    {
        [self.tenantButton setTitle:[NSString stringWithFormat:@"   %@",MASTER_RECORD.lodgerName] forState:UIControlStateNormal];
        [self.tenantButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.tenantButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    
    if(MASTER_RECORD.orderTableToPersonalIfo.firstName!=nil)
    {
        [self.personalInfoButton setTitle:[NSString stringWithFormat:@"   %@ %@",MASTER_RECORD.orderTableToPersonalIfo.lastName,MASTER_RECORD.orderTableToPersonalIfo.firstName] forState:UIControlStateNormal];
        [self.personalInfoButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.personalInfoButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    //Add Owner Name
    if(MASTER_RECORD.ownerNmae!=nil)
    {
        [self.ownerButton setTitle:[NSString stringWithFormat:@"   %@",MASTER_RECORD.ownerNmae] forState:UIControlStateNormal];
        [self.ownerButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.ownerButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    //
    if(MASTER_RECORD.careTakerObject!=nil)
    {
        [self.objectCaretakerButton setTitle:[NSString stringWithFormat:@"   %@",MASTER_RECORD.careTakerObject] forState:UIControlStateNormal];
        [self.objectCaretakerButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.objectCaretakerButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    
    if(MASTER_RECORD.careTakerFirma!=nil)
    {
        [self.caretakerComButton setTitle:[NSString stringWithFormat:@"   %@",MASTER_RECORD.careTakerFirma] forState:UIControlStateNormal];
        [self.caretakerComButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.caretakerComButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    //
    if(MASTER_RECORD.billingFirma!=nil)
    {
        [self.billingAddressButton setTitle:[NSString stringWithFormat:@"   %@",MASTER_RECORD.billingFirma] forState:UIControlStateNormal];
        [self.billingAddressButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.billingAddressButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    //
    if([MASTER_RECORD.orderTableToPhotoDetail allObjects].count > 0)
    {
        [self.pictureForOrderButton setTitle:[NSString stringWithFormat:@"   angefügt"] forState:UIControlStateNormal];
        [self.pictureForOrderButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.pictureForOrderButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    //
    if([MASTER_RECORD.orderDescription count])
    {
        [self.contractDecButton setTitle:@"   angefügt" forState:UIControlStateNormal];
        [self.contractDecButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    }
    else
    {
        [self.contractDecButton setTitle:@"   fehlt" forState:UIControlStateNormal];
    }
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] == 6)
    {
        // iOS-6 code
        _scrollView.contentOffset = CGPointMake(0, self.recentContentOffset.y);
    }
//    [_scrollView setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    [_scrollView setContentSize:CGSizeMake(320, 470)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NavigationbarButton Items

-(UIBarButtonItem *)addLeftBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createLeftBarButtonForViewController:self withTitle:@"  zurück" withImage:@"backbutton.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(UIBarButtonItem *)addRightBarButtonItem
{
    UIBarButtonItem *barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"senden" withImage:@"buttonbg.png"];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(sendButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}

#pragma mark back baar Button
-(void)backButtonClicked
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
    NSLog(@"Pressed Back Baar Button");
}

#pragma mark save baar Button
-(void)sendButtonClicked
{
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    NSString *emailFileName = [NSString stringWithFormat:@"index.html"];
    NSString *emailFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: emailFileName];
    NSString *body = [NSString stringWithContentsOfFile:emailFilePath encoding:NSUTF8StringEncoding error:nil];
    
    
    // make sure you have the image name and extension (for demo purposes, I'm using "myImage" and "png" for the file "myImage.png", which may or may not be localized)
    NSString *imageFileName = @"logo";
    NSString *imageFileExtension = @"jpg";
    
    // load the path of the image in the main bundle (this gets the full local path to the image you need, including if it is localized and if you have a @2x version)
    UIImage *emailImage;
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageFileName ofType:imageFileExtension];
    emailImage = [UIImage imageWithContentsOfFile:imagePath];;
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(emailImage)];
    
    [objMFMailcomposeVc  addAttachmentData:imageData
                                  mimeType:@"image/png"
                                  fileName:imagePath];
    
    // generate the html tag for the image (don't forget to use file:// for local paths)
    body = [body stringByReplacingOccurrencesOfString:@"//logo_PLACEHOLDER//" withString:[NSString stringWithFormat:@"<img src=\"file://%@\" />", imagePath]];
    
    
    //Add Personal Information To Mail
    if([MASTER_RECORD.orderTableToPersonalIfo.maleOrFemale length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Geschlecht" withString:@"Geschlecht"];
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Geschlecht_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.maleOrFemale];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Geschlecht" withString:@""];
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Geschlecht_PLACEHOLDER//" withString:@""];
    }
    
    
    if([MASTER_RECORD.orderTableToPersonalIfo.firstName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Vorname" withString:@"Vorname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Vorname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.firstName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Vorname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Vorname_PLACEHOLDER//" withString:@""];
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.lastName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Nachname" withString:@"Nachname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Nachname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.lastName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Nachname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Nachname_PLACEHOLDER//" withString:@""];
        
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.phoneDirectly length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Tel. Fest" withString:@"Tel. Fest"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Tel. Fest_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.phoneDirectly];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Tel. Fest" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Tel. Fest_PLACEHOLDER//" withString:@""];
        
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.mobilePhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Tel. Mobil" withString:@"Tel. Mobil"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Tel. Mobil_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.mobilePhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Tel. Mobil" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Tel. Mobil_PLACEHOLDER//" withString:@""];
        
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.faxDirectly length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Fax" withString:@"Fax"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Fax_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.faxDirectly];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Fax" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Fax_PLACEHOLDER//" withString:@""];
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.emailID length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_E-Mail" withString:@"E-Mail"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_E-Mail_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.emailID];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_E-Mail" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_E-Mail_PLACEHOLDER//" withString:@""];
        
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.emailCC length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_CC" withString:@"CC"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_CC_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.emailCC];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_CC" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_CC_PLACEHOLDER//" withString:@""];
        
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.companyName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Firma" withString:@"Firma"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Firma_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.companyName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Firma" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Firma_PLACEHOLDER//" withString:@""];
        
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.streetNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Strasse" withString:@"Strasse"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Strasse_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.streetNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Strasse" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Strasse_PLACEHOLDER//" withString:@""];
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.zipCode length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_PLZ" withString:@"PLZ"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_PLZ_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.zipCode];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_PLZ" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_PLZ_PLACEHOLDER//" withString:@""];
        
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.place length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Ort" withString:@"Ort"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Ort_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.place];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Ort" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_Ort_PLACEHOLDER//" withString:@""];
    }
    
    if([MASTER_RECORD.orderTableToPersonalIfo.centralPhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Tel. Zentral" withString:@"Tel. Zentral"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_centralPhoneNo_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPersonalIfo.centralPhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PI_Tel. Zentral" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Pers&ouml;nlicheAngaben_centralPhoneNo_PLACEHOLDER//" withString:@""];
    }
    
    
    
    //Add Order Detail To Mail
    if(MASTER_RECORD.orderName)
    {
        body = [body stringByReplacingOccurrencesOfString:@"//Auftrag_Auftragsname_PLACEHOLDER//" withString:MASTER_RECORD.orderName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"//Auftrag_Auftragsname_PLACEHOLDER//" withString:@""];
    }
    
    if(MASTER_RECORD.orderDate)
    {
        body = [body stringByReplacingOccurrencesOfString:@"//Auftrag_Datum des Auftrags_PLACEHOLDER//" withString:MASTER_RECORD.orderDate];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"//Auftrag_Datum des Auftrags_PLACEHOLDER//" withString:@""];
    }
    
    // Add Property Address to Mail
    //1
    if([MASTER_RECORD.orderTableToPropertyAddress.streetNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PA_Strasse" withString:@"Strasse"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Strasse_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPropertyAddress.streetNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PA_Strasse" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Strasse_PLACEHOLDER//" withString:@""];
    }
    //2
    if([MASTER_RECORD.orderTableToPropertyAddress.zipCode length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PA_PLZ" withString:@"PLZ"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_PLZ_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPropertyAddress.zipCode];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PA_PLZ" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_PLZ_PLACEHOLDER//" withString:@""];
    }
    
    //3
    if([MASTER_RECORD.orderTableToPropertyAddress.place length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"PA_Ort" withString:@"Ort"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Ort_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToPropertyAddress.place];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"PA_Ort" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Ort_PLACEHOLDER//" withString:@""];
    }
    
    //Add Lodger Address to mail
    
    //1
    if([MASTER_RECORD.orderTableToTenantAddress.maleOrFemale length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Geschlecht" withString:@"Geschlecht"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Geschlecht_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.maleOrFemale];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Geschlecht" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Geschlecht_PLACEHOLDER//" withString:@""];
    }
    
    //2
    if([MASTER_RECORD.orderTableToTenantAddress.firstName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Vorname" withString:@"Vorname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Vorname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.firstName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Vorname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Vorname_PLACEHOLDER//" withString:@""];
    }
    
    //3
    if([MASTER_RECORD.orderTableToTenantAddress.lastName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Nachname" withString:@"Nachname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Nachname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.lastName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Nachname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Nachname_PLACEHOLDER//" withString:@""];
    }
    
    //4
    if([MASTER_RECORD.orderTableToTenantAddress.homePhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Tel. Privat" withString:@"Tel. Privat"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Tel. Privat_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.homePhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Tel. Privat" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Tel. Privat_PLACEHOLDER//" withString:@""];
    }
    
    //5
    if([MASTER_RECORD.orderTableToTenantAddress.bussinessPhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Tel. Gesch&auml;ft" withString:@"Tel. Gesch&auml;ft"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Tel. Gesch&auml;ft_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.bussinessPhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Tel. Gesch&auml;ft" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Tel. Gesch&auml;ft_PLACEHOLDER//" withString:@""];
    }
    
    //6
    if([MASTER_RECORD.orderTableToTenantAddress.mobilePhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Tel. Mobil" withString:@"Tel. Mobil"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Tel. Mobil_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.mobilePhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Tel. Mobil" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_Tel. Mobil_PLACEHOLDER//" withString:@""];
    }
    
    //7
    if([MASTER_RECORD.orderTableToTenantAddress.emailID length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_E-Mail" withString:@"E-Mail"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_E-mail_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.emailID];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_E-Mail" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Mieter_E-mail_PLACEHOLDER//" withString:@""];
    }
    
    //8
    if([MASTER_RECORD.orderTableToTenantAddress.vacancyType length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Zustand" withString:@"Zustand"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Zustand_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.vacancyType];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Zustand" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Zustand_PLACEHOLDER//" withString:@""];
    }
    
    //9
    if([MASTER_RECORD.orderTableToTenantAddress.propertyType length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Typ" withString:@"Typ"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Typ_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.propertyType];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Typ" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Typ_PLACEHOLDER//" withString:@""];
    }
    
    //10
    if([MASTER_RECORD.orderTableToTenantAddress.floorType length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Etage" withString:@"Etage"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Etage_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.floorType];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_Etage" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_Etage_PLACEHOLDER//" withString:@""];
    }
    
    //11
    if([MASTER_RECORD.orderTableToTenantAddress.directionType length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_T&uuml;r" withString:@"T&uuml;r"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_T&uuml;r_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.directionType];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_T&uuml;r" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_T&uuml;r_PLACEHOLDER//" withString:@""];
    }
    
    //12
    if([MASTER_RECORD.orderTableToTenantAddress.tenantNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_TenantNo" withString:@"Nr. Mieter/Liegenschaft"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_TenantNo_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.tenantNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_TenantNo" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_TenantNo_PLACEHOLDER//" withString:@""];
    }
    //13
    if([MASTER_RECORD.orderTableToTenantAddress.orderNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_OrderNo" withString:@"Auftrags-Nr.(REM, etc.)"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_OrderNo_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToTenantAddress.orderNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"M_OrderNo" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Adressen_OrderNo_PLACEHOLDER//" withString:@""];
    }
    
    //Add Owner Address to Mail
    
    //1
    if([MASTER_RECORD.orderTableToOwnerAddress.companyName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Firma" withString:@"Firma"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Firma_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.companyName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Firma" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Firma_PLACEHOLDER//" withString:@""];
    }
    
    //2
    if([MASTER_RECORD.orderTableToOwnerAddress.streetNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Strasse" withString:@"Strasse"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Strasse_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.streetNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Strasse" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Strasse_PLACEHOLDER//" withString:@""];
    }
    
    //3
    if([MASTER_RECORD.orderTableToOwnerAddress.zipCode length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_PLZ" withString:@"PLZ"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_PLZ_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.zipCode];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_PLZ" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_PLZ_PLACEHOLDER//" withString:@""];
    }
    
    //4
    if([MASTER_RECORD.orderTableToOwnerAddress.place length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Ort" withString:@"Ort"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Ort_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.place];
    }
    else
    {
        
        body = [body stringByReplacingOccurrencesOfString:@"E_Ort" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Ort_PLACEHOLDER//" withString:@""];
    }
    
    //5
    if([MASTER_RECORD.orderTableToOwnerAddress.centralPhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Tel. Zentrale" withString:@"Tel. Zentrale"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Tel. Zentrale_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.centralPhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Tel. Zentrale" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&amp;uuml;mer_Tel. Zentrale_PLACEHOLDER//" withString:@""];
    }
    
    //6
    if([MASTER_RECORD.orderTableToOwnerAddress.maleOrFemale length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Geschlecht" withString:@"Geschlecht"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Geschlecht_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.maleOrFemale];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Geschlecht" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Geschlecht_PLACEHOLDER//" withString:@""];
    }
    
    //7
    if([MASTER_RECORD.orderTableToOwnerAddress.firstName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Vorname" withString:@"Vorname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Vorname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.firstName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Vorname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Vorname_PLACEHOLDER//" withString:@""];
    }
    
    //8
    if([MASTER_RECORD.orderTableToOwnerAddress.lastName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Nachname" withString:@"Nachname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Nachname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.lastName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Nachname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Nachname_PLACEHOLDER//" withString:@""];
    }
    
    //9
    if([MASTER_RECORD.orderTableToOwnerAddress.landlinePhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Tel. Festnet" withString:@"Tel. Festnet"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Tel. Festnet_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.landlinePhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Tel. Festnet" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Tel. Festnet_PLACEHOLDER//" withString:@""];
    }
    
    //10
    if([MASTER_RECORD.orderTableToOwnerAddress.mobilePhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Tel. Mobile" withString:@"Tel. Mobile"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Tel. Mobile_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.mobilePhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_Tel. Mobile" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_Tel. Mobile_PLACEHOLDER//" withString:@""];
    }
    
    //11
    if([MASTER_RECORD.orderTableToOwnerAddress.emailID length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_E-Mail" withString:@"E-Mail"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_E-mail_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToOwnerAddress.emailID];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"E_E-Mail" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Eigent&uuml;mer_E-mail_PLACEHOLDER//" withString:@""];
    }
    
    
    //Add Caretaker Company Detail to mail
    
    //1
    if([MASTER_RECORD.orderTableToCTCAddress.companyName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Firma" withString:@"Firma"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Firma_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.companyName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Firma" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Firma_PLACEHOLDER//" withString:@""];
    }
    
    //2
    if([MASTER_RECORD.orderTableToCTCAddress.streetNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Strasse" withString:@"Strasse"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Strasse_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.streetNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Strasse" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Strasse_PLACEHOLDER//" withString:@""];
    }
    
    //3
    if([MASTER_RECORD.orderTableToCTCAddress.zipCode length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_PLZ" withString:@"PLZ"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_PLZ_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.zipCode];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_PLZ" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_PLZ_PLACEHOLDER//" withString:@""];
    }
    
    //4
    if([MASTER_RECORD.orderTableToCTCAddress.place length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Ort" withString:@"Ort"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Ort_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.place];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Ort" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Ort_PLACEHOLDER//" withString:@""];
    }
    
    //5
    if([MASTER_RECORD.orderTableToCTCAddress.centralPhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Tel. Zentrale" withString:@"Tel. Zentrale"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Tel. Zentrale_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.centralPhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Tel. Zentrale" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Tel. Zentrale_PLACEHOLDER//" withString:@""];
    }
    
    //6
    if([MASTER_RECORD.orderTableToCTCAddress.firstName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Vorname" withString:@"Vorname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Vorname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.firstName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Vorname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Vorname_PLACEHOLDER//" withString:@""];
    }
    
    //7
    if([MASTER_RECORD.orderTableToCTCAddress.lastName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Nachname" withString:@"Nachname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Nachname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.lastName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Nachname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Nachname_PLACEHOLDER//" withString:@""];
    }
    
    //8
    if([MASTER_RECORD.orderTableToCTCAddress.landlinePhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Tel. Festnetz" withString:@"Tel. Festnetz"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Tel. Festnetz_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.landlinePhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Tel. Festnetz" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Tel. Festnetz_PLACEHOLDER//" withString:@""];
    }
    
    //9
    if([MASTER_RECORD.orderTableToCTCAddress.mobilePhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Tel. Mobile" withString:@"Tel. Mobile"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Tel. Mobile_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.mobilePhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Tel. Mobile" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Tel. Mobile_PLACEHOLDER//" withString:@""];
    }
    
    //10
    if([MASTER_RECORD.orderTableToCTCAddress.emailID length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_E-Mail" withString:@"E-Mail"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_E-mail_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.emailID];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_E-Mail" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_E-mail_PLACEHOLDER//" withString:@""];
    }
    
    //11
    if([MASTER_RECORD.orderTableToCTCAddress.maleOrFemale length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Geschlecht" withString:@"Geschlecht"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Geschlecht_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.maleOrFemale];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_Geschlecht" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_Geschlecht_PLACEHOLDER//" withString:@""];
    }
    //12
    if([MASTER_RECORD.orderTableToCTCAddress.firmaEmailID length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_mailfirma" withString:@"E-Mail_Firma"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_mailfirma_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTCAddress.firmaEmailID];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"H_mailfirma" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Hauswart-Firma_mailfirma_PLACEHOLDER//" withString:@""];
    }
    
    //Add Object caretaker Address to Mail
    
    //1
    if([MASTER_RECORD.orderTableToCTObjAddress.maleOrFemale length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Geschlecht" withString:@"Geschlecht"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Geschlecht_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.maleOrFemale];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Geschlecht" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Geschlecht_PLACEHOLDER//" withString:@""];
    }
    
    //2
    if([MASTER_RECORD.orderTableToCTObjAddress.firstName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Vorname" withString:@"Vorname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Vorname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.firstName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Vorname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Vorname_PLACEHOLDER//" withString:@""];
    }
    
    //3
    if([MASTER_RECORD.orderTableToCTObjAddress.lastName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Nachname" withString:@"Nachname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Nachname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.lastName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Nachname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Nachname_PLACEHOLDER//" withString:@""];
    }
    
    
    //4
    if([MASTER_RECORD.orderTableToCTObjAddress.streetNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Strasse" withString:@"Strasse"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Strasse_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.streetNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Strasse" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Strasse_PLACEHOLDER//" withString:@""];
    }
    
    //5
    if([MASTER_RECORD.orderTableToCTObjAddress.zipCode length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_PLZ" withString:@"PLZ"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_PLZ_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.zipCode];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_PLZ" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_PLZ_PLACEHOLDER//" withString:@""];
    }
    
    //6
    if([MASTER_RECORD.orderTableToCTObjAddress.place length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Ort" withString:@"Ort"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Ort_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.place];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Ort" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Ort_PLACEHOLDER//" withString:@""];
    }
    
    //7
    if([MASTER_RECORD.orderTableToCTObjAddress.centralPhoneNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Tel. Zentrale" withString:@"Tel. Festnetz"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Tel. Zentrale_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.centralPhoneNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Tel. Zentrale" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Tel. Zentrale_PLACEHOLDER//" withString:@""];
    }
    //8
    if([MASTER_RECORD.orderTableToCTObjAddress.mobileNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Tel. Mobile" withString:@"Tel. Mobile"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Tel. Mobile_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.mobileNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_Tel. Mobile" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_Tel. Mobile_PLACEHOLDER//" withString:@""];
    }
    
    //9
    if([MASTER_RECORD.orderTableToCTObjAddress.emailID length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_E-Mail" withString:@"E-Mail"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_E-mail_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToCTObjAddress.emailID];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"C_E-Mail" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Objekt-Hauswart_E-mail_PLACEHOLDER//" withString:@""];
    }
    
    //Add Rechnungs-Adresse Address to Mail(billing address)
    
    //1
    if([MASTER_RECORD.orderTableToBillingAddress.companyName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Firma" withString:@"Firma"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Firma_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.companyName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Firma" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Firma_PLACEHOLDER//" withString:@""];
    }
    
    //2
    if([MASTER_RECORD.orderTableToBillingAddress.streetNo length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Strasse" withString:@"Strasse"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Strasse_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.streetNo];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Strasse" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Strasse_PLACEHOLDER//" withString:@""];
    }
    
    //3
    if([MASTER_RECORD.orderTableToBillingAddress.zipCode length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_PLZ" withString:@"PLZ"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_PLZ_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.zipCode];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_PLZ" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_PLZ_PLACEHOLDER//" withString:@""];
    }
    
    //4
    if([MASTER_RECORD.orderTableToBillingAddress.place length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Ort" withString:@"Ort"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Ort_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.place];
    }
    else
    {
        
        body = [body stringByReplacingOccurrencesOfString:@"R_Ort" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Ort_PLACEHOLDER//" withString:@""];
    }
    
    //5
    if([MASTER_RECORD.orderTableToBillingAddress.maleOrFemale length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Geschlecht" withString:@"Geschlecht"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Geschlecht_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.maleOrFemale];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Geschlecht" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Geschlecht_PLACEHOLDER//" withString:@""];
    }
    
    //6
    if([MASTER_RECORD.orderTableToBillingAddress.firstName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Vorname" withString:@"Vorname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Vorname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.firstName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Vorname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Vorname_PLACEHOLDER//" withString:@""];
    }
    
    //7
    if([MASTER_RECORD.orderTableToBillingAddress.lastName length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Nachname" withString:@"Nachname"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Nachname_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.lastName];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_Nachname" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_Nachname_PLACEHOLDER//" withString:@""];
    }
    //8
    if([MASTER_RECORD.orderTableToBillingAddress.email length]>0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_billingmail" withString:@"E-Mail"];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_billingmail_PLACEHOLDER//" withString:MASTER_RECORD.orderTableToBillingAddress.email];
    }
    else
    {
        body = [body stringByReplacingOccurrencesOfString:@"R_billingmail" withString:@""];
        
        body = [body stringByReplacingOccurrencesOfString:@"//Rechnungs-Adresse_billingmail_PLACEHOLDER//" withString:@""];
    }
    
    
    
    //Add Order Descreption To Mail
    
    NSMutableString *finalHtmlString=[[NSMutableString alloc] init];
    
    for (OrderDescription *orderDescObject in MASTER_RECORD.orderDescription)
    {
        NSMutableArray *selectedArray = [[NSMutableArray alloc]init];
        NSMutableDictionary *selectedTextsDictionary = [[NSMutableDictionary alloc]init];
        for (Tasks *tasks in orderDescObject.tasks)
        {
            [selectedArray addObject:tasks.assignedTask];
        }
        [selectedTextsDictionary setValue:selectedArray forKey:orderDescObject.category];
        NSLog(@"TASK: %@",selectedTextsDictionary);
        
        [finalHtmlString appendString:[NSMutableString stringWithFormat:@"<tr><td>%@</td></tr><tr><td>%@</td></tr>",orderDescObject.category,orderDescObject.subcategory]];
        
        for(int i=0;i<selectedArray.count;i++)
        {
            [finalHtmlString appendString:[NSString stringWithFormat:@"<tr><td>• %@</td></tr>",[selectedArray objectAtIndex:i]]];
        }
        if(orderDescObject.textDescription!=nil)
        {
            [finalHtmlString appendString:[NSString stringWithFormat:@"<tr><td>• %@</td></tr>",orderDescObject.textDescription]];
        }
        //
        // [finalHtmlString appendString:htmlString];
        
        NSLog(@"HTML STRING: %@",finalHtmlString);
        
    }
    body = [body stringByReplacingOccurrencesOfString:@"//Reparieren_PLACEHOLDER//" withString:finalHtmlString];
    
    
    //Add Photo_Detail To Mail
    NSMutableString *finalHtmlStringForPhoto=[[NSMutableString alloc] init];
    
    for(PhotoDetail *objPhotoDetail in MASTER_RECORD.orderTableToPhotoDetail)
    {
        
        BOOL fileExists=[[NSFileManager defaultManager] fileExistsAtPath:objPhotoDetail.imageURL];
        UIImage *emailImage;
        
        if (fileExists)
        {
            emailImage = [UIImage imageWithContentsOfFile:objPhotoDetail.imageURL];;
            
        }
        
        //This example would come from the main bundle, but your source can be elsewhere
        //Convert the image into data
        NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(emailImage)];
        
        NSInteger imageSize   = imageData.length;
        NSLog(@"SIZE OF IMAGE: %.2f Mb", (float)imageSize/1024/1024);
        
        if(imageSize > 26214400)// 25 mb = 26214400 bytes
        {
            [[[UIAlertView alloc] initWithTitle:@"Fehler" message:@"Angehängte Datei sollte nicht größer als 25 MB sein" delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
            return;
        }
        
        //Create a base64 string representation of the data using NSData+Base64
        //  NSString *base64String = [imageData base64EncodedString];
        //Add the encoded string to the emailBody string
        
        [objMFMailcomposeVc  addAttachmentData:imageData
                                      mimeType:@"image/png"
                                      fileName:objPhotoDetail.imageURL];
        
        //Don't forget the "<b>" tags are required, the "<p>" tags are optional
        //[emailBody appendString:[NSString stringWithFormat:@"<p><b><img src='data:image/png;base64,%@'></b></p>",base64String]];
        //You could repeat here with more text or images, otherwise
        
        NSString *url = objPhotoDetail.imageURL;
        NSArray *parts = [url componentsSeparatedByString:@"/"];
        NSString *filename = [parts objectAtIndex:[parts count]-1];
        
        NSString *tempStr=@"|";
        if(objPhotoDetail.detailText.length==0)
        {
            tempStr=@"";
        }
        
        [finalHtmlStringForPhoto appendString:[NSMutableString stringWithFormat:@"<tr><td width='357'>%@</td><td width='395'>%@ %@ %@</td></tr>",filename,objPhotoDetail.titleText,tempStr,objPhotoDetail.detailText]];
    }
    body = [body stringByReplacingOccurrencesOfString:@"//Photo_detail_PLACEHOLDER//" withString:finalHtmlStringForPhoto];
    
    // Email Subject
    //NSString *emailTitle = @"Auftrag Detailliertheit";
    
    if([MASTER_RECORD.orderDate length]==0)
    {
        MASTER_RECORD.orderDate = @"";
    }
    if([MASTER_RECORD.orderTableToPropertyAddress.streetNo length]==0)
    {
        MASTER_RECORD.orderTableToPropertyAddress.streetNo = @"";
    }
    if([MASTER_RECORD.orderTableToPropertyAddress.place length]==0)
    {
        MASTER_RECORD.orderTableToPropertyAddress.place = @"";
    }

    NSString *emailTitle =[NSString stringWithFormat:@"Auftrag %@ | %@ | %@",MASTER_RECORD.orderDate,MASTER_RECORD.orderTableToPropertyAddress.streetNo,MASTER_RECORD.orderTableToPropertyAddress.place];
    
    // Email Content
    
    // To address
    NSArray *toRecipents;
    NSArray *ccRecipients;
    if(MASTER_RECORD.orderTableToPersonalIfo.emailID!=nil)
    {
        toRecipents = [NSArray arrayWithObject:MASTER_RECORD.orderTableToPersonalIfo.emailID];
        
    }
    if(MASTER_RECORD.orderTableToPersonalIfo.emailCC!=nil)
    {
        ccRecipients = [NSArray arrayWithObjects:MASTER_RECORD.orderTableToPersonalIfo.emailCC, nil];
        
    }
    
    
    [objMFMailcomposeVc setSubject:emailTitle];
    [objMFMailcomposeVc setMessageBody:body isHTML:YES];
    [objMFMailcomposeVc setToRecipients:toRecipents];
    [objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Keine Mail-Konten" message:@"Bitte setzen Sie ein Mail-Konto, um E-Mail senden." delegate:self cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }
    
}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            if(![MASTER_RECORD.isSentMail isEqualToString:@"YES"])
            {
                MASTER_RECORD.isSentMail=@"NO";
                
            }
            break;
            
        case MFMailComposeResultSaved:
            if(![MASTER_RECORD.isSentMail isEqualToString:@"YES"])
            {
                MASTER_RECORD.isSentMail=@"NO";
                
            }
            NSLog(@"Mail saved");
            break;
            
        case MFMailComposeResultSent:
        {
            
            MASTER_RECORD.isSentMail=@"YES";
            EmailFeddbackViewController *objEmailFeddbackViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EmailFeddbackViewController"];
            [self.navigationController pushViewController:objEmailFeddbackViewController animated:YES];
            NSLog(@"Mail sent");
            break;
        }
            
        case MFMailComposeResultFailed:
            
            //Alert
            [self showAlert];
            if(![MASTER_RECORD.isSentMail isEqualToString:@"YES"])
            {
                MASTER_RECORD.isSentMail=@"NO";
                
            }
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
            
        default:
            break;
            
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark UIAlertView delegate
- (void)showAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sendung Auftrag" message:@"Ihr Auftrag konnte nicht gesendet werden. Überprüfen Sie Ihre Internetvebindung und versuchen Sie es später noch einmal." delegate:self cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil];
    
    alert.delegate=self;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)//OK button pressed
    {
    }
}
#pragma mark Objects Buttons
- (IBAction)allButtonAction:(UIButton *)sender
{
    
    switch (sender.tag)
    {
        case 0:
        {
            ListObjectAddressViewController *objListObjectAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListObjectAddressViewController"];
            [self.navigationController pushViewController:objListObjectAddressViewController animated:NO];
            
            objListObjectAddressViewController.showRadioButton=YES;
            isFromAddressTabPropertyAddress=NO;
            
            break;
        }
        case 1:
        {
            OrderDescriptionViewController *orderDescriptionViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderDescriptionViewController"];
            [self.navigationController pushViewController:orderDescriptionViewController animated:NO];
            
            break;        }
        case 2:
        {
            
            PHOTO_FLAG=1;
            isSecureButtonClicked=0;
            if(isOrderSentGlobal)
            {
                isOrderSentPhoto=YES;
            }
            [self.tabBarController setSelectedIndex:1];
            break;
        }
        case 3:
        {
            TenatAddressViewController *objTenatAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TenatAddressViewController"];
            [self.navigationController pushViewController:objTenatAddressViewController animated:NO];
            
            break;
            
        }
        case 4:
        {
            ListOwnerAddressViewController *objListOwnerAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListOwnerAddressViewController"];
            
            
            objListOwnerAddressViewController.showRadioButton=YES;
            
            isFromAddressTabOwner=NO;
            
            [self.navigationController pushViewController:objListOwnerAddressViewController animated:NO];
            break;
        }
        case 5:
        {
            ListCTCAddressViewController *objListCTCAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListCTCAddressViewController"];
            
            objListCTCAddressViewController.showRadioButton=YES;
            
            isFromAddressTabCTComp=NO;
            
            [self.navigationController pushViewController:objListCTCAddressViewController animated:NO];
            break;
        }
        case 6:
        {
            ListCTObjectViewController *objListCTObjectViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListCTObjectViewController"];
            
            objListCTObjectViewController.showRadioButton=YES;
            
            isFromAddressTabCTO=NO;
            
            [self.navigationController pushViewController:objListCTObjectViewController animated:NO];
            break;
            
        }
        case 7:
        {
            PersonalInfoViewController *objPersonalInfoViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
            [self.navigationController pushViewController:objPersonalInfoViewController animated:NO];
            break;
        }
        case 8:
        {
            ListBillingAddressViewController *objListBillingAddressViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListBillingAddressViewController"];
            
            
            objListBillingAddressViewController.showRadioButton=YES;
            
            isFromAddressTabBilling=NO;
            
            [self.navigationController pushViewController:objListBillingAddressViewController animated:NO];
            break;
        }
            
        default:
            break;
    }
}


#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}


@end
