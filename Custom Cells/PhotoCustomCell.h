//
//  PhotoCustomCell.h
//  Service48
//
//  Created by Redbytes on 16/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *cellBackgroundImageview;

@property (strong, nonatomic) IBOutlet UIImageView *imageBackgroundImageView;

@property (strong, nonatomic) IBOutlet UITextView *freeTextView;

@property (strong, nonatomic) IBOutlet UILabel *titleTextLabel;

@property (strong, nonatomic) IBOutlet UIImageView *orderImageView;

@end
