//
//  ListObjectAddressCustomCell.m
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"Helvetica-Bold"

#import "ListObjectAddressCustomCell.h"

@implementation ListObjectAddressCustomCell
@synthesize addressLabel,cellBackgroundImage,cellRadioButtonImage,button,enterOrderLabel,cellOverlaybutton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        enterOrderLabel =[[UILabel alloc]initWithFrame:CGRectMake(5, 7, 200, 25)];
		[enterOrderLabel setFont:[UIFont fontWithName:kFontName size:17]];
		[enterOrderLabel setBackgroundColor:[UIColor clearColor]];
        [enterOrderLabel setTextColor:[UIColor lightGrayColor]];
        [enterOrderLabel setTextAlignment:NSTextAlignmentLeft];

        addressLabel =[[UILabel alloc]initWithFrame:CGRectMake(40, 7, 150, 25)];
		[addressLabel setFont:[UIFont fontWithName:kFontName size:17]];
		[addressLabel setBackgroundColor:[UIColor clearColor]];
        [addressLabel setTextColor:[UIColor whiteColor]];
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(7, 9, 25, 25);
        [button setBackgroundImage:[UIImage imageNamed:@"radiobuttondeselect.png"] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor clearColor]];

        cellOverlaybutton = [UIButton buttonWithType:UIButtonTypeCustom];
        cellOverlaybutton.frame = CGRectMake(35, 0, self.frame.size.width-100, self.frame.size.height);
       // [cellOverlaybutton setBackgroundImage:[UIImage imageNamed:@"radiobuttondeselect.png"] forState:UIControlStateNormal];
        [cellOverlaybutton setBackgroundColor:[UIColor clearColor]];

        cellBackgroundImage=[[UIImageView alloc] initWithFrame:CGRectMake(0.5, 0, 282, 47)];
        [cellBackgroundImage setImage:[UIImage imageNamed:@"cellbg1.png"]];
        [cellBackgroundImage setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:addressLabel];
        [self.contentView addSubview:enterOrderLabel];

        [self.contentView addSubview:button];
        [self.contentView addSubview:cellOverlaybutton];

        [self.contentView setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];

    }
    return self;
}
- (void)layoutSubviews
{
    
    [super layoutSubviews];
    
    [addressLabel setFrame:CGRectMake(40, 7, 180, 25)];
    
    [UIView setAnimationsEnabled:YES];

    for (UIView *subview in self.subviews)
    {
        for (UIView *subview2 in subview.subviews)
        {
            NSLog(@"S : %@ \n subview %@",[subview2 class],subview2);
            
            if ([NSStringFromClass([subview2 class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"])
            { // move delete confirmation view
                
                [UIView setAnimationsEnabled:NO];
                [subview bringSubviewToFront:subview2];

                [addressLabel setFrame:CGRectMake(87, 7, 180, 25)];
               
            }
            
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)didTransitionToState:(UITableViewCellStateMask)state
{
    [super didTransitionToState:state];

    if (state & UITableViewCellStateShowingDeleteConfirmationMask )
    {
        //[self sendSubviewToBack:self.contentView];
        [UIView setAnimationsEnabled:NO];

    }
    
}

@end
