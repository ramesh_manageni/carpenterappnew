//
//  ListOrdersCustomCell.m
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//
#define kFontName @"Helvetica-Bold"

#import "ListOrdersCustomCell.h"

@implementation ListOrdersCustomCell
@synthesize orderLabel,dateLabel,cellBackgroundImage,enterOrderLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        orderLabel =[[UILabel alloc]initWithFrame:CGRectMake(5, 0, 200, 25)];
		[orderLabel setFont:[UIFont fontWithName:kFontName size:17]];
		[orderLabel setBackgroundColor:[UIColor clearColor]];
        [orderLabel setTextColor:[UIColor whiteColor]];
        [orderLabel setTextAlignment:NSTextAlignmentLeft];
        
        enterOrderLabel =[[UILabel alloc]initWithFrame:CGRectMake(5, 7, 200, 25)];
		[enterOrderLabel setFont:[UIFont fontWithName:kFontName size:17]];
		[enterOrderLabel setBackgroundColor:[UIColor clearColor]];
        [enterOrderLabel setTextColor:[UIColor lightGrayColor]];
        [enterOrderLabel setTextAlignment:NSTextAlignmentLeft];

        dateLabel =[[UILabel alloc]initWithFrame:CGRectMake(5, 20, 200, 30)];
		[dateLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
		[dateLabel setBackgroundColor:[UIColor clearColor]];
        [dateLabel setTextColor:[UIColor darkGrayColor]];
        [dateLabel setTextAlignment:NSTextAlignmentLeft];
        
        
        cellBackgroundImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 282, 47.5)];
        [cellBackgroundImage setImage:[UIImage imageNamed:@"cellbg1.png"]];
        [cellBackgroundImage setBackgroundColor:[UIColor clearColor]];
        //[self.contentView addSubview:cellBackgroundImage];

        [self.contentView addSubview:orderLabel];
        [self.contentView addSubview:enterOrderLabel];
        [self.contentView addSubview:dateLabel];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [orderLabel setFrame:CGRectMake(5, 0, 200, 25)];
    [enterOrderLabel setFrame:CGRectMake(5, 7, 200, 25)];
    [dateLabel setFrame:CGRectMake(5, 20, 200, 30)];
    
    [UIView setAnimationsEnabled:YES];

    for (UIView *subview in self.subviews)
    {
         for (UIView *subview2 in subview.subviews)
         {
             NSLog(@"S : %@ \n subview %@",[subview2 class],subview2);
            
            if ([NSStringFromClass([subview2 class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"])
            { // move delete confirmation view
               
                [UIView setAnimationsEnabled:NO];
                //[enterOrderLabel setFrame:CGRectMake(47, 9, 200, 25)];
                [subview bringSubviewToFront:subview2];

                [orderLabel setFrame:CGRectMake(47, 0, 200, 25)];
                [dateLabel setFrame:CGRectMake(47, 20, 200, 30)];
                
            }
            
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)didTransitionToState:(UITableViewCellStateMask)state
{
    if (state & UITableViewCellStateShowingDeleteConfirmationMask )
    {
        //[self sendSubviewToBack:self.contentView];
        [UIView setAnimationsEnabled:NO];
    }
    [super didTransitionToState:state];
}

@end
