//
//  ListOrdersCustomCell.h
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListOrdersCustomCell : UITableViewCell

@property(strong,nonatomic)UILabel *orderLabel;
@property(strong,nonatomic)UILabel *enterOrderLabel;
@property(strong,nonatomic)UILabel *dateLabel;
@property(strong,nonatomic)UIImageView *cellBackgroundImage;


@end
