//
//  PhotoCustomCell.m
//  Service48
//
//  Created by Redbytes on 16/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PhotoCustomCell.h"

@implementation PhotoCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self.freeTextView setEditable:NO];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setFrame:(CGRect)frame
{


    //frame.origin.y += 4;
    //frame.size.height -= 2 * 4;
    frame.size.height=68.5;
    [self.imageBackgroundImageView setFrame:CGRectMake(0, 0, 85, 70)];
    [self.orderImageView setFrame:CGRectMake(0, 0, 82, 69)];
    [self.titleTextLabel setFrame:CGRectMake(99, 8, 168, 21)];
    [self.freeTextView setFrame:CGRectMake(93, 32, 168, 36)];
    [super setFrame:frame];

}

- (void)layoutSubviews
{
    [super layoutSubviews];
   // [self setBackgroundColor:[UIColor greenColor]];
    
    [self.imageBackgroundImageView setFrame:CGRectMake(0, 0, 85, 70)];
    [self.orderImageView setFrame:CGRectMake(0, 0, 82, 69)];
    [self.titleTextLabel setFrame:CGRectMake(99, 8, 168, 21)];
    [self.freeTextView setFrame:CGRectMake(93, 32, 168, 36)];
    
    [UIView setAnimationsEnabled:YES];
    
    for (UIView *subview in self.subviews)
    {
        
        
        if ([NSStringFromClass([subview class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"])
        {
            NSLog(@"S : %@ \n subviews %@",[subview class],subview.subviews);

            [UIView setAnimationsEnabled:NO];
            [self.imageBackgroundImageView setFrame:CGRectMake(43, 0, 85, 70)];
            [self.orderImageView setFrame:CGRectMake(43, 0, 82, 69)];
            [self.titleTextLabel setFrame:CGRectMake(139, 8, 100, 21)];
            [self.freeTextView setFrame:CGRectMake(133, 32, 100, 36)];
            
            [self bringSubviewToFront:subview];


            return;
        }
        

        for (UIView *subview2 in subview.subviews)
        {
            NSLog(@"S : %@ \n subview %@",[subview2 class],subview2);

            if ([NSStringFromClass([subview2 class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"])
            { // move delete confirmation view
                
                [UIView setAnimationsEnabled:NO];
                [self.imageBackgroundImageView setFrame:CGRectMake(44, 0, 85, 70)];
                [self.orderImageView setFrame:CGRectMake(44, 0, 82, 69)];
                [self.titleTextLabel setFrame:CGRectMake(139, 8, 100, 21)];
                [self.freeTextView setFrame:CGRectMake(133, 32, 100, 36)];
                
                [self bringSubviewToFront:subview2];

                return;


            }
            
        }
    }
}

-(void)didTransitionToState:(UITableViewCellStateMask)state
{
    [super didTransitionToState:state];
    
    if (state & UITableViewCellStateShowingDeleteConfirmationMask )
    {
        [UIView setAnimationsEnabled:NO];
    }
    
}

-(void)willTransitionToState:(UITableViewCellStateMask)state

{
    
    [super willTransitionToState:state];
    
    if ((state & UITableViewCellStateShowingDeleteConfirmationMask) ==UITableViewCellStateShowingDeleteConfirmationMask)
    {
        for (UIView *subview in self.subviews)
        {
            if ([NSStringFromClass([subview class])isEqualToString:@"UITableViewCellDeleteConfirmationControl"])
            {
                NSLog(@"UITableViewCellDeleteConfirmationControl Detected");
            }
        }
    }
    
}


@end
