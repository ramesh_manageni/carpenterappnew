//
//  ListObjectAddressCustomCell.h
//  Service48
//
//  Created by Redbytes on 22/04/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListObjectAddressCustomCell : UITableViewCell
@property(strong,nonatomic)UIImageView *cellBackgroundImage;
@property(strong,nonatomic)UIImageView *cellRadioButtonImage;
@property(strong,nonatomic)UILabel *addressLabel;
@property(strong,nonatomic)UIButton *button;
@property(strong,nonatomic)UIButton *cellOverlaybutton;
@property(strong,nonatomic)UILabel *enterOrderLabel;

@end
