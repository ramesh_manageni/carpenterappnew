//
//  CategoryDescriptionRepository.m
//  PackingList
//
//  Created by Chetan Zalake on 23/07/13.
//  Copyright (c) 2013 Chetan Zalake. All rights reserved.
//

#import "CategoryDescriptionRepository.h"



@implementation CategoryDescriptionRepository

/*
- (CategoryTable *)getCategoryDescriptionDataFromDBWithCategoryType : (NSString*)category_type
{
    
    NSString *query = @"SELECT * FROM Categorylist "
    @"WHERE category_type = ?";

    FMResultSet *resultSet = [db executeQuery:query,category_type];
    
    SingletonArray *sharedArray = [SingletonArray sharedArray];
    sharedArray.receivedObjects = [[NSMutableArray alloc]init];
                                                             
   
    CategoryTable *categoryDescriptionObj = nil;
        
    while ([resultSet next]) {
        categoryDescriptionObj = [[CategoryTable alloc] init];
        
        categoryDescriptionObj.category_id = [resultSet intForColumn:@"category_id"];
        categoryDescriptionObj.category_name = [resultSet stringForColumn:@"category_name"];
        categoryDescriptionObj.category_description = [resultSet stringForColumn:@"category_description"];
        categoryDescriptionObj.category_type = [resultSet stringForColumn:@"category_type"];
        categoryDescriptionObj.count = [resultSet intForColumn:@"count"];
        categoryDescriptionObj.packed = [resultSet stringForColumn:@"packed"];
        
        
        
        [sharedArray.receivedObjects addObject:categoryDescriptionObj];
        
        
        
    
    }
    [resultSet close];
    return categoryDescriptionObj;
                                                          
    
}

- (CategoryTable *)getCategoryDescriptionDataFromDBWithCategory_ID : (int)category_id
{
    
    NSString *query = [NSString stringWithFormat: @"SELECT * FROM Categorylist "
    @"WHERE category_id = %d",category_id];
    
    FMResultSet *resultSet = [db executeQuery:query];
    
    SingletonArray *sharedArray = [SingletonArray sharedArray];
    sharedArray.receivedObjects = [[NSMutableArray alloc]init];
    
    
    CategoryTable *categoryDescriptionObj = nil;
    
    while ([resultSet next]) {
        categoryDescriptionObj = [[CategoryTable alloc] init];
        
        categoryDescriptionObj.category_id = [resultSet intForColumn:@"category_id"];
        categoryDescriptionObj.category_name = [resultSet stringForColumn:@"category_name"];
        categoryDescriptionObj.category_description = [resultSet stringForColumn:@"category_description"];
        categoryDescriptionObj.category_type = [resultSet stringForColumn:@"category_type"];
        categoryDescriptionObj.count = [resultSet intForColumn:@"count"];
        categoryDescriptionObj.packed = [resultSet stringForColumn:@"packed"];
        
        
        
        [sharedArray.receivedObjects addObject:categoryDescriptionObj];
        
        NSLog(@"%d",category_id);
        
        
    }
    [resultSet close];
    return categoryDescriptionObj;
    
    
}


- (CategoryTable *)getAllCategoryDescriptionDataFromDB
{
    NSString *query = @"SELECT * FROM Categorylist ";
    
    
    FMResultSet *resultSet = [db executeQuery:query];
    
    SingletonArray *sharedArray = [SingletonArray sharedArray];
    sharedArray.receivedObjects = [[NSMutableArray alloc]init];
    
    
    CategoryTable *categoryDescriptionObj = nil;
    
    while ([resultSet next]) {
        categoryDescriptionObj = [[CategoryTable alloc] init];
        
        categoryDescriptionObj.category_id = [resultSet intForColumn:@"category_id"];
        categoryDescriptionObj.category_name = [resultSet stringForColumn:@"category_name"];
        categoryDescriptionObj.category_description = [resultSet stringForColumn:@"category_description"];
        categoryDescriptionObj.category_type = [resultSet stringForColumn:@"category_type"];
        categoryDescriptionObj.count = [resultSet intForColumn:@"count"];
        categoryDescriptionObj.packed = [resultSet stringForColumn:@"packed"];
        
        
        
        [sharedArray.receivedObjects addObject:categoryDescriptionObj];
        
        
        
        
    }
    [resultSet close];
    return categoryDescriptionObj;

}

-(BOOL)setPaidStatusofCategory_id : (int) category_id Status : (BOOL) status
{
     NSString *query = [NSString stringWithFormat:@"UPDATE PaidStatus "
                        @"SET Status = %c WHERE category_id = %d",status,category_id];
    
    BOOL result = [db executeUpdate:query];
    
    if(result == YES)
    {
        NSLog(@"SUCCESS");
        return YES;
    }
    
    else
    {
        NSLog(@"ERROR");
        return NO;
    }
}

- (BOOL)getPaidStausOfCategory_id : (int) category_id
{
    NSString *query = [NSString stringWithFormat:@"SELECT status from PaidStatus "
                       @"where category_id = %d",category_id];
    NSString *result;
    
    FMResultSet *resultSet = [db executeQuery:query];
    if([resultSet next])
    {
        
        result = [resultSet stringForColumn:@"status"];
        
    }
    [resultSet close];
    
    if([result isEqualToString:@"NO"])
    return NO;
    else
        return YES;
    
}

- (BOOL)addCategoryToDBWithCategoryName : (NSString *)category_name description: (NSString*)category_description category_id : (int)category_id category_type : (NSString *)category_type packedChar:(NSString *)packed_char count:(int)count;
{
    NSString *query = [NSString stringWithFormat:@"INSERT INTO Categorylist "
    @"(category_name,category_description, category_id, category_type, packed, count) "
    @"VALUES ('%@','%@',%d,'%@','%@',%d)",category_name,category_description, category_id,category_type,packed_char,count];
    
    BOOL result = [db executeUpdate:query];
    
    
    if(result == YES)
        NSLog(@"SUCCESS");
    else
        NSLog(@"ERROR");
    
    return result;
    
}

-(BOOL) setCountOfItemsWithCategory_id : (int)category_id addOrSubstractValueNotation : (NSString*)Notation
{
    NSString *query = [NSString stringWithFormat:@"UPDATE Categorylist set count = count%@ where category_id = %d",Notation,category_id];
    
    BOOL result = [db executeUpdate:query];
    
    
    if(result == YES)
        NSLog(@"SUCCESS");
    else
        NSLog(@"ERROR");
    
    return result;

}

-(BOOL) deleteCategoryWithCategory_id : (int)category_id
{
    NSString *queryForCategory = [NSString stringWithFormat:@"DELETE From Categorylist where category_id = %d",category_id];
    NSString *queryForSubCategory = [NSString stringWithFormat:@"DELETE From subcategory where category_id = %d",category_id];
    NSString *queryForItem = [NSString stringWithFormat:@"DELETE From item where category_id = %d",category_id];
    
    BOOL resultForCategory = [db executeUpdate:queryForCategory];
    BOOL resultForSubCategory = [db executeUpdate:queryForSubCategory];
    BOOL resultForItem = [db executeUpdate:queryForItem];
    
    if(resultForCategory == resultForItem == resultForSubCategory)
    {
        NSLog(@"SUCCESS");
        return YES;
    }
    else
    {
        NSLog(@"ERROR");
        return NO;
    }
    
}
*/
-(NSMutableArray*) getCategory_Name : (int)sectionNumber
{
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM categorias WHERE tipo_id = %d",sectionNumber+1];;
    //SingletonArray *sharedArray = [SingletonArray sharedArray];
    NSMutableArray *receivedObjects = [[NSMutableArray alloc]init];
    FMResultSet *resultSet = [db executeQuery:query];
    
    while ([resultSet next])
    {
        
        [receivedObjects addObject:[resultSet stringForColumn:@"name"]];
    }
    [resultSet close];
    return receivedObjects ;
}


@end
