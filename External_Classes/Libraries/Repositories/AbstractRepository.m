//
//  AbstractRepository.m
//  Fishtacular
//
//  Created by  on 05/03/13.
//  Copyright (c) 2013 Fishtacular. All rights reserved.
//

#import "AbstractRepository.h"

@implementation AbstractRepository

+ (NSString *) databaseFilename
{
	return @"Service48.db";
}

+ (NSString *) databasePath {
    
    NSString *fileName = [AbstractRepository databaseFilename];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbFilePath = [documentDirectory stringByAppendingPathComponent:fileName];
    return dbFilePath;
    
}

- (id)init {
    
    if (self = [super init]) {
        
        NSString *dbPath = [AbstractRepository databasePath];
        db = [FMDatabase databaseWithPath:dbPath];
        [db setTraceExecution:YES];
        [db setLogsErrors:YES];
        [db open];
        
    }
    return self;
    
}

@end
