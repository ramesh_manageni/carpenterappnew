//
//  SubCategoryDescriptionRepository.m
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SubCategoryDescriptionRepository.h"
#import "SunCategoryTable.h"

@implementation SubCategoryDescriptionRepository

-(NSMutableArray*) getSubCategory_Name : (int)categoryId
{
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM subcategorias WHERE categoria_id = %d",categoryId+1];;
    //SingletonArray *sharedArray = [SingletonArray sharedArray];
    NSMutableArray *receivedObjects = [[NSMutableArray alloc]init];
    FMResultSet *resultSet = [db executeQuery:query];
    
    SunCategoryTable *subCategoryDescriptionObject = nil;
    
    while ([resultSet next])
    {
        subCategoryDescriptionObject = [[SunCategoryTable alloc]init];
        subCategoryDescriptionObject.textId = [resultSet intForColumn:@"texto_id"];
        subCategoryDescriptionObject.subCategoryName = [resultSet stringForColumn:@"name"];
        [receivedObjects addObject:subCategoryDescriptionObject];
    }
    [resultSet close];
    return receivedObjects ;
}

@end
