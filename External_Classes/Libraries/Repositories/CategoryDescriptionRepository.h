//
//  CategoryDescriptionRepository.h
//  PackingList
//
//  Created by Chetan Zalake on 23/07/13.
//  Copyright (c) 2013 Chetan Zalake. All rights reserved.
//

#import "AbstractRepository.h"
#import "CategoryTable.h"

@interface CategoryDescriptionRepository : AbstractRepository

/*
- (CategoryTable *)getCategoryDescriptionDataFromDBWithCategoryType : (NSString*)category_type;

- (BOOL)addCategoryToDBWithCategoryName : (NSString *)category_name description: (NSString*)category_description category_id : (int)category_id category_type : (NSString *)category_type packedChar:(NSString *)packed_char count:(int)count;

-(BOOL) setCountOfItemsWithCategory_id : (int)category_id addOrSubstractValueNotation : (NSString*)Notation;

-(BOOL) deleteCategoryWithCategory_id : (int)category_id;

-(int) getLastCategory_Id;

- (BOOL)getPaidStausOfCategory_id : (int) category_id;

- (CategoryTable *)getAllCategoryDescriptionDataFromDB;

- (CategoryTable *)getCategoryDescriptionDataFromDBWithCategory_ID : (int)category_id;

-(BOOL)setPaidStatusofCategory_id : (int) category_id Status : (BOOL) status;
*/

-(NSMutableArray*) getCategory_Name : (int)sectionNumber;

@end
