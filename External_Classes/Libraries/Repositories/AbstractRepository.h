//
//  AbstractRepository.h
//  Fishtacular
//
//  Created by  on 05/03/13.
//  Copyright (c) 2013 . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMResultSet.h"

@interface AbstractRepository : NSObject{
    
    FMDatabase *db;
    
}

+ (NSString *) databaseFilename;


@end
