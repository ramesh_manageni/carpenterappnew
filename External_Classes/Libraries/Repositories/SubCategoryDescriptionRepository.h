//
//  SubCategoryDescriptionRepository.h
//  Service48
//
//  Created by Chetan Zalake on 19/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AbstractRepository.h"

@interface SubCategoryDescriptionRepository : AbstractRepository

-(NSMutableArray*) getSubCategory_Name : (int)categoryId;

@end
