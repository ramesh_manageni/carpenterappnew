

#import <UIKit/UIKit.h>

@interface UIImage (AspectFit)

- (CGRect)tgr_aspectFitRectForSize:(CGSize)size;

@end

