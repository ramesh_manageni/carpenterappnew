//
//  AppContext.m
//  Anypic
//
//  Created by Redbytes on 8/30/13.
//
//

#import "AppContext.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1

@implementation AppContext


#pragma mark - Initialization

+ (id)sharedAppContext {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        _storedTextsArray = [[NSMutableArray alloc]init];
    }
    return self;
}

-(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

-(NSString *)storyboardString
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        return @"Main";
    }
    else
    {
        return @"Main_iPad";
    }
}


@end
