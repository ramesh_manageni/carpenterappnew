//
//  DatabaseHandler.m
//  Service48
//
//  Created by Redbytes on 12/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "DatabaseHandler.h"
#import "Constant.h"
#import "CareTakerObjectAddress.h"
#import "BillingAddress.h"
#import "OrderTable.h"

@implementation DatabaseHandler

//Add object to database
+(BOOL)insertRecord:(NSManagedObject *)record withData:(NSDictionary *)recordDict withMasterData:(NSDictionary *)masterDict withEntityNmae:(NSString *)entityName;
{
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    if (record==nil)
    {
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [delegate managedObjectContext];
        record=[NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    }
    
    NSArray *destinationKeys = recordDict.allKeys;
    for (NSString *key in destinationKeys)
    {
        id value = [recordDict valueForKey:key];
        // Avoid NULL values
        if (value && ![value isEqual:[NSNull null]]) {
            [record setValue:value forKey:key];
        }
    }
    
    NSArray *masterDestinationKeys = masterDict.allKeys;
    for (NSString *key in masterDestinationKeys)
    {
        if(record!=nil)
        {
            [MASTER_RECORD setValue:record forKey:key];
            
        }
    }
    if([entityName isEqualToString:@"PropertyAddress"])
    {
        NSString *streatNo;
        NSString *place;
        streatNo=MASTER_RECORD.orderTableToPropertyAddress.streetNo;
        place=MASTER_RECORD.orderTableToPropertyAddress.place;
        
        if(streatNo==nil && place==nil)
        {
            MASTER_RECORD.orderName=@"(Ohne Adresse)";
        }
        else
        {
            if(streatNo==nil)
            {
                streatNo=@"";
            }
            if(place==nil)
            {
                place=@"";
            }
            
            MASTER_RECORD.orderName=[NSString stringWithFormat:@"%@ %@",streatNo,place];
        }
        
        if([streatNo isEqualToString:@""] && [place isEqualToString:@""])
        {
            MASTER_RECORD.orderName=@"(Ohne Adresse)";
        }
        
        streatNo=nil;
        place=nil;
    }
    if([entityName isEqualToString:@"CareTakerObjectAddress"])
    {
        
//        if([[record valueForKey:@"companyName"]length]!=0)
//        {
//            [MASTER_RECORD setCareTakerObject:[record valueForKey:@"companyName"]];
//            
//        }
        if([[record valueForKey:@"firstName"] length]!=0)
        {
            [MASTER_RECORD setCareTakerObject:[record valueForKey:@"firstName"]];
            
        }
        else if([[record valueForKey:@"lastName"]length]!=0)
        {
            [MASTER_RECORD setCareTakerObject:[record valueForKey:@"lastName"]];
            
        }
        
        
        if([[record valueForKey:@"firstName"]length]!=0)
        {
            if([[record valueForKey:@"lastName"]length]!=0)
            {
                [MASTER_RECORD setCareTakerObject:[NSString stringWithFormat:@"%@ %@",[record valueForKey:@"lastName"],[record valueForKey:@"firstName"]]];
            }
            else
            {
                [MASTER_RECORD setCareTakerObject:[record valueForKey:@"firstName"]];
            }
        }
        
        else if([[record valueForKey:@"lastName"]length]!=0)
        {
            [MASTER_RECORD setCareTakerObject:[record valueForKey:@"lastName"]];
        }
        
        else
        {
            [MASTER_RECORD setCareTakerObject:@"(Ohne Vorname)"];
            
        }
        
    }
    if([entityName isEqualToString:@"CareTakerCompanyAddress"])
    {
        
        if([[record valueForKey:@"companyName"] length]==0)
        {
            [record setValue:@"(Ohne Firma)" forKey:@"careTakerFirmaName"];
            
        }
        else
        {
            
            [record setValue:[record valueForKey:@"companyName"] forKey:@"careTakerFirmaName"];
        }
        
        
        [MASTER_RECORD setCareTakerFirma:[record valueForKey:@"careTakerFirmaName"]];
    }
    
   if([entityName isEqualToString:@"BillingAddress"])
    {
    if([[record valueForKey:@"companyName"]length]==0)
    {
        [record setValue:@"(Ohne Firma)" forKey:@"billingFirmaName"];
        
        if([[record valueForKey:@"firstName"]length]!=0)
        {
            [record setValue:[record valueForKey:@"firstName"] forKey:@"billingFirmaName"];
            
            if([[record valueForKey:@"lastName"]length]!=0)
            {
                
                [record setValue:[NSString stringWithFormat:@"%@ %@",[record valueForKey:@"firstName"],[record valueForKey:@"lastName"]] forKey:@"billingFirmaName"];
                
            }
            
        }
    }
    else
    {
        
        [record setValue:[record valueForKey:@"companyName"] forKey:@"billingFirmaName"];
    }
    
    MASTER_RECORD.billingFirma=[record valueForKey:@"billingFirmaName"];
    

//        if([[record valueForKey:@"companyName"] length]==0)
//        {
//            [record setValue:@"(Ohne Firma)" forKey:@"billingFirmaName"];
//            
//        }
//        else
//        {
//            
//            [record setValue:[record valueForKey:@"companyName"] forKey:@"billingFirmaName"];
//        }
//        
//        
//        [MASTER_RECORD setBillingFirma:[record valueForKey:@"billingFirmaName"]];
   }
    
    if([entityName isEqualToString:@"OwnerAddress"])
    {
        if([[record valueForKey:@"companyName"]length]==0)
        {
            [record setValue:@"(Ohne Firma)" forKey:@"ownerName"];
            
            if([[record valueForKey:@"firstName"]length]!=0)
            {
                [record setValue:[record valueForKey:@"firstName"] forKey:@"ownerName"];
                 
                if([[record valueForKey:@"lastName"]length]!=0)
                {
                    
                    [record setValue:[NSString stringWithFormat:@"%@ %@",[record valueForKey:@"firstName"],[record valueForKey:@"lastName"]] forKey:@"ownerName"];
                    
                }
                
            }
        }
        else
        {
            
            [record setValue:[record valueForKey:@"companyName"] forKey:@"ownerName"];
        }
        
        MASTER_RECORD.ownerNmae=[record valueForKey:@"ownerName"];


    }

    
    NSError *err;
    
    if( ! [context save:&err] )
    {
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
        return NO;
    }
    else
    {
        return YES;
    }
    
}

//Get count of perticular objects from database
+(NSMutableArray *)getEntityCountWithName:(NSString *)entityName
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSError *error;
    
    return [[[NSArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]] mutableCopy];
}

@end
