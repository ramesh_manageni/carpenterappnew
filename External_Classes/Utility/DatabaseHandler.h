//
//  DatabaseHandler.h
//  Service48
//
//  Created by Redbytes on 12/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseHandler : NSObject
//To insert data into database
//Parameters:
//recordDict-this is dictionary object that holds all data that has to be stored in to DB.
//masterDict-this dictionaty contains object of master table and the recordDict has to be added into this one.
//entityName-table name.
+(BOOL)insertRecord:(NSManagedObject *)record withData:(NSDictionary *)recordDict withMasterData:(NSDictionary *)masterDict withEntityNmae:(NSString *)entityName;

//To get count of record
+(NSMutableArray *)getEntityCountWithName:(NSString *)entityName;

@end
