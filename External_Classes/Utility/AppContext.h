//
//  AppContext.h
//  Anypic
//
//  Created by Redbytes on 8/30/13.
//
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"




@interface AppContext : NSObject


+ (id)sharedAppContext;

@property (nonatomic, strong) NSMutableArray *storedTextsArray;


-(AppDelegate *)sharedAppDelegate;
-(NSString *)storyboardString;

@end
