//
//  NavigationUtility.m
//  Service48
//
//  Created by Redbytes on 13/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "NavigationUtility.h"

@implementation NavigationUtility
#define ksize 16

+(UIBarButtonItem *)createRightBarButtonForViewController:(UIViewController *)targetViewController withTitle:(NSString *)title withImage:(NSString *)imageName
{
    float xValue;
    float width;
    xValue=0;
    width=95;
    
    if (!SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        xValue=10;
    }
    
    if([title isEqualToString:@"senden"]||[title isEqualToString:@"sichern"])
    {
        width=70;
        xValue=30;
    }
    UIButton *btn = [[UIButton alloc] initWithFrame: CGRectMake(0+xValue, 3, width, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:imageName] ;
    [btn setBackgroundImage:backImage  forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:ksize];
    
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 95, 35)];
    btnView.bounds = CGRectOffset(btnView.bounds, 0, 0);
    [btnView addSubview:btn];
    [btnView sizeToFit];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:btnView];
    
    return buttonItem;
    
}
+(UIBarButtonItem *)createLeftBarButtonForViewController:(UIViewController *)targetViewController withTitle:(NSString *)title withImage:(NSString *)imageName
{
    
    float xValue;
    xValue=0;
    if (!SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        xValue=10;
    }

    UIButton *btn = [[UIButton alloc] initWithFrame: CGRectMake(0-xValue, 3, 72.0f, 30.0f)];
    UIImage *backImage = [UIImage imageNamed:imageName] ;
    [btn setBackgroundImage:backImage  forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:ksize];

    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 72, 35)];
    btnView.bounds = CGRectOffset(btnView.bounds, 0, 0);
    [btnView addSubview:btn];
    [btnView sizeToFit];

    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:btnView];
    return buttonItem;

}
+(UILabel *)setNavigationTitle:(NSString *) title withFontNmae:(NSString *)fontName andSize:(double )size
{
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.text = title;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.font = [UIFont fontWithName:fontName size:size];

    [lblTitle sizeToFit];
    return lblTitle;

}

@end
